/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System;
using System.Collections;
using TogglePlay.Delegates;

namespace TogglePlay
{
	/// <summary>
	/// Uses class as a container instead of namespace so can use Yield.continueBreak for example
	/// </summary>
	public class Yield
	{
		/// <summary>
		/// Base abstract class for use in 
		/// </summary>
		public abstract class EnumeratedYield : YieldInstruction
		{
			public abstract IEnumerator Yield();
		}

		public class WaitForYeildable : EnumeratedYield
		{
			IEnumerator enumerator = null;

			public WaitForYeildable( IYieldable yieldable )
			{
				enumerator = yieldable.Enumerator;
			}

			public override IEnumerator Yield()
			{
				while( enumerator != null && enumerator.MoveNext() )
					yield return enumerator.Current;

				yield break;
			}
		}

		public class WaitForEnumerable : EnumeratedYield
		{
			IEnumerator enumerator = null;
			
			public WaitForEnumerable( IEnumerable yieldable )
			{
				enumerator = yieldable.GetEnumerator();
			}
			
			public override IEnumerator Yield()
			{
				while( enumerator != null && enumerator.MoveNext() )
					yield return enumerator.Current;
				
				yield break;
			}
		}

		/// <summary>
		/// Keeps looping until the animation is no longer running
		/// </summary>
		public class WaitWhileAnimated : EnumeratedYield
		{
			Animation a = null;
			string s = null;
			public WaitWhileAnimated( Animation target )
			{
				a = target;
			}

			public WaitWhileAnimated( Animation target, string clipName )
			{
				a = target;
				s = clipName;
			}

			public override IEnumerator Yield()
			{
				while(true)
				{
					if( a == null )
						yield break;
					if( string.IsNullOrEmpty(s) == false && a.IsPlaying(s) )
						yield return null;
					else if( a.isPlaying )
						yield return null;
					else
						yield break;
				}
			}
		}

		/// <summary>
		/// Keeps looping until the amount of time has passed.
		/// Alternative to WaitForSeconds so can be paused mid instructon
		/// </summary>
		public class WaitForLength : EnumeratedYield
		{
			float seconds = 0f;
			bool realTime = false;
			public WaitForLength( float seconds )
			{
				this.seconds = seconds;
			}

			public WaitForLength( float seconds, bool realTime )
			{
				this.seconds = seconds;
				this.realTime = realTime;
			}
			
			public override IEnumerator Yield()
			{
				if( realTime )
				{
					float start = Time.time;
					while( Time.time < start + seconds )
						yield return null;
				}
				else
				{
					for( float t=0; t<seconds; t+=Time.deltaTime )
						yield return null;
				}
			}
		}

		public class WaitForUpdates : EnumeratedYield
		{
			int updateCountFor = 0;
			public WaitForUpdates( int updateCount )
			{
				updateCountFor = updateCount;
			}
			
			public override IEnumerator Yield()
			{
				do
				{
					yield return null;
					updateCountFor--;
				}
				while(updateCountFor > 0);

				yield break;
			}
		}

		public class WaitForCondition : EnumeratedYield
		{
			Conditional.ICondition condition = null;
			public WaitForCondition( Conditional.ICondition condition )
			{
				this.condition = condition;
			}
			
			public override IEnumerator Yield()
			{
				while( condition.ConditionMet == false )
				{
					yield return null;
				}
				
				yield break;
			}
		}

		//----------------------------------------------------------------------------------
		// Special yields

		public class ErrorBreak : YieldInstruction
		{
			// Essentially the same as yield break; But can be clearer in code
			public string errorMessage = "";

			public ErrorBreak()
			{
			}
			public ErrorBreak( string message )
			{
				errorMessage = message;
			}
		}
		public static ErrorBreak error { get{ return new ErrorBreak(); } }

		//----------------------------------------------------------------------------------

		public class CompleteBreak : YieldInstruction
		{
			// Essentially the same as yield break; But can be clearer in code
			public string message = "";
			
			public CompleteBreak()
			{
			}
			public CompleteBreak( string message )
			{
				this.message = message;
			}
		}
		public static CompleteBreak completed { get{ return new CompleteBreak(); } }

		//----------------------------------------------------------------------------------
		
		public class NullStep : YieldInstruction
		{
			// Essentially the same as yield break; But can be clearer in code
			public string message = "";
			
			public NullStep()
			{
			}
			public NullStep( string message )
			{
				this.message = message;
			}
		}
		public static NullStep nullStep { get{ return new NullStep(); } }

		//----------------------------------------------------------------------------------

		public class CancelBreak : YieldInstruction
		{
			// Essentially the same as yield break; But can be clearer in code
			public string message = "";
			
			public CancelBreak()
			{
			}
			public CancelBreak( string message )
			{
				this.message = message;
			}
		}
		public static CancelBreak cancel { get{ return new CancelBreak(); } }

		//----------------------------------------------------------------------------------

		///<summary>Breaks out of the extended CoroutineEx and continues as standard unity coroutine.
		/// this will then only allow standard unity YieldInstructions.
		/// This previous CoroutineEx will be set to inactive.</summary>
		public class ContinueBreak : YieldInstruction
		{
			// The MonoBehaviourEx will break out of the current CoroutineEx and Begin a new one
			// This is useful if waiting for a Coroutine to finish elsewhere which is watching the coroutine but want
			// it to continue
		}
		///<summary>Breaks out of the extended CoroutineEx and continues as standard unity coroutine.
		/// this will then only allow standard unity YieldInstructions.
		/// This previous CoroutineEx will be set to inactive.</summary>
		public static ContinueBreak continueBreak { get{ return new ContinueBreak(); } }

		//----------------------------------------------------------------------------------

		public class PauseBreak : YieldInstruction
		{
			// This will cause the Coroutine to pause.
			// This must however be resumed manually as the routine will not continue.
		}
		public static PauseBreak pauseBreak { get{ return new PauseBreak(); } }

	}
}