﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay
{
	/// <summary>
	/// Base type for MonoBehaviours that you want a Dictionary stored by referenceID to be easily accessable
	/// example, public class SomeClassBehaviour : RefMonoBehaviour<SomeClassBehaviour>
	/// </summary>
	public class RefMonoBehaviour<T> : MonoBehaviourEx where T : MonoBehaviour
	{
		protected enum State { AWOKEN = 0, ENABLED, DISABLED }

		protected struct ReferenceState
		{
			public T obj;
			public State currentState;

			public ReferenceState( T obj )
			{
				this.obj = obj;
				currentState = State.AWOKEN;
			}
		}


		/// <summary>
		/// Static Dictionary of the type that the RefMonoBehaviour is inherited and set the Type to.
		/// </summary>
		protected static Dictionary<string, ReferenceState> references = new Dictionary<string, ReferenceState>();

		/// <summary>
		/// Gets the global object found, else returns null.
		/// </summary>
		/// <returns>The MonoBehaviour object found (null if not).</returns>
		/// <param name="referenceID">ReferenceID of the object to look for.</param>
		public static T GetGlobal( string referenceID )
		{
			ReferenceState rs = new ReferenceState(default(T));
			if( references.TryGetValue(referenceID, out rs) )
				return rs.obj;
			return null;
		}

		/// <summary>
		/// Gets the global object found, else returns null.
		/// </summary>
		/// <returns>The MonoBehaviour object found (null if not).</returns>
		/// <param name="referenceID">ReferenceID of the object to look for.</param>
		/// <param name="includeDisabled">If set to <c>true</c> include disabled objects (GameObject must have been enabled at least once).</param>
		public static T GetGlobal( string referenceID, bool includeDisabled )
		{
			ReferenceState rs = new ReferenceState(default(T));
			if( references.TryGetValue(referenceID, out rs) )
			{
				if( rs.currentState == State.DISABLED && ! includeDisabled )
					return rs.obj;
			}
			return null;
		}

		//------------------------------------------

		/// <summary>
		/// The referenceID to use for refering to this object within the static.
		/// </summary>
		public string referenceID = null;

		//------------------------------------------

		/// <summary>
		/// Initialise the reference on Awake.
		/// </summary>
		protected virtual void Awake()
		{
			// check to make sure the references does not contain the ID already
			if( references.ContainsKey(referenceID) )
			{
				Debug.LogError("Global " + typeof(T).ToString() + " Referenced already exists, cannot add");
				return;
			}

			T typeObject = null;

			try{ typeObject = this as T; }
			catch{ Debug.LogError("Cannot convert object to Type T, case of incorrect type in inheritance."); }

			// add it to the dictionary
			references.Add(referenceID, new ReferenceState(typeObject));
		}

		/// <summary>
		/// Sets this object within the references to be enabled.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			ReferenceState rs = new ReferenceState(default(T));
			if( references.TryGetValue(referenceID, out rs) )
			{
				if( rs.obj == this )
					rs.currentState = State.ENABLED;
			}
		}
	
		/// <summary>
		/// Sets this object within the references to be disabled.
		/// </summary>
		protected virtual void OnDisable()
		{
			ReferenceState rs = new ReferenceState(default(T));
			if( references.TryGetValue(referenceID, out rs) )
			{
				if( rs.obj == this )
					rs.currentState = State.DISABLED;
			}
		}

		/// <summary>
		/// Removes this object from the references
		/// </summary>
		protected virtual void OnDestroy()
		{
			ReferenceState rs = new ReferenceState(default(T));
			if( references.TryGetValue(referenceID, out rs) )
			{
				if( rs.obj == this )
					references.Remove(referenceID);
			}
		}
	}

}