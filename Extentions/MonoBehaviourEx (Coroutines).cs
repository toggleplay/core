﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TogglePlay;

namespace TogglePlay
{
	public enum ECoroutineArrayOptions
	{
		Asynchronous = 0,
		Synchronous
	}
}

/// <summary>
/// Extended MonoBehaviour class with additional functions.
/// </summary>
public partial class MonoBehaviourEx : MonoBehaviour
{
	/// <summary>
	/// All active CoroutineEx that are running.
	/// </summary>
	List<CoroutineEx> runningRoutines = new List<CoroutineEx>(); // TODO WindowsPhone version

#if UNITY_EDITOR
	List<CoroutineEx> completedRoutines = new List<CoroutineEx>();
#endif

	//----------------------------------------------------------------------------------
	// Coroutine Running Management Functions, Subroutines are run as Unity Coroutines
	//----------------------------------------------------------------------------------

	/// <summary>
	/// CoroutineEx to run as a subroutine of the running routine.
	/// </summary>
	/// <param name="routine">CoroutineEx to run as a subroutine.</param>
	IEnumerator Subroutine( CoroutineEx routine, bool addToRunning = true )
	{
		if( routine == null || ! routine.isRunning )
		{
			yield break;
		}

		if( addToRunning )
			runningRoutines.Add(routine);

		do
		{
			yield return routine.Advance();
		}
		while( routine.isRunning );

		runningRoutines.Remove( routine );
#if UNITY_EDITOR
		completedRoutines.Add( routine );
#endif
		yield break;
	}

	/// <summary>
	/// Queued array of CoroutineEx to run as a subroutine of the running routine.
	/// </summary>
	/// <param name="routines">Array of CoroutineEx to run asynchronously.</param>
	IEnumerator AsynchronousSubroutine( CoroutineEx[] routines )
	{
		for( int i=0; i<routines.Length; ++i )
		{
			if( routines[i] == null || ! routines[i].isRunning )
				continue;

			runningRoutines.Add( routines[i] );
			
			do
			{
				// yielded so that the parent Subroutine will handle any WaitForSeconds etc
				yield return routines[i].Advance();
			}
			while(routines[i].isRunning);
			
			runningRoutines.Remove( routines[i] );
#if UNITY_EDITOR
			completedRoutines.Add( routines[i] );
#endif
		}

		yield break;
	}

	/// <summary>
	/// Keep going with the routine until all the routines in the array have finished
	/// </summary>
	/// <param name="routines">Array of CoroutineEx to run synchronously.</param>
	IEnumerator SynchronousSubroutine( CoroutineEx[] routines )
	{
		bool areRunning = true;
		while( areRunning )
		{
			areRunning = false;

			for( int i=0; i<routines.Length; ++i )
			{
				if( routines[i] != null && routines[i].isRunning )
				{
					areRunning = true;
					break;
				}
			}

			if( areRunning )
				yield return null;
		}

		yield break;
	}

	//----------------------------------------------------------------------------------
	// Object activity management
	//----------------------------------------------------------------------------------

	protected void ReactivateCoroutines()
	{
		for( int i=0; i<runningRoutines.Count; ++i )
			base.StartCoroutine( Subroutine(runningRoutines[i], false) );	
	}

	//----------------------------------------------------------------------------------
	// Starting new running coroutine
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Starts the coroutine and returns an CoroutineEx class for management.
	/// </summary>
	/// <returns>The coroutineEx for manageing the routine.</returns>
	/// <param name="enumerator">IEnumerator to run.</param>
	public new CoroutineEx StartCoroutine( IEnumerator enumerator )
	{
		if( enumerator == null )
		{
			Debug.LogWarning("Cannot start a Coroutine with a null enumerator");
			return null;
		}

		CoroutineEx ex = new CoroutineEx( enumerator, this );

		if( this.gameObject.activeInHierarchy == false )
		{
			// log this?
			runningRoutines.Add(ex);
		}
		else if( ex != null && ex.isRunning )
		{
			base.StartCoroutine( Subroutine(ex) );
		}

		return ex;
	}

	/// <summary>
	/// Starts the coroutine sequence and returns an CoroutineEx class for management.
	/// </summary>
	/// <returns>The coroutineEx for manageing the routine.</returns>
	/// <param name="enumerator">IEnumerators to run in sequence.</param>
	public new CoroutineEx StartCoroutine( IEnumerator[] enumeratorArray, ECoroutineArrayOptions options )
	{
		return options == ECoroutineArrayOptions.Asynchronous ? StartCoroutineSequence( enumeratorArray ) : StartCoroutineArray( enumeratorArray );
	}

	/// <summary>
	/// Starts the coroutine sequence and returns an CoroutineEx class for management.
	/// </summary>
	/// <returns>The coroutineEx for manageing the routine.</returns>
	/// <param name="enumerator">IEnumerators to run in sequence.</param>
	public CoroutineEx StartCoroutineSequence( IEnumerator[] enumeratorSequence )
	{
		if( enumeratorSequence == null )
		{
			Debug.LogWarning("Cannot start a Coroutine sequence with a null enumerator array");
			return null;
		}

		// create an array of CoroutineEx in order
		CoroutineEx[] sequence = new CoroutineEx[enumeratorSequence.Length];
		for( int i=0; i<enumeratorSequence.Length; ++i )
			sequence[i] = new CoroutineEx( enumeratorSequence[i], this );

		// Start a Coroutine to manage the sequence
		return StartCoroutine( AsynchronousSubroutine( sequence ) );
	}

	/// <summary>
	/// Starts all the coroutine at the same time and returns a CoroutineEx class for management.
	/// </summary>
	/// <returns>The coroutineEx for manageing the routine.</returns>
	/// <param name="enumerator">IEnumerators to run in sequence.</param>
	public CoroutineEx StartCoroutineArray( IEnumerator[] enumerators )
	{
		if( enumerators == null )
		{
			Debug.LogWarning("Cannot start a Coroutine sequence with a null enumerator array");
			return null;
		}
		
		CoroutineEx[] coroutineGroup = new CoroutineEx[enumerators.Length];
		for( int i=0; i<enumerators.Length; ++i )
		{
			// begin all coroutines running within unity base coroutines using the subroutine
			coroutineGroup[i] = new CoroutineEx( enumerators[i], this );
			base.StartCoroutine( Subroutine(coroutineGroup[i]) );
		}
		
		return StartCoroutine( SynchronousSubroutine(coroutineGroup) );
	}

	//----------------------------------------------------------------------------------
	// Stopping running Coroutines
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Stops the coroutineEx by name.
	/// </summary>
	/// <param name="coroutineName">name of Coroutine to stop.</param>
	public new void StopCoroutine( string coroutineName )
	{
		for( int i=0; i>runningRoutines.Count; ++i )
		{
			if( runningRoutines[i].name == coroutineName )
				runningRoutines[i].Stop();
		}
	}

	/// <summary>
	/// Triggers all coroutinesEx that are running to stop.
	/// </summary>
	public new void StopAllCoroutines()
	{
		for( int i=0; i<runningRoutines.Count; ++i )
			runningRoutines[i].Stop();
	}

	/// <summary>
	/// Stops all coroutinesEx that are running that are returned true from the Predicate.
	/// </summary>
	/// <param name="match">The predicate to check to see what matches.</param>
	public new void StopAllCoroutines( System.Predicate<CoroutineEx> match )
	{
		for( int i=0; i<runningRoutines.Count; ++i )
		{
			if( match.Invoke( runningRoutines[i] ) )
				runningRoutines[i].Stop();
		}
	}

	//----------------------------------------------------------------------------------
	// Pausing Coroutines
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Pauses the coroutineEx that is running by name.
	/// </summary>
	/// <param name="coroutineName">Coroutine name.</param>
	public void PauseCoroutine( string coroutineName )
	{
		for( int i=0; i>runningRoutines.Count; ++i )
		{
			if( runningRoutines[i].name == coroutineName )
				runningRoutines[i].Pause();
		}
	}

	/// <summary>
	/// Pauses all running CoroutineEx.
	/// </summary>
	public void PauseAllCoroutines()
	{
		for( int i=0; i>runningRoutines.Count; ++i )
			runningRoutines[i].Pause();
	}

	/// <summary>
	/// Pauses all coroutinesEx that the Predicate defines as true.
	/// </summary>
	/// <param name="match">Predicate to check for which Coroutines to pause.</param>
	public void PauseAllCoroutines( System.Predicate<CoroutineEx> match )
	{
		for( int i=0; i>runningRoutines.Count; ++i )
		{
			if( match.Invoke(runningRoutines[i]) )
				runningRoutines[i].Pause();
		}
	}

	//----------------------------------------------------------------------------------
	// Resumeing Coroutine functions
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Resumes the running coroutineEx by name.
	/// </summary>
	/// <param name="coroutineName">Coroutine name.</param>
	public void ResumeCoroutine( string coroutineName )
	{
		for( int i=0; i>runningRoutines.Count; ++i )
		{
			if( runningRoutines[i].name == coroutineName )
				runningRoutines[i].Resume();
		}
	}

	/// <summary>
	/// Resumes all coroutineEx running on this MonoBehaviourEx.
	/// </summary>
	public void ResumeAllCoroutines()
	{
		for( int i=0; i>runningRoutines.Count; ++i )
			runningRoutines[i].Resume();
	}

	/// <summary>
	/// Resumes all coroutineEx that are running and the Predicate 'match' returns true.
	/// </summary>
	/// <param name="match">Predicate to check with.</param>
	public void ResumeAllCoroutines( System.Predicate<CoroutineEx> match )
	{
		for( int i=0; i>runningRoutines.Count; ++i )
		{
			if( match.Invoke(runningRoutines[i]) )
				runningRoutines[i].Resume();
		}
	}

	//----------------------------------------------------------------------------------
}
