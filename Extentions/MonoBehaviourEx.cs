﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public partial class MonoBehaviourEx : MonoBehaviour
{
	protected virtual void OnEnable()
	{
		ReactivateCoroutines();
	}
}
