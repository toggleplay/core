/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Coroutine class used to manage a running coroutine.
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay
{
	/// <summary>
	/// Extended Coroutine class for manageing the running routine.
	/// CoroutineEx can only be yielded within another CoroutineEx, not unity standard Coroutine
	/// </summary>
	[System.Serializable]
	public class CoroutineEx : Yield.EnumeratedYield
	{
		// the function name of the coroutine being run
		string functionName = "Unknown";

		// The Coroutine that is being run
		IEnumerator myCoroutine = null;

		// any Yield.EnumeratedYields yielded within the routine
		IEnumerator subCoroutine = null;

		// indicates if the coroutine is runing or not
		bool active = true;

		// indicates if the corotine is paused and should not proceed with the advancing
		bool paused = false;

#if UNITY_EDITOR
		// used for debug purposes so can see when it began
		#pragma warning disable 0414
		float routineStartedTime = 0;
		float runningTime = 0;
		List<string> logs = new List<string>();
		#pragma warning restore 0414
#endif

		/// <summary>
		/// the MonoBehaviour that starts the Coroutine, so can continue on it if needed.
		/// This is useful when sending the CoroutineEx instance between classes and need to know
		/// which MonoBehaviourEx is managing the CoroutineEx.
		/// </summary>
		[HideInInspector] MonoBehaviourEx fromMonoBehaviour = null;

		/// <summary>Keep a reference to the Yield.EnumeratedYield for performance.</summary>
		protected static System.Type enumYieldType = typeof(Yield.EnumeratedYield);

		//----------------------------------------------------------------------------------
		// Constructors
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Initializes a new instance of the <see cref="TogglePlay.CoroutineEx"/> class.
		/// </summary>
		/// <param name="coroutine">Coroutine to begin.</param>
		/// <param name="calledFrom">Start on which monobehaviour.</param>
		public CoroutineEx( IEnumerator coroutine, MonoBehaviourEx calledFrom )
		{
			myCoroutine = coroutine;
			fromMonoBehaviour = calledFrom;

			if( coroutine == null )
			{
				active = false;
				return;
			}

			functionName = coroutine.GetType().Name;

			// get name inbetween the  < and > that gives the function name called for the specified IEnumerator type
			int start = functionName.IndexOf('<') + 1;
			int length = functionName.IndexOf('>') - start;
			functionName = functionName.Substring(start, length);

#if UNITY_EDITOR
			routineStartedTime = Time.time;
#endif
		}
		
		//----------------------------------------------------------------------------------
		// Properties
		//----------------------------------------------------------------------------------

		/// <summary>
		/// The name of the function running as Coroutine.
		/// </summary>
		public string name
		{
			get{ return functionName; }
		}

		/// <summary>
		/// If the CoroutineEx is ative, paused means it is still active but paused.
		/// </summary>
		/// <value><c>true</c> if is running; otherwise, <c>false</c>.</value>
		public bool isRunning
		{
			get{ return active; }
		}

		/// <summary>
		/// If the CoroutineEx has been paused.
		/// </summary>
		/// <value><c>true</c> if is paused; otherwise, <c>false</c>.</value>
		public bool isPaused
		{
			get{ return paused; }
		}
		
		//----------------------------------------------------------------------------------
		// General
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Tells the Coroutine to stop running and remove itself.
		/// </summary>
		public void Stop()
		{
			active = false;
		}

		/// <summary>
		/// Pause the running of this coroutine.
		/// </summary>
		public void Pause()
		{
			paused = true;
		}

		/// <summary>
		/// Resumes the running of the Coroutine.
		/// </summary>
		public void Resume()
		{
			paused = false;
		}
		
		//----------------------------------------------------------------------------------
		// Advancing the coroutine
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Will continue yielding null, until this coroutine is stopped.
		/// Yield is taken
		/// </summary>
		public override IEnumerator Yield()
		{
			while( active )
				yield return null;
		}

		/// <summary>
		/// Advance this routine by a step. If it is paused it will return null.
		/// </summary>
		public YieldInstruction Advance()
		{
			if( myCoroutine == null )
			{
				active = false;
				return null;
			}

#if UNITY_EDITOR
			runningTime = Time.time - routineStartedTime;
#endif

			if( isPaused )
				return null;

			// if there is a enumerated yield do here
			if( subCoroutine != null )
			{
				if( subCoroutine.MoveNext() == false )
					subCoroutine = null;
				else
					return subCoroutine.Current as YieldInstruction;
			}

			// if the coroutine has ended set the coroutine to inactive
			if( ! myCoroutine.MoveNext() )
				active = false;

			object c = myCoroutine.Current;
			if( c == null )
				return null;

			// get the instruction of the current yield
			YieldInstruction instruction = c as YieldInstruction;
			if( instruction == null )
			{
				IYieldable iy = c as IYieldable;
				if( iy != null )
					instruction = new Yield.WaitForYeildable(iy);
				else
					return instruction;
			}
			System.Type type = instruction.GetType();

			// check to see if it is to be a subroutine
			if( type.IsSubclassOf( enumYieldType ) )
			{
				Yield.EnumeratedYield runningYield = instruction as Yield.EnumeratedYield;
				subCoroutine = runningYield.Yield();
				if( subCoroutine.MoveNext() == false )
					subCoroutine = null;
				else
					return subCoroutine.Current as YieldInstruction;
			}

			// Process any system YieldInstruction types
			switch( type.Name )
			{
			case "NullStep":
#if UNITY_EDITOR
				TogglePlay.Yield.NullStep nullStep = instruction as TogglePlay.Yield.NullStep;
				if( ! string.IsNullOrEmpty( nullStep.message ) )
					logs.Add( nullStep.message );
#endif
				Stop();
				return instruction;

			case "ContinueBreak":
				// start new coroutine on calling MonoBehaviour, exit this one
				// after a continue break, only works with standard unity YieldInstructions
				if( fromMonoBehaviour != null )
				{
					Stop();
					fromMonoBehaviour.StartCoroutine(myCoroutine);
				}
				else
					Debug.LogError("Cannot continue break CoroutineEx with a null calling MonoBehaviour");
				return instruction;

			case "PauseBreak":
				Pause();
				return instruction;

			case "CancelBreak":
#if UNITY_EDITOR
				TogglePlay.Yield.CancelBreak cancelCast = instruction as TogglePlay.Yield.CancelBreak;
				if( ! string.IsNullOrEmpty( cancelCast.message ) )
					logs.Add( cancelCast.message );
#endif
				Stop();
				return instruction;

			case "CompleteBreak":
#if UNITY_EDITOR
				TogglePlay.Yield.CompleteBreak completeCast = instruction as TogglePlay.Yield.CompleteBreak;
				if( ! string.IsNullOrEmpty( completeCast.message ) )
					logs.Add( completeCast.message );
#endif
				Stop();
				return instruction;

			case "ErrorBreak":
				TogglePlay.Yield.ErrorBreak eBreak = instruction as TogglePlay.Yield.ErrorBreak;
#if UNITY_EDITOR
				if( ! string.IsNullOrEmpty( eBreak.errorMessage ) )
				{
					logs.Add( eBreak.errorMessage );

					if( fromMonoBehaviour != null )
						Debug.LogError(eBreak != null ? "Error" : eBreak.errorMessage, fromMonoBehaviour);
					else
						Debug.LogError(eBreak != null ? "Error" : eBreak.errorMessage);
				}
#endif
				Stop();
				return instruction;
			}

			// any unity manage instructions would return here as instruction. (WaitForSeconds, WaitForFixedUpdate etc)
			return instruction;
		}
	}
}
