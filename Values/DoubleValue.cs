﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class DoubleValue : System.IComparable
	{
		private class SortAscendingHelper: IComparer<DoubleValue>
		{
			int IComparer<DoubleValue>.Compare(DoubleValue a, DoubleValue b)
			{
				if( a == null )
					return -1;

				return a.CompareTo(b);
			}
		}
		public static IComparer<DoubleValue> sortAscending
		{
			get
			{
				return (IComparer<DoubleValue>) new SortAscendingHelper();
			}
		}

		private class SortDescendingHelper: IComparer<DoubleValue>
		{
			int IComparer<DoubleValue>.Compare(DoubleValue a, DoubleValue b)
			{
				if( a == null )
					return 1;

				return -a.CompareTo(b);
			}
		}
		public static IComparer<DoubleValue> sortDescending
		{
			get
			{
				return (IComparer<DoubleValue>) new SortDescendingHelper();
			}
		}

		private class SortByPropertyHelper : IComparer<DoubleValue>
		{
			int IComparer<DoubleValue>.Compare(DoubleValue a, DoubleValue b)
			{
				if( a == null && b == null )
					return 0;
				else if( b == null )
					return 1;
				else if( a == null )
					return -1;

				return ((int)a.pType).CompareTo((int)b.pType);
			}
		}
		public static IComparer<DoubleValue> sortByProperty
		{
			get
			{
				return (IComparer<DoubleValue>) new SortByPropertyHelper();
			}
		}

		//-----------------------------------------------

		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating,
			alongCurve, alongCurveRepeating, randomAlongTwoCurves, randomAlongTwoCurvesRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };
		public enum ECurvePropertyType { alongCurve = 3, alongCurveRepeating };
		public enum ETwoCurvesPropertyType { randomAlongTwoCurves = 5, randomAlongTwoCurvesRepeating };

		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] private double constantA = 0;
		public double constantValue { get{ return constantA; } }
		public double rangeFrom { get{ return constantA; } }

		[SerializeField] private double constantB = 0;
		public double rangeTo { get{ return constantB; } }

		[SerializeField] private AnimationCurve curveA = new AnimationCurve( new Keyframe[] { new Keyframe(0,0), new Keyframe(1,0) } );
		[SerializeField] private AnimationCurve curveB = new AnimationCurve( new Keyframe[] { new Keyframe(0,0), new Keyframe(1,0) } );

		double? storedValue = null;
		float curveTime = 0;

		public DoubleValue()
		{
		}

		public DoubleValue( double constant )
		{
			Set(constant);
		}
		public DoubleValue( EConstantPropertyType t, double conA, double conB )
		{
			Set(t, conA, conB);
		}

		public DoubleValue( ECurvePropertyType t, AnimationCurve onCurve )
		{
			Set( t, onCurve );
		}
		
		public DoubleValue( ETwoCurvesPropertyType t, AnimationCurve onCurveA, AnimationCurve onCurveB )
		{
			Set( t, onCurveA, onCurveB );
		}

		public void Set( double constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}

		public void Set( EConstantPropertyType t, double conA, double conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}

		public void Set( ECurvePropertyType t, AnimationCurve onCurve )
		{
			pType = (EPropertyType)((int)t);
			curveA = onCurve;
			curveB = onCurve;
			storedValue = null;
		}

		public void Set( ETwoCurvesPropertyType t, AnimationCurve onCurveA, AnimationCurve onCurveB )
		{
			pType = (EPropertyType)((int)t);
			curveA = onCurveA;
			curveB = onCurveB;
			storedValue = null;
		}

		//---------------------------------------

		public int CompareTo(object obj)
		{
			if( obj == null )
				return 1;
			
			DoubleValue otherDoubleVal = obj as DoubleValue;
			if( ! ReferenceEquals( otherDoubleVal, null ) )
				return this.Value.CompareTo(otherDoubleVal.Value);
			else
			{
				double otherDouble = 0;
				try
				{
					otherDouble = (double)obj;
					return this.Value.CompareTo(otherDouble);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a DoubleValue or double");
				}
			}
		}

		//---------------------------------------

		public double Value
		{
			get
			{
				// lots of conversion to double, maybe look at using non unity Random

				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = (double)Random.Range( (float)constantA, (float)constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return (double)Random.Range( (float)constantA, (float)constantB );
				}
				else if( pType == EPropertyType.alongCurve )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					if( curveA.keys.Length == double.NaN )
						return double.NaN;
					storedValue = (double)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.alongCurveRepeating )
				{
					if( curveA.keys.Length == 0 )
						return double.NaN;
					return (double)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
				}
				else if( pType == EPropertyType.randomAlongTwoCurves )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					if( curveA.keys.Length == 0 || curveB.keys.Length == 0)
						return double.NaN;
					
					// if max of A < min of B // or max of B < min of A
					if( curveA.keys[curveA.keys.Length-1].time < curveB.keys[0].time || curveB.keys[curveA.keys.Length-1].time < curveA.keys[0].time )
					{
						curveTime = curveA.keys[curveA.keys.Length-1].time - curveA.keys[0].time;
						curveTime /= curveTime + curveB.keys[curveB.keys.Length-1].time - curveB.keys[0].time;
						
						if( Random.value < curveTime )
							storedValue = (double)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
						else
							storedValue = (double)curveB.Evaluate( Random.Range( curveB.keys[0].time, curveB.keys[curveB.keys.Length-1].time ) );
					}
					// range are in sync, but still may have stragglers
					else
					{
						curveTime = Random.Range(Mathf.Min(curveA.keys[0].time, curveB.keys[0].time),
						                         Mathf.Max(curveA.keys[curveA.keys.Length-1].time, curveB.keys[curveB.length-1].time));
						
						if( curveTime < curveA.keys[0].time || curveTime > curveA.keys[curveA.keys.Length-1].time )
						{
							// just intersecting with curveB
							storedValue = (double)curveB.Evaluate(curveTime);
						}
						else if( curveTime < curveB.keys[0].time || curveTime > curveB.keys[curveB.keys.Length-1].time )
						{
							// just intersecting with curveA
							storedValue = (double)curveA.Evaluate(curveTime);
						}
						else
						{
							// the time range is in cross over of the two curves
							storedValue = (double)Random.Range(curveA.Evaluate(curveTime), curveB.Evaluate(curveTime));
						}
					}
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomAlongTwoCurvesRepeating )
				{
					if( curveA.keys.Length == 0 || curveB.keys.Length == 0 )
						return 0;
					
					// if max of A < min of B // or max of B < min of A
					if( curveA.keys[curveA.keys.Length-1].time < curveB.keys[0].time || curveB.keys[curveA.keys.Length-1].time < curveA.keys[0].time )
					{
						curveTime = curveA.keys[curveA.keys.Length-1].time - curveA.keys[0].time;
						curveTime /= curveTime + curveB.keys[curveB.keys.Length-1].time - curveB.keys[0].time;
						
						if( Random.value < curveTime )
							return (double)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
						else
							return (double)curveB.Evaluate( Random.Range( curveB.keys[0].time, curveB.keys[curveB.keys.Length-1].time ) );
					}
					// range are in sync, but still may have stragglers
					else
					{
						curveTime = Random.Range(Mathf.Min(curveA.keys[0].time, curveB.keys[0].time),
						                         Mathf.Max(curveA.keys[curveA.keys.Length-1].time, curveB.keys[curveB.length-1].time));
						
						if( curveTime < curveA.keys[0].time || curveTime > curveA.keys[curveA.keys.Length-1].time )
						{
							// just intersecting with curveB
							return (double)curveB.Evaluate(curveTime);
						}
						else if( curveTime < curveB.keys[0].time || curveTime > curveB.keys[curveB.keys.Length-1].time )
						{
							// just intersecting with curveA
							return (double)curveA.Evaluate(curveTime);
						}
						else
						{
							// the time range is in cross over of the two curves
							return (double)Random.Range(curveA.Evaluate(curveTime), curveB.Evaluate(curveTime));
						}
					}
				}
				return 0d; // this should never occur
			}
		}

		//***********************************************************  Operators  ***************************

		public override string ToString()
		{
			return string.Format("[DoubleValue: Value={0}]", Value);
		}

		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			DoubleValue dv = (DoubleValue)obj;
			if( (System.Object)dv == null )
				return false;
			return Equals(dv);
		}
		public bool Equals(DoubleValue other)
		{
			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			else if( pType == EPropertyType.alongCurve || pType == EPropertyType.alongCurveRepeating )
			{
				if( curveA.Equals(other.curveA) )
					return true;
			}
			else if( pType == EPropertyType.randomAlongTwoCurves || pType == EPropertyType.randomAlongTwoCurvesRepeating )
			{
				if( !curveA.Equals(other.curveA) )
					return false;
				if( !curveB.Equals(other.curveB) )
					return false;
				return true;
			}
			
			return false;
		}

		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			else if( pType == EPropertyType.alongCurve || pType == EPropertyType.alongCurveRepeating )
			{
				return pType.GetHashCode() ^ curveA.GetHashCode();
			}
			else if( pType == EPropertyType.randomAlongTwoCurves || pType == EPropertyType.randomAlongTwoCurvesRepeating )
			{
				return pType.GetHashCode() ^ curveA.GetHashCode() ^ curveB.GetHashCode();
			}
			
			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode() ^ curveA.GetHashCode() ^ curveB.GetHashCode();
		}

		public static double operator +(DoubleValue dv1, DoubleValue dv2) 
		{
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return double.NaN;
			return (dv1.Value + dv2.Value);
		}
		public static double operator +(DoubleValue dv, float f) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value + (double)f);
		}
		public static double operator +(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value + d);
		}
		public static double operator +(DoubleValue dv, int i) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value + (double)i);
		}
		public static double operator +(float f, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value + (double)f);
		}
		public static double operator +(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value + d);
		}
		public static int operator +(int i, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return i;
			return (int)(dv.Value + (double)i);
		}

		public static double operator -(DoubleValue dv1, DoubleValue dv2) 
		{
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return double.NaN;
			return (dv1.Value - dv2.Value);
		}
		public static double operator -(DoubleValue dv, float f) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value - (double)f);
		}
		public static double operator -(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value - d);
		}
		public static double operator -(DoubleValue dv, int i) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value - (double)i);
		}
		public static double operator -(float f, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value - (double)f);
		}
		public static double operator -(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value - d);
		}
		public static int operator -(int i, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return i;
			return (int)(dv.Value - (double)i);
		}

		public static double operator *(DoubleValue dv1, DoubleValue dv2) 
		{
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return double.NaN;
			return (dv1.Value * dv2.Value);
		}
		public static double operator *(DoubleValue dv, float f) 
		{
			if( (System.Object)dv == null )
				return f;
			return (dv.Value * (double)f);
		}
		public static double operator *(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return d;
			return (dv.Value * d);
		}
		public static double operator *(DoubleValue dv, int i) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value * (double)i);
		}
		public static double operator *(float f, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value * (double)f);
		}
		public static double operator *(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value * d);
		}
		public static int operator *(int i, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return i;
			return (int)(dv.Value * (double)i);
		}

		public static double operator /(DoubleValue dv1, DoubleValue dv2) 
		{
			if( (System.Object)dv1 == null || dv2 == null )
				return double.NaN;
			return (dv1.Value / dv2.Value);
		}
		public static double operator /(DoubleValue dv, float f) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value / (double)f);
		}
		public static double operator /(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value / d);
		}
		public static double operator /(DoubleValue dv, int i) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value / (double)i);
		}
		public static double operator /(float f, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value / (double)f);
		}
		public static double operator /(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (d / dv.Value);
		}
		public static int operator /(int i, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return i;
			return (int)(dv.Value / (double)i);
		}

		public static double operator %(DoubleValue dv1, DoubleValue dv2) 
		{
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return double.NaN;
			return (dv1.Value % dv2.Value);
		}
		public static double operator %(DoubleValue dv, float f) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value % (double)f);
		}
		public static double operator %(DoubleValue dv, double f) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value % f);
		}
		public static double operator %(DoubleValue dv, int i) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value % (double)i);
		}
		public static double operator %(double f, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return double.NaN;
			return (dv.Value % f);
		}



		public static bool operator ==(DoubleValue dv1, DoubleValue dv2) 
		{
			if( System.Object.ReferenceEquals(dv1, dv2) )
				return true;
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return false;
			return (dv1.Value == dv2.Value);
		}
		public static bool operator !=(DoubleValue dv1, DoubleValue dv2) 
		{
			if( System.Object.ReferenceEquals(dv1, dv2) )
				return false;
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return false;
			return !(dv1.Value == dv2.Value);
		}
		
		public static bool operator <(DoubleValue dv1, DoubleValue dv2) 
		{
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return false;
			return (dv1.Value < dv2.Value);
		}
		public static bool operator >(DoubleValue dv1, DoubleValue dv2) 
		{
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return false;
			return (dv1.Value > dv2.Value);
		}
		
		public static bool operator <=(DoubleValue dv1, DoubleValue dv2) 
		{
			if( System.Object.ReferenceEquals(dv1, dv2) )
				return true;
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return false;
			return (dv1.Value <= dv2.Value);
		}
		public static bool operator >=(DoubleValue dv1, DoubleValue dv2) 
		{
			if( System.Object.ReferenceEquals(dv1, dv2) )
				return true;
			if( (System.Object)dv1 == null || (System.Object)dv2 == null )
				return false;
			return (dv1.Value >= dv2.Value);
		}

		public static bool operator ==(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return false;
			return (d == dv.Value);
		}
		public static bool operator !=(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return false;
			return (d != dv.Value);
		}

		public static bool operator <(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return false;
			return (d < dv.Value);
		}
		public static bool operator >(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return false;
			return (d > dv.Value);
		}

		public static bool operator <=(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return false;
			return (d <= dv.Value);
		}
		public static bool operator >=(double d, DoubleValue dv) 
		{
			if( (System.Object)dv == null )
				return false;
			return (d >= dv.Value);
		}

		public static bool operator ==(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return false;
			return (dv.Value == d);
		}
		public static bool operator !=(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return false;
			return (dv.Value != d);
		}

		public static bool operator <(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return false;
			return (dv.Value < d);
		}
		public static bool operator >(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return false;
			return (dv.Value > d);
		}
		
		public static bool operator <=(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return false;
			return (dv.Value <= d);
		}
		public static bool operator >=(DoubleValue dv, double d) 
		{
			if( (System.Object)dv == null )
				return false;
			return (dv.Value >= d);
		}
	}
}
