/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class Vector4Value : System.IComparable
	{
		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };

		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] private Vector4 constantA = Vector4.zero;
		public Vector4 constantValue { get{ return constantA; } }
		public Vector4 rangeFrom { get{ return constantA; } }

		[SerializeField] private Vector4 constantB = Vector4.zero;
		public Vector4 rangeTo { get{ return constantB; } }

		Vector4? storedValue = null;

		public Vector4Value()
		{
		}

		public Vector4Value( Vector4 constant )
		{
			Set(constant);
		}
		public Vector4Value( EConstantPropertyType t, Vector4 conA, Vector4 conB )
		{
			Set(t, conA, conB);
		}

		public void Set( Vector4 constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}

		public void Set( EConstantPropertyType t, Vector4 conA, Vector4 conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}

		//---------------------------------------

		public int CompareTo(object obj)
		{
			if( obj == null )
				return 1;
			
			Vector4Value otherV3Val = obj as Vector4Value;
			if( ! ReferenceEquals( otherV3Val, null ) )
			{
				return this.Value.magnitude.CompareTo(otherV3Val.Value.magnitude);
			}
			else
			{
				Vector4 other = Vector4.zero;
				try
				{
					other = (Vector4)obj;
					return this.Value.magnitude.CompareTo(other.magnitude);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a Vector4Value or Vector4");
				}
			}
		}

		//----------------------------------------------------------------------------------
		// 
		//----------------------------------------------------------------------------------

		public Vector4 Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = RandomVector4( constantA, constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return RandomVector4( constantA, constantB );
				}
				return Vector4.zero; // this should never occur
			}
		}

		//***********************************************************  Operators  ***************************

		public override string ToString()
		{
			return string.Format("[Vector4Value: Value={0}]", Value);
		}

		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			Vector4Value v4v = (Vector4Value)obj;
			if( (System.Object)v4v == null )
				return false;

			return Equals(v4v);
		}
		public bool Equals(Vector4Value other)
		{
			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			
			return false;
		}

		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			
			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
		}

		public static Vector4 operator +(Vector4Value v4v1, Vector4Value v4v2) 
		{
			if( (System.Object)v4v1 == null || (System.Object)v4v2 == null )
				return Vector4.zero;
			Vector4 v1 = v4v1.Value;
			Vector4 v2 = v4v2.Value;
			return v1 + v2;
		}
		public static Vector4 operator +(Vector4Value v4v, float f) 
		{
			if( (System.Object)v4v == null )
				return Vector4.zero;
			Vector4 v = v4v.Value;
			return new Vector4(v.x+f, v.y+f, v.z+f, v.w+f);
		}

		public static Vector4 operator -(Vector4Value v4v1, Vector4Value v4v2) 
		{
			if( (System.Object)v4v1 == null || (System.Object)v4v2 == null )
				return Vector4.zero;
			Vector4 v1 = v4v1.Value;
			Vector4 v2 = v4v2.Value;
			return v1 - v2;
		}
		public static Vector4 operator -(Vector4Value v4v, float f) 
		{
			if( (System.Object)v4v == null )
				return Vector4.zero;
			Vector4 v = v4v.Value;
			return new Vector4(v.x-f, v.y-f, v.z-f, v.w-f);
		}

		public static Vector4 operator *(Vector4Value v4v1, Vector4Value v4v2) 
		{
			if( (System.Object)v4v1 == null || (System.Object)v4v2 == null )
				return Vector4.zero;
			Vector4 v1 = v4v1.Value;
			Vector4 v2 = v4v2.Value;
			return new Vector4(v1.x*v2.x, v1.y*v2.y, v1.z*v2.z, v1.w*v2.w);
		}
		public static Vector4 operator *(Vector4Value v4v, float f) 
		{
			if( (System.Object)v4v == null )
				return Vector4.zero;
			return (v4v.Value * f);
		}


		public static Vector4 operator /(Vector4Value v4v1, Vector4Value v4v2) 
		{
			if( (System.Object)v4v1 == null || v4v2 == null )
				return Vector4.zero;
			Vector4 v1 = v4v1.Value;
			Vector4 v2 = v4v2.Value;
			return new Vector4(v1.x/v2.x, v1.y/v2.y, v1.z/v2.z, v1.w/v2.z);
		}
		public static Vector4 operator /(Vector4Value v4v, float f) 
		{
			if( (System.Object)v4v == null )
				return Vector4.zero;
			return (v4v.Value / f);
		}


		public static bool operator ==(Vector4Value v4v1, Vector4Value v4v2) 
		{
			if( System.Object.ReferenceEquals(v4v1, v4v2) )
				return true;
			if( (System.Object)v4v1 == null || (System.Object)v4v2 == null )
				return false;
			return (v4v1.Value == v4v2.Value);
		}
		public static bool operator !=(Vector4Value v4v1, Vector4Value v4v2) 
		{
			if( System.Object.ReferenceEquals(v4v1, v4v2) )
				return false;
			if( (System.Object)v4v1 == null || (System.Object)v4v2 == null )
				return false;
			return !(v4v1.Value == v4v2.Value);
		}


		public static bool operator ==(Vector4 v4, Vector4Value v4v) 
		{
			if( (System.Object)v4v == null )
				return false;
			return (v4 == v4v.Value);
		}
		public static bool operator !=(Vector4 v4, Vector4Value v4v) 
		{
			if( (System.Object)v4v == null )
				return false;
			return (v4 != v4v.Value);
		}
	

		public static bool operator ==(Vector4Value v4v, Vector4 v4) 
		{
			if( (System.Object)v4v == null )
				return false;
			return (v4v.Value == v4);
		}
		public static bool operator !=(Vector4Value v4v, Vector4 v4) 
		{
			if( (System.Object)v4v == null )
				return false;
			return (v4v.Value != v4);
		}

		//----------------------------------------------------------------------------------

		Vector4 RandomVector4( Vector4 fromVector, Vector4 toVector )
		{
			return new Vector4( Random.Range(fromVector.x, toVector.x), 
			                   Random.Range(fromVector.y, toVector.y), 
			                   Random.Range(fromVector.z, toVector.z),
			                   Random.Range(fromVector.w, toVector.w) );
		}
	}
}
