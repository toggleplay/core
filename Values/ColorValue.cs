/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class ColorValue : System.IComparable
	{
		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating, randomWithinList, randomWithinListRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };
		public enum EListedPropertyType { randomWithinList = 3, randomWithinListRepeating };

		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] private Color constantA = Color.white;
		public Color constantValue { get{ return constantA; } }
		public Color rangeFrom { get{ return constantA; } }

		[SerializeField] private Color constantB = Color.white;
		public Color rangeTo { get{ return constantB; } }

		[SerializeField] private List<Color> listOf = new List<Color>();
		public Color[] colorsInList { get{ return listOf.ToArray(); } }

		Color? storedValue = null;

		public ColorValue()
		{
		}

		public ColorValue( Color constant )
		{
			Set(constant);
		}
		public ColorValue( EConstantPropertyType t, Color conA, Color conB )
		{
			Set(t, conA, conB);
		}

		public void Set( Color constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}

		public void Set( EConstantPropertyType t, Color conA, Color conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}

		public void Set( EListedPropertyType t, List<Color> colorList )
		{
			pType = (EPropertyType)((int)t);
			listOf = colorList;
			storedValue = null;
		}

		//---------------------------------------

		// compares by alpha
		public int CompareTo(object obj)
		{
			if( obj == null )
				return 1;
			
			ColorValue otherCVal = obj as ColorValue;
			if( ! ReferenceEquals( otherCVal, null ) )
			{
				return this.Value.a.CompareTo(otherCVal.Value.a);
			}
			else
			{
				Color other = Color.white;
				try
				{
					other = (Color)obj;
					return this.Value.a.CompareTo(other.a);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a ColorValue or Color");
				}
			}
		}

		//---------------------------------------

		public Color Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = RandomColor( constantA, constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return RandomColor( constantA, constantB );
				}
				else if( pType == EPropertyType.randomWithinList )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					if( listOf.Count == 0 )
						return Color.white;
					storedValue = listOf[Random.Range(0,listOf.Count)];
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomWithinListRepeating )
				{
					return listOf[Random.Range(0,listOf.Count)];
				}

				return Color.white; // this should never occur
			}
		}

		//***********************************************************  Operators  ***************************

		public override string ToString()
		{
			return string.Format("[ColorValue: Value={0}]", Value);
		}

		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			ColorValue cv = (ColorValue)obj;
			if( (System.Object)cv == null )
				return false;

			return Equals(cv);
		}
		public bool Equals(ColorValue other)
		{
			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			
			return false;
		}

		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			else if( pType == EPropertyType.randomWithinList || pType == EPropertyType.randomWithinListRepeating )
			{
				int code = pType.GetHashCode();
				for( int i=0; i<listOf.Count; ++i )
					code = code ^ listOf[i].GetHashCode();
				return code;
			}

			Debug.LogError("Developer error, Code should exit before this point");
			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
		}

		public static Color operator +(ColorValue cv1, ColorValue cv2) 
		{
			if( (System.Object)cv1 == null || (System.Object)cv2 == null )
				return Color.white;
			Color c1 = cv1.Value;
			Color c2 = cv2.Value;
			return c1 + c2;
		}
		public static Color operator +(ColorValue cv, float f) 
		{
			if( (System.Object)cv == null )
				return Color.white;
			Color c = cv.Value;
			return new Color(c.r+f, c.g+f, c.b+f, c.a);
		}

		public static Color operator -(ColorValue cv1, ColorValue cv2) 
		{
			if( (System.Object)cv1 == null || (System.Object)cv2 == null )
				return Color.white;
			Color c1 = cv1.Value;
			Color c2 = cv2.Value;
			return c1 - c2;
		}
		public static Color operator -(ColorValue cv, float f) 
		{
			if( (System.Object)cv == null )
				return Color.white;
			Color c = cv.Value;
			return new Color(c.r-f, c.g-f, c.b-f, c.a);
		}

		public static Color operator *(ColorValue cv1, ColorValue cv2) 
		{
			if( (System.Object)cv1 == null || (System.Object)cv2 == null )
				return Color.white;
			Color c1 = cv1.Value;
			Color c2 = cv2.Value;
			return new Color(c1.r * c2.r, c1.g * c2.g, c1.b * c2.b, c1.a * c2.a);
		}
		public static Color operator *(ColorValue cv, float f) 
		{
			if( (System.Object)cv == null )
				return Color.white;
			return (cv.Value * f);
		}


		public static Color operator /(ColorValue cv1, ColorValue cv2) 
		{
			if( (System.Object)cv1 == null || cv2 == null )
				return Color.white;
			Color c1 = cv1.Value;
			Color c2 = cv2.Value;
			return new Color(c1.r / c2.r, c1.g / c2.g, c1.b / c2.b, c1.a / c2.a);
		}
		public static Color operator /(ColorValue cv, float f) 
		{
			if( (System.Object)cv == null )
				return Color.white;
			return (cv.Value / f);
		}


		public static bool operator ==(ColorValue cv1, ColorValue cv2) 
		{
			if( System.Object.ReferenceEquals(cv1, cv2) )
				return true;
			if( (System.Object)cv1 == null || (System.Object)cv2 == null )
				return false;
			return (cv1.Value == cv2.Value);
		}
		public static bool operator !=(ColorValue cv1, ColorValue cv2) 
		{
			if( System.Object.ReferenceEquals(cv1, cv2) )
				return false;
			if( (System.Object)cv1 == null || (System.Object)cv2 == null )
				return false;
			return !(cv1.Value == cv2.Value);
		}


		public static bool operator ==(Color c, ColorValue cv) 
		{
			if( (System.Object)cv == null )
				return false;
			return (c == cv.Value);
		}
		public static bool operator !=(Color c, ColorValue cv) 
		{
			if( (System.Object)cv == null )
				return false;
			return (c != cv.Value);
		}
	

		public static bool operator ==(ColorValue cv, Color c) 
		{
			if( (System.Object)cv == null )
				return false;
			return (cv.Value == c);
		}
		public static bool operator !=(ColorValue cv, Color c) 
		{
			if( (System.Object)cv == null )
				return false;
			return (cv.Value != c);
		}

		//----------------------------------------------------------------------------------

		Color RandomColor( Color fromColor, Color toColor )
		{
			return new Color( Random.Range(fromColor.r, toColor.r), 
			                 Random.Range(fromColor.g, toColor.g), 
			                 Random.Range(fromColor.b, toColor.b),
			                 Random.Range(fromColor.a, toColor.a) );
		}
	}
}
