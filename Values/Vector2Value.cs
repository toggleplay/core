/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class Vector2Value : System.IComparable
	{
		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };

		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] private Vector2 constantA = Vector2.zero;
		public Vector2 constantValue { get{ return constantA; } }
		public Vector2 rangeFrom { get{ return constantA; } }

		[SerializeField] private Vector2 constantB = Vector2.zero;
		public Vector2 rangeTo { get{ return constantB; } }

		Vector2? storedValue = null;

		public Vector2Value()
		{
		}

		public Vector2Value( Vector2 constant )
		{
			Set(constant);
		}
		public Vector2Value( EConstantPropertyType t, Vector2 conA, Vector2 conB )
		{
			Set(t, conA, conB);
		}

		public void Set( Vector2 constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}

		public void Set( EConstantPropertyType t, Vector2 conA, Vector2 conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}

		//---------------------------------------

		public int CompareTo(object obj)
		{
			if( obj == null )
				return 1;
			
			Vector2Value otherV2Val = obj as Vector2Value;
			if( ! ReferenceEquals( otherV2Val, null ) )
			{
				return this.Value.magnitude.CompareTo(otherV2Val.Value.magnitude);
			}
			else
			{
				Vector2 other = new Vector2(0,0);
				try
				{
					other = (Vector2)obj;
					return this.Value.magnitude.CompareTo(other.magnitude);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a Vector2Value or Vector2");
				}
			}
		}

		//---------------------------------------

		public Vector2 Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = RandomVector2( constantA, constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return RandomVector2( constantA, constantB );
				}
				return Vector2.zero; // this should never occur
			}
		}

		//***********************************************************  Operators  ***************************

		public override string ToString()
		{
			return string.Format("[Vector2Value: Value={0}]", Value);
		}

		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			Vector2Value v2v = (Vector2Value)obj;
			if( (System.Object)v2v == null )
				return false;

			return Equals(v2v);
		}
		public bool Equals(Vector2Value other)
		{
			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			
			return false;
		}

		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			
			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
		}

		public static Vector2 operator +(Vector2Value v2v1, Vector2Value v2v2) 
		{
			if( (System.Object)v2v1 == null || (System.Object)v2v2 == null )
				return Vector2.zero;
			Vector2 v1 = v2v1.Value;
			Vector2 v2 = v2v2.Value;
			return v1 + v2;
		}
		public static Vector2 operator +(Vector2Value v2v, float f) 
		{
			if( (System.Object)v2v == null )
				return Vector2.zero;
			Vector2 v = v2v.Value;
			return new Vector2(v.x+f, v.y+f);
		}

		public static Vector2 operator -(Vector2Value v2v1, Vector2Value v2v2) 
		{
			if( (System.Object)v2v1 == null || (System.Object)v2v2 == null )
				return Vector2.zero;
			Vector2 v1 = v2v1.Value;
			Vector2 v2 = v2v2.Value;
			return v1 - v2;
		}
		public static Vector2 operator -(Vector2Value v2v, float f) 
		{
			if( (System.Object)v2v == null )
				return Vector2.zero;
			Vector2 v = v2v.Value;
			return new Vector2(v.x-f, v.y-f);
		}

		public static Vector2 operator *(Vector2Value v2v1, Vector2Value v2v2) 
		{
			if( (System.Object)v2v1 == null || (System.Object)v2v2 == null )
				return Vector2.zero;
			Vector2 v1 = v2v1.Value;
			Vector2 v2 = v2v2.Value;
			return new Vector2(v1.x * v2.x, v1.y * v2.y);
		}
		public static Vector2 operator *(Vector2Value v2v, float f) 
		{
			if( (System.Object)v2v == null )
				return Vector2.zero;
			return (v2v.Value * f);
		}


		public static Vector2 operator /(Vector2Value v2v1, Vector2Value v2v2) 
		{
			if( (System.Object)v2v1 == null || v2v2 == null )
				return Vector2.zero;
			Vector2 v1 = v2v1.Value;
			Vector2 v2 = v2v2.Value;
			return new Vector2(v1.x / v2.x, v1.y / v2.y);
		}
		public static Vector2 operator /(Vector2Value v2v, float f) 
		{
			if( (System.Object)v2v == null )
				return Vector2.zero;
			return (v2v.Value / f);
		}


		public static bool operator ==(Vector2Value v2v1, Vector2Value v2v2) 
		{
			if( System.Object.ReferenceEquals(v2v1, v2v2) )
				return true;
			if( (System.Object)v2v1 == null || (System.Object)v2v2 == null )
				return false;
			return (v2v1.Value == v2v2.Value);
		}
		public static bool operator !=(Vector2Value v2v1, Vector2Value v2v2) 
		{
			if( System.Object.ReferenceEquals(v2v1, v2v2) )
				return false;
			if( (System.Object)v2v1 == null || (System.Object)v2v2 == null )
				return false;
			return !(v2v1.Value == v2v2.Value);
		}


		public static bool operator ==(Vector2 v2, Vector2Value v2v) 
		{
			if( (System.Object)v2v == null )
				return false;
			return (v2 == v2v.Value);
		}
		public static bool operator !=(Vector2 v2, Vector2Value v2v) 
		{
			if( (System.Object)v2v == null )
				return false;
			return (v2 != v2v.Value);
		}
	

		public static bool operator ==(Vector2Value v2v, Vector2 v2) 
		{
			if( (System.Object)v2v == null )
				return false;
			return (v2v.Value == v2);
		}
		public static bool operator !=(Vector2Value v2v, Vector2 v2) 
		{
			if( (System.Object)v2v == null )
				return false;
			return (v2v.Value != v2);
		}

		//----------------------------------------------------------------------------------

		Vector2 RandomVector2( Vector2 fromVector, Vector2 toVector )
		{
			return new Vector2( Random.Range(fromVector.x, toVector.x),
			                   Random.Range(fromVector.y, toVector.y) );
		}
	}
}
