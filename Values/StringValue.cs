﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class StringValue : System.IComparable
	{
		public enum EPropertyType { constant = 0, random, randomRepeating };
		public enum ERandomPropertyType { random = 1, randomRepeating };

		[SerializeField] protected EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] protected string constantValue = "";
		[SerializeField] protected string[] stringArray = new string[0];
		[SerializeField] protected List<string> lstStrings = new List<string>();
		public string[] strings
		{
			get
			{
				if( stringArray == null || stringArray.Length == 0 )
					return new string[0];

				string[] strs = new string[stringArray.Length];
				for( int i=0; i<stringArray.Length; ++i )
					strs[i] = stringArray[i];
				return strs;
			}
			protected set
			{
				if( value == null || value.Length == 0 )
				{
					stringArray = new string[0];
					return;
				}

				stringArray = new string[value.Length];
				for( int i=0; i<value.Length; ++i )
					stringArray[i] = value[i];
			}
		}

		public string this[int id]
		{
			get
			{
				if( id < 0 || id >= strings.Length )
					return null;

				return strings[id];
			}
		}

		protected string storedValue = null;


		public StringValue( string constant )
		{
			Set(constant);
		}
		public StringValue( ERandomPropertyType t )
		{
			Set(t);
		}

		public StringValue( ERandomPropertyType t, string[] fromStrings )
		{
			Set(t, fromStrings);
		}
		
		public void Set( string constant )
		{
			pType = EPropertyType.constant;
			constantValue = constant;
			storedValue = null;
		}
		public void Set( ERandomPropertyType t )
		{
			pType = (EPropertyType)((int)t);
			storedValue = null;
		}
		public void Set( ERandomPropertyType t, string[] fromStrings )
		{
			pType = (EPropertyType)((int)t);
			strings = fromStrings;
			storedValue = null;
		}

		//----------------------------------------------

		public int CompareTo(object obj)
		{
			if (obj == null)
				return 1;

			StringValue otherStringValue = obj as StringValue;
			if( ! ReferenceEquals(otherStringValue, null) )
				return this.Value.CompareTo(otherStringValue.Value);
			else
			{
				string otherString = null;
				try
				{
					otherString = (string)obj;
					return this.Value.CompareTo(otherString);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a StringValue or string");
				}
			}
		}

		//----------------------------------------------
		
		public string Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantValue;
				}
				else if( pType == EPropertyType.random )
				{
					if( ! string.IsNullOrEmpty(storedValue) )
						return storedValue;
					if( stringArray.Length == 0 )
						return string.Empty;
					storedValue = stringArray[Random.Range(0,stringArray.Length)];
				}
				else if( pType == EPropertyType.randomRepeating )
				{
					if( stringArray.Length == 0 )
						return string.Empty;
					return stringArray[Random.Range(0,stringArray.Length)];
				}
				return storedValue;
			}
		}
		
		//***********************************************************  Operators  ***************************
		
		public override string ToString()
		{
			return string.Format("[StringValue: Value : {0}]", Value);
		}
		
		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			StringValue bv = obj as StringValue;
			if( (System.Object)bv == null )
				return false;
			
			return Equals(bv);
		}
		public bool Equals(StringValue other)
		{
			if( System.Object.ReferenceEquals(this, other) )
				return true;
			if( (System.Object)other == null )
				return false;

			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant || pType == EPropertyType.random )
				return storedValue == other.storedValue;

			return false;
		}
		
		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			return pType.GetHashCode() ^ storedValue.GetHashCode();
		}

		
		
		public static bool operator ==(StringValue sv1, StringValue sv2) 
		{
			if( System.Object.ReferenceEquals(sv1, sv2) )
				return true;
			if( (System.Object)sv1 == null || (System.Object)sv2 == null )
				return false;
			return (sv1.Value == sv2.Value);
		}
		public static bool operator !=(StringValue sv1, StringValue sv2) 
		{
			if( System.Object.ReferenceEquals(sv1, sv2) )
				return false;
			if( (System.Object)sv1 == null || (System.Object)sv2 == null )
				return false;
			return !(sv1.Value == sv2.Value);
		}

		
		public static bool operator ==(string str, StringValue sv) 
		{
			if( (System.Object)sv == null )
				return false;
			return (str == sv.Value);
		}
		public static bool operator !=(string str, StringValue sv) 
		{
			if( (System.Object)sv == null )
				return false;
			return (str != sv.Value);
		}

		public static bool operator ==(StringValue sv, string str) 
		{
			if( (System.Object)sv == null )
				return false;
			return (sv.Value == str);
		}
		public static bool operator !=(StringValue sv, string str) 
		{
			if( (System.Object)sv == null )
				return false;
			return (sv.Value != str);
		}
	}
}
