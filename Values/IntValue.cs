﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class IntValue : System.IComparable
	{
		private class SortAscendingHelper : IComparer<IntValue>
		{
			int IComparer<IntValue>.Compare(IntValue a, IntValue b)
			{
				if( a == null )
					return -1;
				
				return a.CompareTo(b);
			}
		}
		public static IComparer<IntValue> sortAscending
		{
			get{ return (IComparer<IntValue>) new SortAscendingHelper(); }
		}
		
		private class SortDescendingHelper: IComparer<IntValue>
		{
			int IComparer<IntValue>.Compare(IntValue a, IntValue b)
			{
				if( a == null )
					return 1;
				
				return -a.CompareTo(b);
			}
		}
		public static IComparer<IntValue> sortDescending
		{
			get{ return (IComparer<IntValue>) new SortDescendingHelper(); }
		}

		private class SortByPropertyHelper : IComparer<IntValue>
		{
			int IComparer<IntValue>.Compare(IntValue a, IntValue b)
			{
				if( a == null && b == null )
					return 0;
				else if( b == null )
					return 1;
				else if( a == null )
					return -1;
				
				return ((int)a.pType).CompareTo((int)b.pType);
			}
		}
		public static IComparer<IntValue> sortByProperty
		{
			get{ return (IComparer<IntValue>) new SortByPropertyHelper(); }
		}
		
		//----------------------------------------------------------------------------------

		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating,
			alongCurve, alongCurveRepeating, randomAlongTwoCurves, randomAlongTwoCurvesRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };
		public enum ECurvePropertyType { alongCurve = 3, alongCurveRepeating };
		public enum ETwoCurvesPropertyType { randomAlongTwoCurves = 5, randomAlongTwoCurvesRepeating };
		
		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }
		
		[SerializeField] private int constantA = 0;
		public int constantValue { get{ return constantA; } }
		public int rangeFrom { get{ return constantA; } }
		
		[SerializeField] private int constantB = 0;
		public int rangeTo { get{ return constantB; } }
		
		[SerializeField] private AnimationCurve curveA = new AnimationCurve( new Keyframe[] { new Keyframe(0,0), new Keyframe(2,0) } );
		[SerializeField] private AnimationCurve curveB = new AnimationCurve( new Keyframe[] { new Keyframe(0,0), new Keyframe(2,0) } );
		
		int? storedValue = null;
		float curveTime = 0;
		
		public IntValue()
		{
		}
		
		public IntValue( int constant )
		{
			Set(constant);
		}
		public IntValue( EConstantPropertyType t, int conA, int conB )
		{
			Set(t, conA, conB);
		}
		
		public IntValue( ECurvePropertyType t, AnimationCurve onCurve )
		{
			Set( t, onCurve );
		}
		
		public IntValue( ETwoCurvesPropertyType t, AnimationCurve onCurveA, AnimationCurve onCurveB )
		{
			Set( t, onCurveA, onCurveB );
		}
		
		public void Set( int constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}
		
		public void Set( EConstantPropertyType t, int conA, int conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}
		
		public void Set( ECurvePropertyType t, AnimationCurve onCurve )
		{
			pType = (EPropertyType)((int)t);
			curveA = onCurve;
			curveB = onCurve;
			storedValue = null;
		}
		
		public void Set( ETwoCurvesPropertyType t, AnimationCurve onCurveA, AnimationCurve onCurveB )
		{
			pType = (EPropertyType)((int)t);
			curveA = onCurveA;
			curveB = onCurveB;
			storedValue = null;
		}

		//----------------------------------------------

		public int CompareTo(object obj)
		{
			if (obj == null)
				return 1;

			IntValue otherIntValue = obj as IntValue;
			if( ! ReferenceEquals(otherIntValue, null) )
				return this.Value.CompareTo(otherIntValue.Value);
			else
			{
				int otherInt = 0;
				try
				{
					otherInt = (int)obj;
					return this.Value.CompareTo(otherInt);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a IntValue or int");
				}
			}
		}

		//----------------------------------------------
		
		public int Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = Random.Range( constantA, constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return Random.Range( constantA, constantB );
				}
				else if( pType == EPropertyType.alongCurve )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					if( curveA.keys.Length == 0 )
						return 0;
					storedValue = (int)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.alongCurveRepeating )
				{
					if( curveA.keys.Length == 0 )
						return 0;
					return (int)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
				}
				else if( pType == EPropertyType.randomAlongTwoCurves )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					if( curveA.keys.Length == 0 || curveB.keys.Length == 0)
						return 0;
					
					// if max of A < min of B // or max of B < min of A
					if( curveA.keys[curveA.keys.Length-1].time < curveB.keys[0].time || curveB.keys[curveA.keys.Length-1].time < curveA.keys[0].time )
					{
						curveTime = curveA.keys[curveA.keys.Length-1].time - curveA.keys[0].time;
						curveTime /= curveTime + curveB.keys[curveB.keys.Length-1].time - curveB.keys[0].time;
						
						if( Random.value < curveTime )
							storedValue = (int)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
						else
							storedValue = (int)curveB.Evaluate( Random.Range( curveB.keys[0].time, curveB.keys[curveB.keys.Length-1].time ) );
					}
					// range are in sync, but still may have stragglers
					else
					{
						curveTime = Random.Range(Mathf.Min(curveA.keys[0].time, curveB.keys[0].time),
						                         Mathf.Max(curveA.keys[curveA.keys.Length-1].time, curveB.keys[curveB.length-1].time));

						if( curveTime < curveA.keys[0].time || curveTime > curveA.keys[curveA.keys.Length-1].time )
						{
							// just intersecting with curveB
							storedValue = (int)curveB.Evaluate(curveTime);
						}
						else if( curveTime < curveB.keys[0].time || curveTime > curveB.keys[curveB.keys.Length-1].time )
						{
							// just intersecting with curveA
							storedValue = (int)curveA.Evaluate(curveTime);
						}
						else
						{
							// the time range is in cross over of the two curves
							storedValue = (int)Random.Range(curveA.Evaluate(curveTime), curveB.Evaluate(curveTime));
						}
					}
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomAlongTwoCurvesRepeating )
				{
					if( curveA.keys.Length == 0 || curveB.keys.Length == 0)
						return 0;

					// if max of A < min of B // or max of B < min of A
					if( curveA.keys[curveA.keys.Length-1].time < curveB.keys[0].time || curveB.keys[curveA.keys.Length-1].time < curveA.keys[0].time )
					{
						curveTime = curveA.keys[curveA.keys.Length-1].time - curveA.keys[0].time;
						curveTime /= curveTime + curveB.keys[curveB.keys.Length-1].time - curveB.keys[0].time;
						
						if( Random.value < curveTime )
							return (int)curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
						else
							return (int)curveB.Evaluate( Random.Range( curveB.keys[0].time, curveB.keys[curveB.keys.Length-1].time ) );
					}
					// range are in sync, but still may have stragglers
					else
					{
						curveTime = Random.Range(Mathf.Min(curveA.keys[0].time, curveB.keys[0].time),
						                         Mathf.Max(curveA.keys[curveA.keys.Length-1].time, curveB.keys[curveB.length-1].time));
						
						if( curveTime < curveA.keys[0].time || curveTime > curveA.keys[curveA.keys.Length-1].time )
						{
							// just intersecting with curveB
							return (int)curveB.Evaluate(curveTime);
						}
						else if( curveTime < curveB.keys[0].time || curveTime > curveB.keys[curveB.keys.Length-1].time )
						{
							// just intersecting with curveA
							return (int)curveA.Evaluate(curveTime);
						}
						else
						{
							// the time range is in cross over of the two curves
							return (int)Random.Range(curveA.Evaluate(curveTime), curveB.Evaluate(curveTime));
						}
					}
				}
				return 0; // this should never occur
			}
		}
		
		//***********************************************************  Operators  ***************************
		
		public override string ToString()
		{
			return string.Format("[IntValue: Value : {0}]", Value);
		}
		
		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			IntValue iv = obj as IntValue;
			if( (System.Object)iv == null )
				return false;
			
			return Equals(iv);
		}
		public bool Equals(IntValue other)
		{
			if( System.Object.ReferenceEquals(this, other) )
				return true;
			if( (System.Object)other == null )
				return false;

			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			else if( pType == EPropertyType.alongCurve || pType == EPropertyType.alongCurveRepeating )
			{
				if( curveA.Equals(other.curveA) )
					return true;
			}
			else if( pType == EPropertyType.randomAlongTwoCurves || pType == EPropertyType.randomAlongTwoCurvesRepeating )
			{
				if( !curveA.Equals(other.curveA) )
					return false;
				if( !curveB.Equals(other.curveB) )
					return false;
				return true;
			}

			return false;
		}
		
		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			else if( pType == EPropertyType.alongCurve || pType == EPropertyType.alongCurveRepeating )
			{
				return pType.GetHashCode() ^ curveA.GetHashCode();
			}
			else if( pType == EPropertyType.randomAlongTwoCurves || pType == EPropertyType.randomAlongTwoCurvesRepeating )
			{
				return pType.GetHashCode() ^ curveA.GetHashCode() ^ curveB.GetHashCode();
			}

			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode() ^ curveA.GetHashCode() ^ curveB.GetHashCode();
		}
		
		public static int operator +(IntValue iv1, IntValue iv2) 
		{
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return 0;
			return (iv1.Value + iv2.Value);
		}
		public static int operator +(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return 0;
			return (iv.Value + i);
		}
		public static int operator +(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return i;
			return (iv.Value + i);
		}

		public static int operator -(IntValue iv1, IntValue iv2) 
		{
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return 0;
			return (iv1.Value - iv2.Value);
		}
		public static int operator -(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return 0;
			return (iv.Value - i);
		}
		public static int operator -(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return i;
			return (iv.Value - i);
		}
		
		public static int operator *(IntValue iv1, IntValue iv2) 
		{
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return 0;
			return (iv1.Value * iv2.Value);
		}
		public static int operator *(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return 0;
			return (iv.Value * i);
		}
		public static int operator *(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return i;
			return (iv.Value * i);
		}
		
		public static int operator /(IntValue iv1, IntValue iv2) 
		{
			if( (System.Object)iv1 == null || iv2 == null )
				return 0;
			return (iv1.Value / iv2.Value);
		}
		public static int operator /(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return 0;
			return (iv.Value / i);
		}
		public static int operator /(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return i;
			return (iv.Value / i);
		}
		
		public static int operator %(IntValue iv1, IntValue iv2) 
		{
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return 0;
			return (iv1.Value % iv2.Value);
		}
		public static int operator %(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return 0;
			return (iv.Value % i);
		}
		public static int operator %(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return 0;
			return (i % iv.Value);
		}
		
		
		public static bool operator ==(IntValue iv1, IntValue iv2) 
		{
			if( System.Object.ReferenceEquals(iv1, iv2) )
				return true;
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return false;
			return (iv1.Value == iv2.Value);
		}
		public static bool operator !=(IntValue iv1, IntValue iv2) 
		{
			if( System.Object.ReferenceEquals(iv1, iv2) )
				return false;
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return false;
			return !(iv1.Value == iv2.Value);
		}
		
		public static bool operator <(IntValue iv1, IntValue iv2) 
		{
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return false;
			return (iv1.Value < iv2.Value);
		}
		public static bool operator >(IntValue iv1, IntValue iv2) 
		{
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return false;
			return (iv1.Value > iv2.Value);
		}
		
		public static bool operator <=(IntValue iv1, IntValue iv2) 
		{
			if( System.Object.ReferenceEquals(iv1, iv2) )
				return true;
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return false;
			return (iv1.Value <= iv2.Value);
		}
		public static bool operator >=(IntValue iv1, IntValue iv2) 
		{
			if( System.Object.ReferenceEquals(iv1, iv2) )
				return true;
			if( (System.Object)iv1 == null || (System.Object)iv2 == null )
				return false;
			return (iv1.Value >= iv2.Value);
		}
		
		public static bool operator ==(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return false;
			return (i == iv.Value);
		}
		public static bool operator !=(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return false;
			return (i != iv.Value);
		}
		
		public static bool operator <(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return false;
			return (i < iv.Value);
		}
		public static bool operator >(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return false;
			return (i > iv.Value);
		}
		
		public static bool operator <=(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return false;
			return (i <= iv.Value);
		}
		public static bool operator >=(int i, IntValue iv) 
		{
			if( (System.Object)iv == null )
				return false;
			return (i >= iv.Value);
		}
		
		public static bool operator ==(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return false;
			return (iv.Value == i);
		}
		public static bool operator !=(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return false;
			return (iv.Value != i);
		}
		
		public static bool operator <(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return false;
			return (iv.Value < i);
		}
		public static bool operator >(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return false;
			return (iv.Value > i);
		}
		
		public static bool operator <=(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return false;
			return (iv.Value <= i);
		}
		public static bool operator >=(IntValue iv, int i) 
		{
			if( (System.Object)iv == null )
				return false;
			return (iv.Value >= i);
		}
	}
}
