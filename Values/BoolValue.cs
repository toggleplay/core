﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	// TODO random will not be right, fix this

	[System.Serializable]
	public class BoolValue : System.IComparable
	{
		public enum EPropertyType { constant = 0, random, randomRepeating };
		public enum ERandomPropertyType { random = 1, randomRepeating };

		[SerializeField] protected EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] protected bool myValue = true;


		public BoolValue( bool constant )
		{
			Set(constant);
		}
		public BoolValue( ERandomPropertyType t )
		{
			Set(t);
		}
		
		public void Set( bool constant )
		{
			pType = EPropertyType.constant;
			myValue = constant;
		}
		
		public void Set( ERandomPropertyType t )
		{
			pType = (EPropertyType)((int)t);
			myValue = Random.value < 0.5f ? false : true;
		}

		//----------------------------------------------

		public int CompareTo(object obj)
		{
			if (obj == null)
				return 1;

			BoolValue otherBoolValue = obj as BoolValue;
			if( ! ReferenceEquals(otherBoolValue, null) )
				return this.Value.CompareTo(otherBoolValue.Value);
			else
			{
				bool otherBool = true;
				try
				{
					otherBool = (bool)obj;
					return this.Value.CompareTo(otherBool);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a BoolValue or bool");
				}
			}
		}

		//----------------------------------------------
		
		public bool Value
		{
			get
			{
				if( pType == EPropertyType.randomRepeating )
				{
					return Random.value < 0.5f ? false : true;
				}
				return myValue;
			}
		}
		
		//***********************************************************  Operators  ***************************
		
		public override string ToString()
		{
			return string.Format("[BoolValue: Value : {0}]", Value);
		}
		
		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			BoolValue bv = obj as BoolValue;
			if( (System.Object)bv == null )
				return false;
			
			return Equals(bv);
		}
		public bool Equals(BoolValue other)
		{
			if( System.Object.ReferenceEquals(this, other) )
				return true;
			if( (System.Object)other == null )
				return false;

			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant || pType == EPropertyType.random )
				return myValue == other.myValue;

			return false;
		}
		
		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
				return myValue.GetHashCode();

			else
				return pType.GetHashCode();
		}

		
		
		public static bool operator ==(BoolValue bv1, BoolValue bv2) 
		{
			if( System.Object.ReferenceEquals(bv1, bv2) )
				return true;
			if( (System.Object)bv1 == null || (System.Object)bv2 == null )
				return false;
			return (bv1.Value == bv2.Value);
		}
		public static bool operator !=(BoolValue bv1, BoolValue bv2) 
		{
			if( System.Object.ReferenceEquals(bv1, bv2) )
				return false;
			if( (System.Object)bv1 == null || (System.Object)bv2 == null )
				return false;
			return !(bv1.Value == bv2.Value);
		}

		
		public static bool operator ==(bool b, BoolValue bv) 
		{
			if( (System.Object)bv == null )
				return false;
			return (b == bv.Value);
		}
		public static bool operator !=(bool b, BoolValue bv) 
		{
			if( (System.Object)bv == null )
				return false;
			return (b != bv.Value);
		}

		public static bool operator ==(BoolValue bv, bool b) 
		{
			if( (System.Object)bv == null )
				return false;
			return (bv.Value == b);
		}
		public static bool operator !=(BoolValue bv, bool b) 
		{
			if( (System.Object)bv == null )
				return false;
			return (bv.Value != b);
		}
	}
}
