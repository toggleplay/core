﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class FloatValue : System.IComparable
	{
		private class SortAscendingHelper: IComparer<FloatValue>
		{
			int IComparer<FloatValue>.Compare(FloatValue a, FloatValue b)
			{
				if( a == null )
					return -1;

				return a.CompareTo(b);
			}
		}
		public static IComparer<FloatValue> sortAscending
		{
			get
			{
				return (IComparer<FloatValue>) new SortAscendingHelper();
			}
		}

		private class SortDescendingHelper: IComparer<FloatValue>
		{
			int IComparer<FloatValue>.Compare(FloatValue a, FloatValue b)
			{
				if( a == null )
					return 1;

				return -a.CompareTo(b);
			}
		}
		public static IComparer<FloatValue> sortDescending
		{
			get
			{
				return (IComparer<FloatValue>) new SortDescendingHelper();
			}
		}

		private class SortByPropertyHelper : IComparer<FloatValue>
		{
			int IComparer<FloatValue>.Compare(FloatValue a, FloatValue b)
			{
				if( a == null && b == null )
					return 0;
				else if( b == null )
					return 1;
				else if( a == null )
					return -1;

				return ((int)a.pType).CompareTo((int)b.pType);
			}
		}
		public static IComparer<FloatValue> sortByProperty
		{
			get
			{
				return (IComparer<FloatValue>) new SortByPropertyHelper();
			}
		}

		//-----------------------------------------------

		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating,
			alongCurve, alongCurveRepeating, randomAlongTwoCurves, randomAlongTwoCurvesRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };
		public enum ECurvePropertyType { alongCurve = 3, alongCurveRepeating };
		public enum ETwoCurvesPropertyType { randomAlongTwoCurves = 5, randomAlongTwoCurvesRepeating };

		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] private float constantA = 0;
		public float constantValue { get{ return constantA; } }
		public float rangeFrom { get{ return constantA; } }

		[SerializeField] private float constantB = 0;
		public float rangeTo { get{ return constantB; } }

		[SerializeField] private AnimationCurve curveA = new AnimationCurve( new Keyframe[] { new Keyframe(0,0), new Keyframe(1,0) } );
		[SerializeField] private AnimationCurve curveB = new AnimationCurve( new Keyframe[] { new Keyframe(0,0), new Keyframe(1,0) } );

		float? storedValue = null;
		float curveTime = 0;

		public FloatValue()
		{
		}

		public FloatValue( float constant )
		{
			Set(constant);
		}
		public FloatValue( EConstantPropertyType t, float conA, float conB )
		{
			Set(t, conA, conB);
		}

		public FloatValue( ECurvePropertyType t, AnimationCurve onCurve )
		{
			Set( t, onCurve );
		}
		
		public FloatValue( ETwoCurvesPropertyType t, AnimationCurve onCurveA, AnimationCurve onCurveB )
		{
			Set( t, onCurveA, onCurveB );
		}

		public void Set( float constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}

		public void Set( EConstantPropertyType t, float conA, float conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}

		public void Set( ECurvePropertyType t, AnimationCurve onCurve )
		{
			pType = (EPropertyType)((int)t);
			curveA = onCurve;
			curveB = onCurve;
			storedValue = null;
		}

		public void Set( ETwoCurvesPropertyType t, AnimationCurve onCurveA, AnimationCurve onCurveB )
		{
			pType = (EPropertyType)((int)t);
			curveA = onCurveA;
			curveB = onCurveB;
			storedValue = null;
		}

		//---------------------------------------

		public int CompareTo(object obj)
		{
			if( obj == null )
				return 1;
			
			FloatValue otherFloatVal = obj as FloatValue;
			if( ! ReferenceEquals( otherFloatVal, null ) )
				return this.Value.CompareTo(otherFloatVal.Value);
			else
			{
				float otherFloat = 0;
				try
				{
					otherFloat = (float)obj;
					return this.Value.CompareTo(otherFloat);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a FloatValue or float");
				}
			}
		}

		//---------------------------------------

		public float Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = Random.Range( constantA, constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return Random.Range( constantA, constantB );
				}
				else if( pType == EPropertyType.alongCurve )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					if( curveA.keys.Length == float.NaN )
						return float.NaN;
					storedValue = curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.alongCurveRepeating )
				{
					if( curveA.keys.Length == 0 )
						return float.NaN;
					return curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
				}
				else if( pType == EPropertyType.randomAlongTwoCurves )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					if( curveA.keys.Length == 0 || curveB.keys.Length == 0)
						return float.NaN;
					
					// if max of A < min of B // or max of B < min of A
					if( curveA.keys[curveA.keys.Length-1].time < curveB.keys[0].time || curveB.keys[curveA.keys.Length-1].time < curveA.keys[0].time )
					{
						curveTime = curveA.keys[curveA.keys.Length-1].time - curveA.keys[0].time;
						curveTime /= curveTime + curveB.keys[curveB.keys.Length-1].time - curveB.keys[0].time;
						
						if( Random.value < curveTime )
							storedValue = curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
						else
							storedValue = curveB.Evaluate( Random.Range( curveB.keys[0].time, curveB.keys[curveB.keys.Length-1].time ) );
					}
					// range are in sync, but still may have stragglers
					else
					{
						curveTime = Random.Range(Mathf.Min(curveA.keys[0].time, curveB.keys[0].time),
						                         Mathf.Max(curveA.keys[curveA.keys.Length-1].time, curveB.keys[curveB.length-1].time));
						
						if( curveTime < curveA.keys[0].time || curveTime > curveA.keys[curveA.keys.Length-1].time )
						{
							// just intersecting with curveB
							storedValue = curveB.Evaluate(curveTime);
						}
						else if( curveTime < curveB.keys[0].time || curveTime > curveB.keys[curveB.keys.Length-1].time )
						{
							// just intersecting with curveA
							storedValue = curveA.Evaluate(curveTime);
						}
						else
						{
							// the time range is in cross over of the two curves
							storedValue = Random.Range(curveA.Evaluate(curveTime), curveB.Evaluate(curveTime));
						}
					}
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomAlongTwoCurvesRepeating )
				{
					if( curveA.keys.Length == 0 || curveB.keys.Length == 0)
						return 0;
					
					// if max of A < min of B // or max of B < min of A
					if( curveA.keys[curveA.keys.Length-1].time < curveB.keys[0].time || curveB.keys[curveA.keys.Length-1].time < curveA.keys[0].time )
					{
						curveTime = curveA.keys[curveA.keys.Length-1].time - curveA.keys[0].time;
						curveTime /= curveTime + curveB.keys[curveB.keys.Length-1].time - curveB.keys[0].time;
						
						if( Random.value < curveTime )
							return curveA.Evaluate( Random.Range( curveA.keys[0].time, curveA.keys[curveA.keys.Length-1].time ) );
						else
							return curveB.Evaluate( Random.Range( curveB.keys[0].time, curveB.keys[curveB.keys.Length-1].time ) );
					}
					// range are in sync, but still may have stragglers
					else
					{
						curveTime = Random.Range(Mathf.Min(curveA.keys[0].time, curveB.keys[0].time),
						                         Mathf.Max(curveA.keys[curveA.keys.Length-1].time, curveB.keys[curveB.length-1].time));
						
						if( curveTime < curveA.keys[0].time || curveTime > curveA.keys[curveA.keys.Length-1].time )
						{
							// just intersecting with curveB
							return curveB.Evaluate(curveTime);
						}
						else if( curveTime < curveB.keys[0].time || curveTime > curveB.keys[curveB.keys.Length-1].time )
						{
							// just intersecting with curveA
							return curveA.Evaluate(curveTime);
						}
						else
						{
							// the time range is in cross over of the two curves
							return Random.Range(curveA.Evaluate(curveTime), curveB.Evaluate(curveTime));
						}
					}
				}
				return 0; // this should never occur
			}
		}

		//***********************************************************  Operators  ***************************

		public override string ToString()
		{
			return string.Format("[FloatValue: Value={0}]", Value);
		}

		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			FloatValue fv = (FloatValue)obj;
			if( (System.Object)fv == null )
				return false;

			return Equals(fv);
		}
		public bool Equals(FloatValue other)
		{
			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			else if( pType == EPropertyType.alongCurve || pType == EPropertyType.alongCurveRepeating )
			{
				if( curveA.Equals(other.curveA) )
					return true;
			}
			else if( pType == EPropertyType.randomAlongTwoCurves || pType == EPropertyType.randomAlongTwoCurvesRepeating )
			{
				if( !curveA.Equals(other.curveA) )
					return false;
				if( !curveB.Equals(other.curveB) )
					return false;
				return true;
			}
			
			return false;
		}

		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			else if( pType == EPropertyType.alongCurve || pType == EPropertyType.alongCurveRepeating )
			{
				return pType.GetHashCode() ^ curveA.GetHashCode();
			}
			else if( pType == EPropertyType.randomAlongTwoCurves || pType == EPropertyType.randomAlongTwoCurvesRepeating )
			{
				return pType.GetHashCode() ^ curveA.GetHashCode() ^ curveB.GetHashCode();
			}
			
			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode() ^ curveA.GetHashCode() ^ curveB.GetHashCode();
		}

		public static float operator +(FloatValue fv1, FloatValue fv2) 
		{
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return float.NaN;
			return (fv1.Value + fv2.Value);
		}
		public static float operator +(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value + f);
		}
		public static float operator +(FloatValue fv, int i) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value + (float)i);
		}
		public static float operator +(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value + f);
		}
		public static int operator +(int i, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return i;
			return (int)(fv.Value + (float)i);
		}

		public static float operator -(FloatValue fv1, FloatValue fv2) 
		{
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return float.NaN;
			return (fv1.Value - fv2.Value);
		}
		public static float operator -(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value - f);
		}
		public static float operator -(FloatValue fv, int i) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value - (float)i);
		}
		public static float operator -(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value - f);
		}
		public static int operator -(int i, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return i;
			return (int)(fv.Value - (float)i);
		}

		public static float operator *(FloatValue fv1, FloatValue fv2) 
		{
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return float.NaN;
			return (fv1.Value * fv2.Value);
		}
		public static float operator *(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return f;
			return (fv.Value * f);
		}
		public static float operator *(FloatValue fv, int i) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value * (float)i);
		}
		public static float operator *(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value * f);
		}
		public static int operator *(int i, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return i;
			return (int)(fv.Value * (float)i);
		}

		public static float operator /(FloatValue fv1, FloatValue fv2) 
		{
			if( (System.Object)fv1 == null || fv2 == null )
				return float.NaN;
			return (fv1.Value / fv2.Value);
		}
		public static float operator /(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value / f);
		}
		public static float operator /(FloatValue fv, int i) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value / (float)i);
		}
		public static float operator /(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value / f);
		}
		public static int operator /(int i, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return i;
			return (int)(fv.Value / (float)i);
		}

		public static float operator %(FloatValue fv1, FloatValue fv2) 
		{
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return float.NaN;
			return (fv1.Value % fv2.Value);
		}
		public static float operator %(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value % f);
		}
		public static float operator %(FloatValue fv, int i) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value % (float)i);
		}
		public static float operator %(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return float.NaN;
			return (fv.Value % f);
		}



		public static bool operator ==(FloatValue fv1, FloatValue fv2) 
		{
			if( System.Object.ReferenceEquals(fv1, fv2) )
				return true;
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return false;
			return (fv1.Value == fv2.Value);
		}
		public static bool operator !=(FloatValue fv1, FloatValue fv2) 
		{
			if( System.Object.ReferenceEquals(fv1, fv2) )
				return false;
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return false;
			return !(fv1.Value == fv2.Value);
		}
		
		public static bool operator <(FloatValue fv1, FloatValue fv2) 
		{
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return false;
			return (fv1.Value < fv2.Value);
		}
		public static bool operator >(FloatValue fv1, FloatValue fv2) 
		{
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return false;
			return (fv1.Value > fv2.Value);
		}
		
		public static bool operator <=(FloatValue fv1, FloatValue fv2) 
		{
			if( System.Object.ReferenceEquals(fv1, fv2) )
				return true;
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return false;
			return (fv1.Value <= fv2.Value);
		}
		public static bool operator >=(FloatValue fv1, FloatValue fv2) 
		{
			if( System.Object.ReferenceEquals(fv1, fv2) )
				return true;
			if( (System.Object)fv1 == null || (System.Object)fv2 == null )
				return false;
			return (fv1.Value >= fv2.Value);
		}

		public static bool operator ==(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return false;
			return (f == fv.Value);
		}
		public static bool operator !=(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return false;
			return (f != fv.Value);
		}

		public static bool operator <(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return false;
			return (f < fv.Value);
		}
		public static bool operator >(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return false;
			return (f > fv.Value);
		}

		public static bool operator <=(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return false;
			return (f <= fv.Value);
		}
		public static bool operator >=(float f, FloatValue fv) 
		{
			if( (System.Object)fv == null )
				return false;
			return (f >= fv.Value);
		}

		public static bool operator ==(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return false;
			return (fv.Value == f);
		}
		public static bool operator !=(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return false;
			return (fv.Value != f);
		}

		public static bool operator <(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return false;
			return (fv.Value < f);
		}
		public static bool operator >(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return false;
			return (fv.Value > f);
		}
		
		public static bool operator <=(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return false;
			return (fv.Value <= f);
		}
		public static bool operator >=(FloatValue fv, float f) 
		{
			if( (System.Object)fv == null )
				return false;
			return (fv.Value >= f);
		}
	}
}
