/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class QuaternionValue
	{
		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };

		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] private Quaternion constantA = Quaternion.identity;
		public Quaternion constantValue { get{ return constantA; } }
		public Quaternion rangeFrom { get{ return constantA; } }

		[SerializeField] private Quaternion constantB = Quaternion.identity;
		public Quaternion rangeTo { get{ return constantB; } }

		Quaternion? storedValue = null;

		public QuaternionValue()
		{
		}

		public QuaternionValue( Quaternion constant )
		{
			Set(constant);
		}
		public QuaternionValue( EConstantPropertyType t, Quaternion conA, Quaternion conB )
		{
			Set(t, conA, conB);
		}

		public void Set( Quaternion constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}

		public void Set( EConstantPropertyType t, Quaternion conA, Quaternion conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}

		//----------------------------------------------------------------------------------

		public Quaternion Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = RandomQuaternion( constantA, constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return RandomQuaternion( constantA, constantB );
				}
				return Quaternion.identity; // this should never occur
			}
		}

		//***********************************************************  Operators  ***************************

		public override string ToString()
		{
			return string.Format("[QuaternionValue: Value={0}]", Value);
		}

		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			QuaternionValue qv = (QuaternionValue)obj;
			if( (System.Object)qv == null )
				return false;

			return Equals(qv);
		}
		public bool Equals(QuaternionValue other)
		{
			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			
			return false;
		}

		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			
			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
		}

		public static Quaternion operator *(QuaternionValue qv1, QuaternionValue qv2) 
		{
			if( (System.Object)qv1 == null || (System.Object)qv2 == null )
				return Quaternion.identity;
			Quaternion q1 = qv1.Value;
			Quaternion q2 = qv2.Value;
			return q1*q2;
		}
		public static Quaternion operator *(QuaternionValue qv, Quaternion q) 
		{
			if( (System.Object)qv == null )
				return Quaternion.identity;
			return (qv.Value * q);
		}


		public static bool operator ==(QuaternionValue qv1, QuaternionValue qv2) 
		{
			if( System.Object.ReferenceEquals(qv1, qv2) )
				return true;
			if( (System.Object)qv1 == null || (System.Object)qv2 == null )
				return false;
			return (qv1.Value == qv2.Value);
		}
		public static bool operator !=(QuaternionValue qv1, QuaternionValue qv2) 
		{
			if( System.Object.ReferenceEquals(qv1, qv2) )
				return false;
			if( (System.Object)qv1 == null || (System.Object)qv2 == null )
				return false;
			return !(qv1.Value == qv2.Value);
		}


		public static bool operator ==(Quaternion q, QuaternionValue qv) 
		{
			if( (System.Object)qv == null )
				return false;
			return (q == qv.Value);
		}
		public static bool operator !=(Quaternion q, QuaternionValue qv) 
		{
			if( (System.Object)qv == null )
				return false;
			return (q != qv.Value);
		}
	

		public static bool operator ==(QuaternionValue qv, Quaternion q) 
		{
			if( (System.Object)qv == null )
				return false;
			return (qv.Value == q);
		}
		public static bool operator !=(QuaternionValue qv, Quaternion q) 
		{
			if( (System.Object)qv == null )
				return false;
			return (qv.Value != q);
		}

		Quaternion RandomQuaternion( Quaternion fromQuat, Quaternion toQuat )
		{
			return Quaternion.Slerp(fromQuat, toQuat, Random.value);
		}
	}
}
