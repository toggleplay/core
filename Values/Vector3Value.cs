/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Values
{
	[System.Serializable]
	public class Vector3Value : System.IComparable
	{
		public enum EPropertyType { constant = 0, randomBetweenTwoConstants, randomBetweenTwoConstantsRepeating };
		public enum EConstantPropertyType { randomBetweenTwoConstants = 1, randomBetweenTwoConstantsRepeating };

		[SerializeField] private EPropertyType pType = EPropertyType.constant;
		public EPropertyType propertyType { get{ return pType; } }

		[SerializeField] private Vector3 constantA = Vector3.zero;
		public Vector3 constantValue { get{ return constantA; } }
		public Vector3 rangeFrom { get{ return constantA; } }

		[SerializeField] private Vector3 constantB = Vector3.zero;
		public Vector3 rangeTo { get{ return constantB; } }

		Vector3? storedValue = null;

		public Vector3Value()
		{
		}

		public Vector3Value( Vector3 constant )
		{
			Set(constant);
		}
		public Vector3Value( EConstantPropertyType t, Vector3 conA, Vector3 conB )
		{
			Set(t, conA, conB);
		}

		public void Set( Vector3 constant )
		{
			pType = EPropertyType.constant;
			constantA = constant;
			constantB = constant;
		}

		public void Set( EConstantPropertyType t, Vector3 conA, Vector3 conB )
		{
			pType = (EPropertyType)((int)t);
			constantA = conA;
			constantB = conB;
			storedValue = null;
		}

		//---------------------------------------

		public int CompareTo(object obj)
		{
			if( obj == null )
				return 1;
			
			Vector3Value otherV3Val = obj as Vector3Value;
			if( ! ReferenceEquals( otherV3Val, null ) )
			{
				return this.Value.magnitude.CompareTo(otherV3Val.Value.magnitude);
			}
			else
			{
				Vector3 other = Vector3.zero;
				try
				{
					other = (Vector3)obj;
					return this.Value.magnitude.CompareTo(other.magnitude);
				}
				catch
				{
					throw new System.ArgumentException("Object to compare to is not a Vector3Value or Vector3");
				}
			}
		}

		//---------------------------------------

		public Vector3 Value
		{
			get
			{
				if( pType == EPropertyType.constant )
				{
					return constantA;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstants )
				{
					if( storedValue.HasValue )
						return storedValue.Value;
					storedValue = RandomVector3( constantA, constantB );
					return storedValue.Value;
				}
				else if( pType == EPropertyType.randomBetweenTwoConstantsRepeating )
				{
					return RandomVector3( constantA, constantB );
				}
				return Vector3.zero; // this should never occur
			}
		}

		//***********************************************************  Operators  ***************************

		public override string ToString()
		{
			return string.Format("[Vector3Value: Value={0}]", Value);
		}

		public override bool Equals(object obj)
		{
			if( obj == null )
				return false;
			Vector3Value v3 = (Vector3Value)obj;
			if( (System.Object)v3 == null )
				return false;

			return Equals(v3);
		}
		public bool Equals(Vector3Value other)
		{
			if( pType != other.propertyType )
				return false;
			else if( pType == EPropertyType.constant )
			{
				return constantValue == other.constantValue;
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				if( rangeFrom != other.rangeFrom )
					return false;
				if( rangeTo != other.rangeTo )
					return false;
				return true;
			}
			
			return false;
		}

		public override int GetHashCode()
		{
			if( pType == EPropertyType.constant )
			{
				return pType.GetHashCode() ^ constantValue.GetHashCode();
			}
			else if( pType == EPropertyType.randomBetweenTwoConstants || pType == EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
			}
			
			return pType.GetHashCode() ^ constantA.GetHashCode() ^ constantB.GetHashCode();
		}

		public static Vector3 operator +(Vector3Value v3v1, Vector3Value v3v2) 
		{
			if( (System.Object)v3v1 == null || (System.Object)v3v2 == null )
				return Vector3.zero;
			Vector3 v1 = v3v1.Value;
			Vector3 v2 = v3v2.Value;
			return v1 + v2;
		}
		public static Vector3 operator +(Vector3Value v3v, float f) 
		{
			if( (System.Object)v3v == null )
				return Vector3.zero;
			Vector3 v = v3v.Value;
			return new Vector3(v.x+f, v.y+f, v.z+f);
		}

		public static Vector3 operator -(Vector3Value v3v1, Vector3Value v3v2) 
		{
			if( (System.Object)v3v1 == null || (System.Object)v3v2 == null )
				return Vector3.zero;
			Vector3 v1 = v3v1.Value;
			Vector3 v2 = v3v2.Value;
			return v1 - v2;
		}
		public static Vector3 operator -(Vector3Value v3v, float f) 
		{
			if( (System.Object)v3v == null )
				return Vector3.zero;
			Vector3 v = v3v.Value;
			return new Vector3(v.x-f, v.y-f, v.z-f);
		}

		public static Vector3 operator *(Vector3Value v3v1, Vector3Value v3v2) 
		{
			if( (System.Object)v3v1 == null || (System.Object)v3v2 == null )
				return Vector3.zero;
			Vector3 v1 = v3v1.Value;
			Vector3 v2 = v3v2.Value;
			return new Vector3(v1.x*v2.x, v1.y*v2.y, v1.z*v2.z);
		}
		public static Vector3 operator *(Vector3Value v3v, float f) 
		{
			if( (System.Object)v3v == null )
				return Vector3.zero;
			return (v3v.Value * f);
		}


		public static Vector3 operator /(Vector3Value v3v1, Vector3Value v3v2) 
		{
			if( (System.Object)v3v1 == null || v3v2 == null )
				return Vector3.zero;
			Vector3 v1 = v3v1.Value;
			Vector3 v2 = v3v2.Value;
			return new Vector3(v1.x/v2.x, v1.y/v2.y, v1.z/v2.z);
		}
		public static Vector3 operator /(Vector3Value v3v, float f) 
		{
			if( (System.Object)v3v == null )
				return Vector3.zero;
			return (v3v.Value / f);
		}


		public static bool operator ==(Vector3Value v3v1, Vector3Value v3v2) 
		{
			if( System.Object.ReferenceEquals(v3v1, v3v2) )
				return true;
			if( (System.Object)v3v1 == null || (System.Object)v3v2 == null )
				return false;
			return (v3v1.Value == v3v2.Value);
		}
		public static bool operator !=(Vector3Value v3v1, Vector3Value v3v2) 
		{
			if( System.Object.ReferenceEquals(v3v1, v3v2) )
				return false;
			if( (System.Object)v3v1 == null || (System.Object)v3v2 == null )
				return false;
			return !(v3v1.Value == v3v2.Value);
		}


		public static bool operator ==(Vector3 v3, Vector3Value v3v) 
		{
			if( (System.Object)v3v == null )
				return false;
			return (v3 == v3v.Value);
		}
		public static bool operator !=(Vector3 v3, Vector3Value v3v) 
		{
			if( (System.Object)v3v == null )
				return false;
			return (v3 != v3v.Value);
		}
	

		public static bool operator ==(Vector3Value v3v, Vector3 v3) 
		{
			if( (System.Object)v3v == null )
				return false;
			return (v3v.Value == v3);
		}
		public static bool operator !=(Vector3Value v3v, Vector3 v3) 
		{
			if( (System.Object)v3v == null )
				return false;
			return (v3v.Value != v3);
		}

		//----------------------------------------------------------------------------------

		Vector3 RandomVector3( Vector3 fromVector, Vector3 toVector )
		{
			return new Vector3( Random.Range(fromVector.x, toVector.x), 
			                   Random.Range(fromVector.y, toVector.y), 
			                   Random.Range(fromVector.z, toVector.z) );
		}
	}
}
