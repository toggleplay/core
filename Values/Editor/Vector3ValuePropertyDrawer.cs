﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace TogglePlay.Values
{
	[CustomPropertyDrawer(typeof(Vector3Value))]
	public class Vector3ValuePropertyDrawer : PropertyDrawer
	{
		float height = 34.0f;
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			Vector3Value.EPropertyType pType = (Vector3Value.EPropertyType)propType.enumValueIndex;

			if( pType == Vector3Value.EPropertyType.constant )
				height = 34.0f;
			else if( pType == Vector3Value.EPropertyType.randomBetweenTwoConstants || pType == Vector3Value.EPropertyType.randomBetweenTwoConstantsRepeating )
				height = 51.0f;

			return height;
		}

		public override void OnGUI(Rect rec, SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			
			int indent = 13*EditorGUI.indentLevel;

			float labelWidth = EditorGUIUtility.labelWidth;
			float areaWidth = rec.width - labelWidth;
			float beginX = rec.x + indent + (rec.x + labelWidth -15);
			
			EditorGUI.LabelField( new Rect( rec.x + indent, rec.y, rec.width, rec.height ), label );

			float yHeight = 17.0f;
			EditorGUI.PropertyField( new Rect( beginX, rec.y, areaWidth, yHeight ),	
			                        propType, GUIContent.none );
			
			Vector3Value.EPropertyType pType = (Vector3Value.EPropertyType)propType.enumValueIndex;
			if( pType == Vector3Value.EPropertyType.constant )
			{
				SerializedProperty constantA = property.FindPropertyRelative("constantA");

				EditorGUI.LabelField( new Rect( beginX, rec.y+yHeight, 33f, yHeight ), "value" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + yHeight, areaWidth-33f, yHeight ), 				
				                        constantA, GUIContent.none );
				height = 34.0f;
			}
			else if( pType == Vector3Value.EPropertyType.randomBetweenTwoConstants || pType == Vector3Value.EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				SerializedProperty constantA = property.FindPropertyRelative("constantA");
				SerializedProperty constantB = property.FindPropertyRelative("constantB");

				EditorGUI.PropertyField( new Rect( beginX, rec.y, areaWidth, yHeight ),		
				                        propType, GUIContent.none );

				EditorGUI.LabelField( new Rect( beginX, rec.y+yHeight, 33f, yHeight ), "from" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + yHeight, areaWidth-33f, yHeight ), 				
				                        constantA, GUIContent.none );

				yHeight = 34;
				EditorGUI.LabelField( new Rect( beginX, rec.y+yHeight, 33f, yHeight ), "to" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + yHeight, areaWidth-33f, yHeight ), 				
				                        constantB, GUIContent.none );
				height = 51.0f;
			}


			if( pType == Vector3Value.EPropertyType.constant )
				height = 34.0f;
			else if( pType == Vector3Value.EPropertyType.randomBetweenTwoConstants || pType == Vector3Value.EPropertyType.randomBetweenTwoConstantsRepeating )
				height = 51.0f;
			rec.height = height;
		}
	}
}
