﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace TogglePlay.Values
{
	[CustomPropertyDrawer(typeof(BoolValue))]
	public class BoolValuePropertyDrawer : PropertyDrawer
	{
		float height = 34f;
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			BoolValue.EPropertyType pType = (BoolValue.EPropertyType)propType.enumValueIndex;
			if( pType == BoolValue.EPropertyType.constant )
				height = 34;
			else if( pType == BoolValue.EPropertyType.random || pType == BoolValue.EPropertyType.randomRepeating )
				height = 17;
			return height;
		}

		public override void OnGUI(Rect rec, SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			
			int indent = 13*EditorGUI.indentLevel;

			float lw = EditorGUIUtility.labelWidth;
			float aw = rec.width - lw;
			float ax = rec.x + lw -15;
			float beginX = rec.x + indent + ax;
			
			EditorGUI.LabelField( new Rect( rec.x + indent, rec.y, rec.width, rec.height ), label );

			EditorGUI.PropertyField( new Rect( beginX, rec.y, aw, 17 ),	
			                        propType, GUIContent.none );

			BoolValue.EPropertyType pType = (BoolValue.EPropertyType)propType.enumValueIndex;
			if( pType == BoolValue.EPropertyType.constant )
			{
				height = 34;
				float yHeight = 17;

				SerializedProperty constantA = property.FindPropertyRelative("myValue");

				EditorGUI.LabelField( new Rect( beginX, rec.y+yHeight, 33f, yHeight ), "value" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + yHeight, aw-33f, yHeight ), 				
				                        constantA, GUIContent.none );
			}
			else if( pType == BoolValue.EPropertyType.random || pType == BoolValue.EPropertyType.randomRepeating )
			{
				height = 17;
			}
		}
	}
}
