﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace TogglePlay.Values
{
	[CustomPropertyDrawer(typeof(IntValue))]
	public class IntValuePropertyDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			return 34.0f;
		}

		public override void OnGUI(Rect rec, SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			
			int indent = 13*EditorGUI.indentLevel;

			float lw = EditorGUIUtility.labelWidth;
			float aw = rec.width - lw;
			float ax = rec.x + lw -15;

			float halfHeight = rec.height * 0.5f;
			float beginX = rec.x + indent + ax;
			
			EditorGUI.LabelField( new Rect( rec.x + indent, rec.y, rec.width, rec.height ), label );


			IntValue.EPropertyType pType = (IntValue.EPropertyType)propType.enumValueIndex;
			if( pType == IntValue.EPropertyType.constant )
			{
				SerializedProperty constantA = property.FindPropertyRelative("constantA");

				EditorGUI.PropertyField( new Rect( beginX, rec.y, aw, halfHeight ),	
				                        propType, GUIContent.none );

				EditorGUI.LabelField( new Rect( beginX, rec.y+halfHeight, 33f, halfHeight ), "value" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + halfHeight, aw-33f, halfHeight ), 				
				                        constantA, GUIContent.none );
			}
			else if( pType == IntValue.EPropertyType.randomBetweenTwoConstants || pType == IntValue.EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				SerializedProperty constantA = property.FindPropertyRelative("constantA");
				SerializedProperty constantB = property.FindPropertyRelative("constantB");

				float sectionWidth = aw/2f;
				EditorGUI.PropertyField( new Rect( beginX, rec.y, aw, halfHeight ),		
				                        propType, GUIContent.none );

				float gap = 33f;

				EditorGUI.LabelField( new Rect( beginX, rec.y+halfHeight, gap, halfHeight ), "from" );
				EditorGUI.PropertyField( new Rect( beginX + gap, rec.y+halfHeight, sectionWidth-gap, halfHeight ), 				
				                        constantA, GUIContent.none );
				EditorGUI.LabelField( new Rect( beginX + 5 + (sectionWidth), rec.y+halfHeight, gap-5, halfHeight ), "to" );
				EditorGUI.PropertyField( new Rect( beginX + gap + (sectionWidth), rec.y+halfHeight, sectionWidth-gap, halfHeight ), 				
				                        constantB, GUIContent.none );
			}
			else if( pType == IntValue.EPropertyType.alongCurve || pType == IntValue.EPropertyType.alongCurveRepeating )
			{
				SerializedProperty curveA = property.FindPropertyRelative("curveA");
				
				EditorGUI.PropertyField( new Rect( beginX, rec.y, aw, halfHeight ),	
				                        propType, GUIContent.none );
				
				EditorGUI.LabelField( new Rect( beginX, rec.y+halfHeight, 33f, halfHeight ), "curve" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + halfHeight, aw-33f, halfHeight ), 				
				                        curveA, GUIContent.none );
			}
			else if( pType == IntValue.EPropertyType.randomAlongTwoCurves || pType == IntValue.EPropertyType.randomAlongTwoCurvesRepeating )
			{
				SerializedProperty curveA = property.FindPropertyRelative("curveA");
				SerializedProperty curveB = property.FindPropertyRelative("curveB");
				
				float sectionWidth = aw/2f;
				EditorGUI.PropertyField( new Rect( beginX, rec.y, aw, halfHeight ),		
				                        propType, GUIContent.none );
				
				float gap = 33f;
				
				EditorGUI.LabelField( new Rect( beginX, rec.y+halfHeight, gap, halfHeight ), "from" );
				EditorGUI.PropertyField( new Rect( beginX + gap, rec.y+halfHeight, sectionWidth-gap, halfHeight ), 				
				                        curveA, GUIContent.none );
				EditorGUI.LabelField( new Rect( beginX + 5 + (sectionWidth), rec.y+halfHeight, gap-5, halfHeight ), "to" );
				EditorGUI.PropertyField( new Rect( beginX + gap + (sectionWidth), rec.y+halfHeight, sectionWidth-gap, halfHeight ), 				
				                        curveB, GUIContent.none );
			}
		}
	}
}
