﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace TogglePlay.Values
{
	[CustomPropertyDrawer(typeof(StringValue))]
	public class StringValuePropertyDrawer : PropertyDrawer
	{
		float height = 34f;
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			return height;
		}

		public override void OnGUI(Rect rec, SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			
			int indent = 13*EditorGUI.indentLevel;

			float lw = EditorGUIUtility.labelWidth;
			float aw = rec.width - lw;
			float ax = rec.x + lw -15;

			float beginX = rec.x + indent + ax;
			
			EditorGUI.LabelField( new Rect( rec.x + indent, rec.y, rec.width, rec.height ), label );


			StringValue.EPropertyType pType = (StringValue.EPropertyType)propType.enumValueIndex;
			if( pType == StringValue.EPropertyType.constant )
			{
				height = 34;
				float halfHeight = 17;
				SerializedProperty constantA = property.FindPropertyRelative("constantValue");

				EditorGUI.PropertyField( new Rect( beginX, rec.y, aw, halfHeight ),	
				                        propType, GUIContent.none );

				EditorGUI.LabelField( new Rect( beginX, rec.y+halfHeight, 33f, halfHeight ), "value" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + halfHeight, aw-33f, halfHeight ), 				
				                        constantA, GUIContent.none );
			}
			else if( pType == StringValue.EPropertyType.random || pType == StringValue.EPropertyType.randomRepeating )
			{
				height = 17;
				SerializedProperty stringsProp = property.FindPropertyRelative("stringArray");

				EditorGUI.PropertyField( new Rect( beginX, rec.y, aw, 17 ),	propType, GUIContent.none );

				for( int i=0; i<stringsProp.arraySize; ++i )
				{
					SerializedProperty element = stringsProp.GetArrayElementAtIndex(i);
					EditorGUI.PropertyField( new Rect( beginX, rec.y + height, aw, 17 ), element, GUIContent.none );
					height += 17;
				}

				if( stringsProp.arraySize > 0 )
				{
					// if text fully blank then remove, only from bottom like tags
					SerializedProperty lastElement = stringsProp.GetArrayElementAtIndex(stringsProp.arraySize-1);
					if( string.IsNullOrEmpty(lastElement.stringValue) )
						stringsProp.DeleteArrayElementAtIndex(stringsProp.arraySize-1);
				}

				// if anything added, add to array
				string newStr = EditorGUI.TextField( new Rect(beginX, rec.y + height, aw, 17), GUIContent.none, "" );
				if( ! string.IsNullOrEmpty(newStr) )
				{
					stringsProp.InsertArrayElementAtIndex(stringsProp.arraySize);
					SerializedProperty element = stringsProp.GetArrayElementAtIndex(stringsProp.arraySize-1);
					element.stringValue = newStr;
				}

				height += 17;
			}

			pType = (StringValue.EPropertyType)propType.enumValueIndex;
			if( pType == StringValue.EPropertyType.constant )
			{
				height = 34;
			}
			else if( pType == StringValue.EPropertyType.random || pType == StringValue.EPropertyType.randomRepeating )
			{
				// find height
			}
		}
	}
}
