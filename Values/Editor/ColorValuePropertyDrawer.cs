﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace TogglePlay.Values
{
	[CustomPropertyDrawer(typeof(ColorValue))]
	public class ColorValuePropertyDrawer : PropertyDrawer
	{
		float height = 34.0f;
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			ColorValue.EPropertyType pType = (ColorValue.EPropertyType)propType.enumValueIndex;

			if( pType == ColorValue.EPropertyType.constant )
				height = 34.0f;
			else if( pType == ColorValue.EPropertyType.randomBetweenTwoConstants || pType == ColorValue.EPropertyType.randomBetweenTwoConstantsRepeating )
				height = 51.0f;
			else if( pType == ColorValue.EPropertyType.randomWithinList || pType == ColorValue.EPropertyType.randomWithinListRepeating )
			{
				SerializedProperty SPListOf = property.FindPropertyRelative("listOf");
				height = 34f + ((float)SPListOf.arraySize * 17f);
			}

			return height;
		}

		public override void OnGUI(Rect rec, SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			
			int indent = 13*EditorGUI.indentLevel;

			float labelWidth = EditorGUIUtility.labelWidth;
			float areaWidth = rec.width - labelWidth;
			float beginX = rec.x + indent + (rec.x + labelWidth -15);
			
			EditorGUI.LabelField( new Rect( rec.x + indent, rec.y, rec.width, rec.height ), label );

			float yHeight = 17.0f;
			EditorGUI.PropertyField( new Rect( beginX, rec.y, areaWidth, 17 ),	
			                        propType, GUIContent.none );
			
			ColorValue.EPropertyType pType = (ColorValue.EPropertyType)propType.enumValueIndex;
			if( pType == ColorValue.EPropertyType.constant )
			{
				SerializedProperty constantA = property.FindPropertyRelative("constantA");

				EditorGUI.LabelField( new Rect( beginX, rec.y+yHeight, 33f, yHeight ), "value" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + yHeight, areaWidth-33f, 17 ), 				
				                        constantA, GUIContent.none );
				height = 34.0f;
			}
			else if( pType == ColorValue.EPropertyType.randomBetweenTwoConstants || pType == ColorValue.EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				SerializedProperty constantA = property.FindPropertyRelative("constantA");
				SerializedProperty constantB = property.FindPropertyRelative("constantB");

				EditorGUI.PropertyField( new Rect( beginX, rec.y, areaWidth, yHeight ),		
				                        propType, GUIContent.none );

				EditorGUI.LabelField( new Rect( beginX, rec.y+yHeight, 33f, yHeight ), "from" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + yHeight, areaWidth-33f, 17 ), 				
				                        constantA, GUIContent.none );

				yHeight = 34;
				EditorGUI.LabelField( new Rect( beginX, rec.y+yHeight, 33f, yHeight ), "to" );
				EditorGUI.PropertyField( new Rect( beginX + 33f, rec.y + yHeight, areaWidth-33f, 17 ), 				
				                        constantB, GUIContent.none );
				height = 51.0f;
			}
			else if( pType == ColorValue.EPropertyType.randomWithinList || pType == ColorValue.EPropertyType.randomWithinListRepeating )
			{
				SerializedProperty SPListOf = property.FindPropertyRelative("listOf");

				EditorGUI.PropertyField( new Rect( beginX, rec.y, areaWidth, yHeight ),		
				                        propType, GUIContent.none );

				SPListOf.arraySize = EditorGUI.IntField( new Rect( rec.x, rec.y+yHeight, rec.width, yHeight ), "Count", SPListOf.arraySize );

				for( int i=0; i < SPListOf.arraySize; ++i )
				{
					yHeight += 17f;
					EditorGUI.PropertyField( new Rect( rec.x, rec.y+yHeight, rec.width, 17f ), SPListOf.GetArrayElementAtIndex(i) );
				}

				height = yHeight + 17f;
			}

			rec.height = height;
		}
	}
}
