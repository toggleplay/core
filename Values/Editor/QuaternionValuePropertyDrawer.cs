﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace TogglePlay.Values
{
	[CustomPropertyDrawer(typeof(QuaternionValue))]
	public class QuaternionValuePropertyDrawer : PropertyDrawer
	{
		float lineHeight = 17.0f;
		float height = 34.0f;
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			QuaternionValue.EPropertyType pType = (QuaternionValue.EPropertyType)propType.enumValueIndex;

			if( pType == QuaternionValue.EPropertyType.constant )
				height = lineHeight*3;
			else if( pType == QuaternionValue.EPropertyType.randomBetweenTwoConstants || pType == QuaternionValue.EPropertyType.randomBetweenTwoConstantsRepeating )
				height = (lineHeight*5)+4;

			return height;
		}

		public override void OnGUI(Rect rec, SerializedProperty property, GUIContent label)
		{
			SerializedProperty propType = property.FindPropertyRelative("pType");
			
			int indent = 13*EditorGUI.indentLevel;

			float labelWidth = EditorGUIUtility.labelWidth;
			float areaWidth = rec.width - labelWidth;
			float beginX = rec.x + indent + (rec.x + labelWidth -15);
			float guiY = rec.y;

			EditorGUI.LabelField( new Rect( rec.x + indent, guiY, rec.width, rec.height ), label );


			EditorGUI.PropertyField( new Rect( beginX, guiY, areaWidth, lineHeight ),	
			                        propType, GUIContent.none );
			
			QuaternionValue.EPropertyType pType = (QuaternionValue.EPropertyType)propType.enumValueIndex;
			if( pType == QuaternionValue.EPropertyType.constant )
			{
				guiY += lineHeight;
				SerializedProperty constantA = property.FindPropertyRelative("constantA");

				EditorGUI.LabelField( new Rect( beginX, guiY, 33f, lineHeight ), "value" );

				float width = (areaWidth-33f)*0.5f;
				Rect leftValRect = new Rect( beginX + 33f, guiY, width, lineHeight );
				Rect rightValRect = new Rect( beginX + 33f + width, guiY, width, lineHeight );

				PropertyField( leftValRect, constantA.FindPropertyRelative("x"), "X", 13);
				PropertyField( rightValRect, constantA.FindPropertyRelative("y"), "Y", 13);
				leftValRect.y += lineHeight; rightValRect.y += lineHeight;
				PropertyField( leftValRect, constantA.FindPropertyRelative("z"), "Z", 13);
				PropertyField( rightValRect, constantA.FindPropertyRelative("w"), "W", 13);

				height = lineHeight*3;
			}
			else if( pType == QuaternionValue.EPropertyType.randomBetweenTwoConstants || pType == QuaternionValue.EPropertyType.randomBetweenTwoConstantsRepeating )
			{
				SerializedProperty constantA = property.FindPropertyRelative("constantA");
				SerializedProperty constantB = property.FindPropertyRelative("constantB");

				guiY += lineHeight;
				EditorGUI.LabelField( new Rect( beginX, guiY, 33f, lineHeight ), "from" );

				float width = (areaWidth-33f)/2f;
				Rect leftValRect = new Rect( beginX + 33f, guiY, width, lineHeight );
				Rect rightValRect = new Rect( beginX + 33f + width, guiY, width, lineHeight );
				
				PropertyField( leftValRect, constantA.FindPropertyRelative("x"), "X", 13);
				PropertyField( rightValRect, constantA.FindPropertyRelative("y"), "Y", 13);
				leftValRect.y += lineHeight; rightValRect.y += lineHeight;
				PropertyField( leftValRect, constantA.FindPropertyRelative("z"), "Z", 13);
				PropertyField( rightValRect, constantA.FindPropertyRelative("w"), "W", 13);

				guiY += lineHeight*2;
				EditorGUI.LabelField( new Rect( beginX, guiY, 33f, lineHeight ), "to" );

				leftValRect.y += lineHeight + 4; rightValRect.y += lineHeight + 4;
				PropertyField( leftValRect, constantB.FindPropertyRelative("x"), "X", 13);
				PropertyField( rightValRect, constantB.FindPropertyRelative("y"), "Y", 13);
				leftValRect.y += lineHeight; rightValRect.y += lineHeight;
				PropertyField( leftValRect, constantB.FindPropertyRelative("z"), "Z", 13);
				PropertyField( rightValRect, constantB.FindPropertyRelative("w"), "W", 13);

				height = (lineHeight*5)+4;
			}

			rec.height = height;
		}

		protected void PropertyField( Rect rec, SerializedProperty prop, string label, float labelWidth )
		{
			EditorGUI.LabelField( rec, label );
			Rect pRect = new Rect(rec);
			pRect.x += labelWidth; pRect.width -= labelWidth;
			EditorGUI.PropertyField( pRect, prop, GUIContent.none );
		}
	}
}
