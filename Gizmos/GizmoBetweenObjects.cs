/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Gizmo
{

	[AddComponentMenu("Toggle Play/Gizmos/Draw Line Between Objects")]
	public class GizmoBetweenObjects : MonoBehaviour
	{
		public bool onlyDrawWhenSelected = false;
		public Transform objectA = null;
		public Transform objectB = null;
		public Color unSelectedColor = Color.gray;
		public Color selectedColor = Color.red;
		
		void OnDrawGizmos()
		{
			if( onlyDrawWhenSelected || objectA == null || objectB == null )
				return;
			
			Gizmos.color = unSelectedColor;

			Gizmos.DrawLine(objectA.position, objectB.position);
		}
		
		void OnDrawGizmosSelected()
		{
			if( objectA == null || objectB == null )
				return;
			
			Gizmos.color = selectedColor;
			Gizmos.DrawLine(objectA.position, objectB.position);
		}
	}

}