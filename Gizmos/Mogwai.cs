/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Gizmo
{

	/// <summary>
	/// Helper gizmo classes
	/// </summary>

	// efficiency has not been taken as much into account here are these are gizmos
	// optimisation pass will occur soon

	public static class Mogwai
	{
		public static void DrawDashLine( Vector3 start, Vector3 end, float dashLength, float gapLength )
		{
			if( gapLength <= 0 )
			{
				Gizmos.DrawLine( start, end );
				return;
			}
			
			Vector3 start2End = end-start;
			float mag = start2End.magnitude;
			if( mag < dashLength )
			{
				Gizmos.DrawLine( start, end );
				return;
			}
			
			Vector3 currentPostion = start;
			Vector3 s2eNorm = start2End.normalized;
			Vector3 dashJump = s2eNorm*dashLength;
			Vector3 gapJump = s2eNorm*gapLength;
			
			bool dash = true;
			while( mag > 0 )
			{
				if( dash )
				{
					if( mag < dashLength )
						dashJump = s2eNorm*mag;
					Gizmos.DrawLine( currentPostion, currentPostion+dashJump );
					currentPostion = currentPostion+dashJump;
					mag -= dashLength;
					dash = !dash;
				}
				else
				{
					currentPostion = currentPostion+gapJump;
					mag -= gapLength;
					dash = !dash;
				}
			}
		}
		
		public static void DrawDualColorLine( Vector3 start, Vector3 end, float dashLength, Color colA, Color colB )
		{
			Vector3 start2End = end-start;
			float mag = start2End.magnitude;
			if( mag < dashLength )
			{
				Gizmos.color = colA;
				Gizmos.DrawLine( start, end );
				return;
			}
			
			Vector3 currentPostion = start;
			Vector3 s2eNorm = start2End.normalized;
			Vector3 dashJump = s2eNorm*dashLength;
			
			bool useA = true;
			while( mag > 0 )
			{
				if( useA )
					Gizmos.color = colA;
				else
					Gizmos.color = colB;
				
				if( mag < dashLength )
					dashJump = s2eNorm*mag;
				
				Gizmos.DrawLine( currentPostion, currentPostion+dashJump );
				currentPostion = currentPostion+dashJump;
				mag -= dashLength;
				useA = !useA;
			}
		}
		
		// size of umbrella = directance.magnitude
		public static void DrawUmbrella( Vector3 origin, Vector3 direction, Vector3 tangent, float theta, int segments, int linesEverySegment )
		{
			Vector3 startingRadial = Quaternion.AngleAxis(theta, tangent) * direction;
			Vector3 previous = startingRadial;
			
			Gizmos.DrawLine(origin, origin + direction );
			
			if( IsWithinRangeOf(theta, 0, 0.001f) )
				return;
			
			Gizmos.DrawLine(origin, origin + startingRadial );
			
			int segLine = 1;
			float fSegments = (float)segments;
			
			for( int i = 0; i < segments; ++i )
			{
				float t = ((float)(i+1)/fSegments);
				
				// rotation around the direction vector to create cone
				Vector3 radialConed = Quaternion.AngleAxis(t*360f, direction) * startingRadial;
				
				if( segLine >= linesEverySegment && i != segments-1 )
				{
					segLine = 0;
					Gizmos.DrawLine( origin, origin + radialConed );
				}
				
				segLine = segLine+1;
				Gizmos.DrawLine( origin + previous, origin + radialConed );
				previous = radialConed;
			}
		}
		
		
		
		public static void DrawDisc( Vector3 origin, Vector3 direction, float radius, int segments, int linesEverySegment )
		{
			Vector3 tangent = GetATangentOf(direction) * radius;
			
			Vector3 previous = tangent;
			
			
			Gizmos.DrawLine(origin, origin + tangent );
			
			int segLine = 1;
			float fSegments = (float)segments;
			
			for( int i = 0; i < segments; ++i )
			{
				float t = ((float)(i+1)/fSegments);
				
				// rotation around the direction vector to create cone
				Vector3 radialConed = Quaternion.AngleAxis(t*360f, direction) * tangent;
				
				if( segLine >= linesEverySegment && i != segments-1 && linesEverySegment > 0 )
				{
					segLine = 0;
					Gizmos.DrawLine( origin, origin + radialConed );
				}
				
				segLine = segLine+1;
				Gizmos.DrawLine( origin + previous, origin + radialConed );
				previous = radialConed;
			}
		}
		
		public static void DrawCylinder( Vector3 origin, Vector3 direction, float radius, float length, int segments )
		{
			direction.Normalize();
			Vector3 tangent = GetATangentOf(direction) * radius;
			
			Vector3 previous = tangent;
			
			Vector3 top = origin + (direction * (length*0.5f));
			Vector3 bottom = origin - (direction * (length*0.5f));
			
			Gizmos.DrawLine(top, top + tangent );
			Gizmos.DrawLine(bottom, bottom + tangent );
			Gizmos.DrawLine(bottom + tangent, top + tangent );
			
			float fSegments = (float)segments;
			
			for( int i = 0; i < segments; ++i )
			{
				float t = ((float)(i+1)/fSegments);
				
				// rotation around the direction vector to create cone
				Vector3 radialConed = Quaternion.AngleAxis(t*360f, direction) * tangent;
				
				//Gizmos.DrawLine( origin + previous, origin + radialConed );
				
				Gizmos.DrawLine(top+previous, top + radialConed );
				Gizmos.DrawLine(bottom+previous, bottom + radialConed );
				Gizmos.DrawLine(bottom + radialConed, top + radialConed );
				
				previous = radialConed;
			}
		}
		
		public static void DrawKite( Transform fromTransform, Transform toTransform, float fractionalWidth = 0.1f )
		{
			Vector3 start = fromTransform.position;
			Vector3 end = toTransform.position;
			
			Vector3 start2End = end - start;
				
			Vector3 tangent = GetATangentOf(start2End);
			
			float length = start2End.magnitude;
			
			tangent *= length*fractionalWidth;
			
			Vector3 offset = start2End * 0.25f;
			
			// draw first one
			Vector3 tangentPos = start + offset + tangent;
			Gizmos.DrawLine(start, tangentPos );
			Gizmos.DrawLine( tangentPos, end);
			Vector3 prevTangentPoint = tangentPos;
			Vector3 startingPoint = prevTangentPoint;
			
			for( int j = 0; j < 3; ++j )
			{
				tangent = Rotate(tangent, start2End, 90);
				tangentPos = start + offset + tangent;
				Gizmos.DrawLine(start, tangentPos );
				Gizmos.DrawLine(tangentPos, end);
				Gizmos.DrawLine(tangentPos, prevTangentPoint);
				if( j == 2 )
					Gizmos.DrawLine(startingPoint, tangentPos);
				prevTangentPoint = tangentPos;
			}
		
		}
		
		public static void DrawDiamond( Transform fromTransform, Transform toTransform, float width = 1, float splitPoint = 0.5f )
		{
			Vector3 start = fromTransform.position;
			Vector3 end = toTransform.position;
			
			Vector3 start2End = end - start;
				
			Vector3 tangent = GetATangentOf(start2End);
			
			tangent *= width;
			
			Vector3 offset = start2End * splitPoint;
			
			// draw first one
			Vector3 tangentPos = start + offset + tangent;
			Gizmos.DrawLine(start, tangentPos );
			Gizmos.DrawLine( tangentPos, end);
			Vector3 prevTangentPoint = tangentPos;
			Vector3 startingPoint = prevTangentPoint;
			
			for( int j = 0; j < 3; ++j )
			{
				tangent = Rotate(tangent, start2End, 90);
				tangentPos = start + offset + tangent;
				Gizmos.DrawLine(start, tangentPos );
				Gizmos.DrawLine(tangentPos, end);
				Gizmos.DrawLine(tangentPos, prevTangentPoint);
				if( j == 2 )
					Gizmos.DrawLine(startingPoint, tangentPos);
				prevTangentPoint = tangentPos;
			}
		
		}

		//----------------------------------------------------------------------------------
		// Utils
		//----------------------------------------------------------------------------------

		public static bool IsWithinRange(float val, float min, float max)
		{
			return (val >= min) && (val <= max);
		}
		public static bool IsWithinRangeOf(float val, float about, float range)
		{
			return Mathf.Abs(val - about) < range;
		}

		// wrong?
		public static Vector3 Rotate(Vector3 vector, Vector3 axis, float angle)
		{
			return Quaternion.AngleAxis(angle, axis) * vector;
		}

		public static Vector3 GetATangentOf( Vector3 dir )
		{
			dir.Normalize();
			float x = Mathf.Abs(dir.x);
			float y = Mathf.Abs(dir.y);
			float z = Mathf.Abs(dir.z);
			
			if( x < y )
			{
				if( x < z )
					return Vector3.Cross( dir, new Vector3(1,0,0));
				else
					return Vector3.Cross( dir, new Vector3(0,0,1));
			}
			else if( y < z )
				return Vector3.Cross( dir, new Vector3(0,1,0));
			else
				return Vector3.Cross( dir, new Vector3(0,0,1));
		}
	}

}
