/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Gizmo
{

	[AddComponentMenu("Toggle Play/Gizmos/Renderer Bounds Gizmo")]
	[ExecuteInEditMode]
	public class GizmoRendererBounds : MonoBehaviour
	{
		
#if UNITY_EDITOR
		public GameObject contentHolder = null;
		public Color displayColor = Color.red;
		
		public void Reset()
		{
			if( contentHolder == null )
				contentHolder = this.gameObject;
		}
		
		
		public void OnDrawGizmos()
		{
			if( contentHolder == null )
				return;
			Color c = displayColor;
			//c.a = 0.4f;
			Gizmos.color = c;
			
			Gizmos.matrix = contentHolder.transform.localToWorldMatrix;
			
			Bounds body = new Bounds(Vector3.zero,Vector3.zero);
			Vector3 min = Vector3.zero;
			Vector3 max = Vector3.zero;
			bool init = false;
			Renderer[] childRenderers = contentHolder.GetComponentsInChildren<Renderer>() as Renderer[];
			foreach( Renderer r in childRenderers )
			{
				Bounds b = r.bounds;
				if( !init )
				{
					min = b.min;
					max = b.max;
					init = true;
					continue;
				}
				
				min.x = Mathf.Min(min.x,b.min.x);
				min.y = Mathf.Min(min.y,b.min.y);
				min.z = Mathf.Min(min.z,b.min.z);
				max.x = Mathf.Max(max.x,b.max.x);
				max.y = Mathf.Max(max.y,b.max.y);
				max.z = Mathf.Max(max.z,b.max.z);
			}
			
			body.SetMinMax(min,max);
			
				
			Gizmos.DrawWireCube(body.center-contentHolder.transform.position, (body.extents*2)*1f/contentHolder.transform.lossyScale.x);
			
			
			Gizmos.matrix = Matrix4x4.identity;
		}
		
#endif
	}

}
