/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Gizmo
{

	[AddComponentMenu("Toggle Play/Gizmos/Draw Hierarchy")]
	public class GizmoHierarchy : MonoBehaviour
	{
		public Transform target = null;
		public bool drawAlways = false;
		
		public Color color = new Color(1,1,1,0.25f);
		public bool displayAsBones = true;
		const float width = 0.1f;
		
		public void Reset()
		{
			if( target == null )
				target = this.transform;
		}
		
		public void OnDrawGizmosSelected( )
		{
			if( target != null && !drawAlways)
			{
				Gizmos.color = color;
				DrawToChildren(target);
			}
		}
		
		public void OnDrawGizmos( )
		{
			if( target != null && drawAlways)
			{
				Gizmos.color = color;
				DrawToChildren(target);
			}
		}
		
		public void DrawToChildren(Transform t)
		{
			for( int i = 0; i < t.childCount; ++i )
			{
				Transform child = t.GetChild(i);
				
				if( (t.position - child.position).sqrMagnitude < 0.001f )
				{
					DrawToChildren(child);
					continue;
				}
				
				if( ! displayAsBones )
				{
					Gizmos.DrawLine(t.position, child.position);
					DrawToChildren(child);
					continue;
				}
				
				Mogwai.DrawKite(t,child,width);
				
				DrawToChildren(child);
			}
		}
	}

}
