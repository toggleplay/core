/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections.Generic;
using TogglePlay.Serializables;

namespace TogglePlay.Gizmo
{

	// if animated does not update and keep in place, will stay in points non animated position.

	[AddComponentMenu("Toggle Play/Gizmos/Draw Normals")]
	public class GizmoMeshNormals : MonoBehaviour
	{
		public MeshFilter targetMesh = null;
		public SkinnedMeshRenderer skinnedMesh = null;
		public float length = 0.3f;

		List<Vector3Pair> normals = new List<Vector3Pair>();
		
		
		public void Reset()
		{
			if( targetMesh == null )
				targetMesh = this.GetComponent<MeshFilter>();
		}
		
		public void OnDrawGizmosSelected( )
		{
			bool addToList = false;
			if( normals.Count == 0 )
				addToList = true;
			if( targetMesh != null )
			{
				Gizmos.matrix = targetMesh.transform.localToWorldMatrix;
				for( int i = 0; i < targetMesh.mesh.vertexCount; ++i )
				{
					Mesh m = targetMesh.sharedMesh;
					if( addToList )
					{
						normals.Add(new Vector3Pair(m.vertices[i], m.vertices[i] + (m.normals[i]*length)) );
						Gizmos.DrawLine(m.vertices[i], m.vertices[i] + (m.normals[i]*length));
					}
					else
						Gizmos.DrawLine(normals[i].a, normals[i].b);
				}
				Gizmos.matrix = Matrix4x4.identity;
			}
			else if( skinnedMesh != null )
			{
				Gizmos.matrix = skinnedMesh.transform.localToWorldMatrix;
				for( int i = 0; i < skinnedMesh.sharedMesh.vertexCount; ++i )
				{
					Mesh m = skinnedMesh.sharedMesh;
					if( addToList )
					{
						Vector3Pair pair = 
							new Vector3Pair(m.vertices[i], m.vertices[i] + (m.normals[i]*length));
						normals.Add(pair);
						Gizmos.DrawLine(m.vertices[i], m.vertices[i] + (m.normals[i]*length));
					}
					else
						Gizmos.DrawLine(normals[i].a, normals[i].b);
				}
				Gizmos.matrix = Matrix4x4.identity;
			}
		}
	}

}
