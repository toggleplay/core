/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Gizmo
{

	/// <summary>
	/// Draws a gizmo at the location of the gameObject 
	/// </summary>

	[AddComponentMenu("Toggle Play/Gizmos/At My Location")]
	public class GizmoAtSelf : MonoBehaviour
	{
		public enum EGizmoType { sphere = 0, cube };
		
		public Color selectedColor = Color.red;
		public bool drawSelected = true;
		public Color unselectedColor = Color.gray;
		public bool drawUnselected = false;
		
		public Vector3 offset = Vector3.zero;
		
		public float size = 1.0f;
		public bool wireframed = false;
		public EGizmoType type = EGizmoType.sphere;
		public void OnDrawGizmosSelected( )
		{
#if UNITY_EDITOR
			if( !drawSelected )
				return;
			
			Gizmos.color = selectedColor;
			
			if( type == EGizmoType.sphere )
			{
				if( wireframed )
				{
					Gizmos.DrawWireSphere( transform.position + offset, size );
				}
				else
				{
					Gizmos.DrawSphere( transform.position + offset, size );
				}
			}
			else if( type == EGizmoType.cube )
			{
				if( wireframed )
				{
					Gizmos.DrawWireCube( transform.position + offset, new Vector3(size,size,size) );
				}
				else
				{
					Gizmos.DrawCube( transform.position + offset, new Vector3(size,size,size) );
				}
			}
#endif
		}
		
		public void OnDrawGizmos( )
		{
#if UNITY_EDITOR
			if( !drawUnselected )
				return;
			
			Gizmos.color = unselectedColor;
			
			if( type == EGizmoType.sphere )
			{
				if( wireframed )
				{
					Gizmos.DrawWireSphere( transform.position + offset, size );
				}
				else
				{
					Gizmos.DrawSphere( transform.position + offset, size );
				}
			}
			else if( type == EGizmoType.cube )
			{
				if( wireframed )
				{
					Gizmos.DrawWireCube( transform.position + offset, new Vector3(size,size,size) );
				}
				else
				{
					Gizmos.DrawCube( transform.position + offset, new Vector3(size,size,size) );
				}
			}
#endif
		}
	}

}
