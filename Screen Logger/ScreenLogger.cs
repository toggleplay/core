﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay
{

	public class ScreenLogger : TogglePlay.Generics.InstantSingleton<ScreenLogger>
	{
		const int logsToDisplay = 7;
		const float eldestFadeAfter = 4f;
		const float newestFadeAfter = 9f;
		const float jumpPerEntry = 16;


		class LogEntry
		{
			public Color col = Color.white;
			public string toLog = "";
			public int entryNumber = 0;

			float triggerFullTime = 0;
			float currentY = 0;
			public float yValue
			{
				get
				{
					return currentY;
				}
			}

			public LogEntry( int logNumber, string l )
			{
				entryNumber = logNumber;
				toLog = l;
				triggerFullTime = Time.time;
				col.a = 0;
			}

			public LogEntry( int logNumber, string l, Color c )
			{
				entryNumber = logNumber;
				toLog = l;
				col = c;
				col.a = 0;
				triggerFullTime = Time.time;
			}

			public void DisplayFully()
			{
				triggerFullTime = Time.time;
			}

			public void Advance( int activeEntry )
			{
				float delta = Time.time - triggerFullTime;

				float a = col.a;// Mathf.Clamp01(delta*2f);// col.a;
				// advance to full for 0.5 seconds
				a = delta * 2 > a ? Mathf.Clamp01(delta*2f) : a;

				int jump = entryNumber - activeEntry;

				// fade out effect
				float fadeTime = (entryNumber - activeEntry) / logsToDisplay;
				if( delta > fadeTime+0.5f )
				{
					float added = Mathf.Lerp(newestFadeAfter, eldestFadeAfter, Mathf.Abs((float)jump) / (float)logsToDisplay);
					a *= Mathf.Lerp( 1, 0, Mathf.InverseLerp(fadeTime, fadeTime + added, delta-0.5f) );
				}
				col.a = a;

				// move into place
				float targetY = jumpPerEntry * jump;

				currentY += Time.deltaTime * 20;
				currentY = currentY > targetY ? targetY : currentY;
			}
		}


		static List<LogEntry> logs = new List<LogEntry>();
		static int logNumber = 0;

		public static void Log( string log )
		{
			logs.Add( new LogEntry( logNumber, log, Color.white) );
			for( int i = logs.Count-logsToDisplay < 0 ? 0 : logs.Count-logsToDisplay; i < logs.Count; ++i )
				logs[i].DisplayFully();
			logNumber ++;
			if( hasSingleton == false )
				Regenerate();
		}

		public static void Log( string log, Color c )
		{
			logs.Add( new LogEntry( logNumber, log, c) );
			for( int i = logs.Count-logsToDisplay < 0 ? 0 : logs.Count-logsToDisplay; i < logs.Count; ++i )
				logs[i].DisplayFully();
			logNumber ++;
			if( hasSingleton == false )
				Regenerate();
		}

		public static void Clear()
		{
			// clear the logs, this will instantly clear the view
			logNumber = 0;
			logs.Clear();
		}

		//----------------------------------------------------------

		protected override void OnEnable()
		{
			base.OnEnable();
			if( singletonInstance == this )
				DontDestroyOnLoad(this.gameObject);
		}

		void OnGUI()
		{
			if( logs.Count == 0 )
				return;

			// loop through either all loops or all until max are hit
			for( int i = logs.Count-logsToDisplay < 0 ? 0 : logs.Count-logsToDisplay; i < logs.Count; ++i )
			{
				logs[i].Advance(logs[logs.Count-1].entryNumber);
				GUI.contentColor = logs[i].col;
				GUI.Label( new Rect(10, Screen.height - 25 + logs[i].yValue, Screen.width-20, 25), logs[i].toLog);
			}
		}

		protected override void OnApplicationQuit()
		{
			logNumber = 0;
			logs.Clear();
		}
	}

}
