﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Super quick example of setting and accessing a value using the
 * TypeDictionary, the type dictionary stores keys unique per type and not
 * by all keys
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

using TogglePlay.Globals;

namespace TogglePlay.Examples
{

	public class GlobalsExample : MonoBehaviour
	{
		float t = 11f;
		void OnEnable()
		{
			TypeDictionary.Set<float>( "float", t );
		}

		void Start()
		{
			Debug.Log( TypeDictionary.ValuesAsString() );
			Debug.LogWarning( " " + TypeDictionary.ContainsValue(t) );
			Debug.Log( TypeDictionary.Get<float>( "float" ).ToString() );
		}
	}

}