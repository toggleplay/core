﻿using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class ToggleActivityOnKey : MonoBehaviour
	{
		public GameObject[] targets = new GameObject[]{};

		void Update()
		{
			// When ever the A key is pressed toggle the activity of the object
			// if running coroutine it will not continue when reenambed,
			// coroutineEx will resume. May break existing code if directly change
			if( Input.GetKeyDown( KeyCode.A ) )
			{
				for( int i=0; i<targets.Length; ++i )
					targets[i].SetActive( ! targets[i].activeSelf );
			}
		}
	}

}