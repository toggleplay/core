﻿using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class MoveBoxUpAsNormal : MonoBehaviour
	{
		[SerializeField] private bool beginOnStart = true;

		private Vector3 m_StartPostion = Vector3.zero;
		private bool m_IsRunning = false;

		public bool isRunning
		{
			get{ return m_IsRunning; }
		}

		void Start()
		{
			m_StartPostion = this.transform.position;

			if( beginOnStart )
				BeginMove();
		}

		public Coroutine BeginMove()
		{
			return StartCoroutine( MoveUpRoutine() );
		}

		IEnumerator MoveUpRoutine()
		{
			m_IsRunning = true;

			float t = 0f;
			while( t < 12f )
			{
				t += Time.deltaTime;

				this.transform.position = new Vector3( m_StartPostion.x, Mathf.Lerp( -2, 3, t/12f ), m_StartPostion.z );

				yield return null;
			}

			m_IsRunning = false;
			yield break;
		}
	}

}
