﻿using UnityEngine;
using System.Collections;

public class RoutineLoop : MonoBehaviourEx
{
	//----------------------------------------------------------------------------------
	// Inspector Variables
	//----------------------------------------------------------------------------------

	[Header( "Incremental Display Labels" )]

	[Tooltip( "Incremental displays to loop through and yield" )]
	[SerializeField] private IncrementalDisplay[] displays = new IncrementalDisplay[]{};


	//----------------------------------------------------------------------------------
	// Loop Methods
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Begin the loop on start
	/// </summary>
	void Start()
	{
		StartCoroutine( Loop() );
	}

	/// <summary>
	/// Constantly loops round yielding a display each frame
	/// if there value increases then it will yield until it has been updated
	/// </summary>
	IEnumerator Loop()
	{
		while( true )
		{
			for( int i=0; i<displays.Length; ++i )
				yield return displays[i];
		}
	}
}
