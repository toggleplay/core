﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A very impactical use, but an use of IYieldable
/// </summary>
public class IncrementalDisplay : MonoBehaviour, TogglePlay.IYieldable
{
	//----------------------------------------------------------------------------------
	// Inspector Variables
	//----------------------------------------------------------------------------------

	[SerializeField] private UnityEngine.UI.Text displayLabel = null;

	//----------------------------------------------------------------------------------
	// Member Variables
	//----------------------------------------------------------------------------------

	int m_Value = 1;
	int m_PrevValue = 1;

	//----------------------------------------------------------------------------------
	// IYieldable
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Interface of IYielable, If a class contains this, then it can be yielded from within a CoroutineEx
	/// </summary>
	public virtual IEnumerator Enumerator
	{
		get
		{
			if( m_Value != m_PrevValue )
				return Increase();

			return null;
		}
	}

	//----------------------------------------------------------------------------------
	// Standard Methods
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Initialise the label
	/// </summary>
	void Start()
	{
		displayLabel.text = m_Value.ToString();
	}

	/// <summary>
	/// Check if the S key is pressed to increase the value
	/// </summary>
	void Update()
	{
		// increase the number
		if( Input.GetKeyDown( KeyCode.S ) )
			m_Value++;
	}

	/// <summary>
	/// Fades out the label and then back in with the new value
	/// </summary>
	IEnumerator Increase()
	{
		for( float t=0.2f; t>0f; t-=Time.deltaTime )
		{
			Color c = displayLabel.color;
			c.a = t/0.2f;
			displayLabel.color = c;
			yield return null;
		}

		displayLabel.text = m_Value.ToString();
		m_PrevValue = m_Value;

		for( float t=0; t<0.2f; t+=Time.deltaTime )
		{
			Color c = displayLabel.color;
			c.a = t/0.2f;
			displayLabel.color = c;
			yield return null;
		}
	}


}
