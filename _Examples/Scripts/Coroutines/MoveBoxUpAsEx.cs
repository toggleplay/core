﻿using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class MoveBoxUpAsEx : MonoBehaviourEx
	{
		[SerializeField] private bool beginOnStart = true;

		private Vector3 m_StartPostion = Vector3.zero;
		private CoroutineEx m_MoveRoutine = null;

		public bool isMoving
		{
			get{ return m_MoveRoutine == null ? false : m_MoveRoutine.isRunning; }
		}

		void Start()
		{
			m_StartPostion = this.transform.position;

			if( beginOnStart )
				m_MoveRoutine = BeginMove();
		}

		public CoroutineEx BeginMove()
		{
			return StartCoroutine( MoveUpRoutine() );
		}

		IEnumerator MoveUpRoutine()
		{
			float t = 0f;
			while( t < 12f )
			{
				t += Time.deltaTime;

				this.transform.position = new Vector3( m_StartPostion.x, Mathf.Lerp( -2, 3, t/12f ), m_StartPostion.z );

				yield return null;
			}

			yield return new Yield.CompleteBreak( "Move completed" );
		}
	}

}
