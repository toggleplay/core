
using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class PausingCoroutines : MonoBehaviourEx
	{
		
		IEnumerator Start()
		{
			CoroutineEx e = StartCoroutine( ExtendedRoutine() );
			
			for( int i=0; i<=70; ++i )
			{
				if( i == 20 )
				{
					Debug.Log( "<color=magenta>Pausing</color>" );
					e.Pause();

				}
				else if( i == 30 )
				{
					Debug.Log( "<color=magenta>Resuming</color>" );
					e.Resume();
				}
				else if( i == 60 )
				{
					Debug.Log( "<color=magenta>Ending</color>" );
					e.Stop();
				}

				Debug.Log( "<color=magenta>start index : <b>" + i + "</b></color>");
				yield return null;
			}
		}

		IEnumerator ExtendedRoutine()
		{
			Debug.Log( "Begin ExtendedRoutine" );

			for( int counter=0; true; ++counter )
			{
				Debug.Log( "<color=cyan>ExtendedRoutine Counter = <b>" + counter.ToString() + "</b></color>" );
				if( counter != 0 && counter % 10 == 0 )
					yield return new TogglePlay.Yield.WaitForUpdates( 3 );

				yield return null;
			}
		}
	}

}
