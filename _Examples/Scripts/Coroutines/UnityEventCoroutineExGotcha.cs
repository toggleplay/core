﻿using UnityEngine;
using System.Collections;

public class UnityEventCoroutineExGotcha : MonoBehaviourEx
{
	/// <summary>
	/// MonoBehaviourEx new version of Coroutine will not work with unity event methods as coroutines
	/// </summary>
	IEnumerator Start()
	{

		// This doesnt work with MonoBehaviourEx due to IEnumerator Start being controlled by unity Coroutine
//		yield return StartCoroutine( Routing() );

		for( TogglePlay.CoroutineEx e = StartCoroutine( Routing() ); e.isRunning; )
			yield return null;
	}
	
	IEnumerator Routing()
	{
		for( int i=0; i<100; ++i )
			yield return null;
	}
}
