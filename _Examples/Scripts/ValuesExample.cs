﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Shows the different ValueTypes in the inspector for editing
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using TogglePlay.Values;

namespace TogglePlay.Examples
{

	public class ValuesExample : MonoBehaviour
	{
		public IntValue intVal = new IntValue();
		public FloatValue floatVal = new FloatValue();
		public DoubleValue doubleVal = new DoubleValue();
		public StringValue stringVal = new StringValue("");
		public BoolValue boolVal = new BoolValue(true);
		public Vector2Value vector2Value = new Vector2Value(new Vector2(1,0));
		public Vector3Value vector3Value = new Vector3Value(new Vector3(1,0,0));
		public Vector4Value vector4Value = new Vector4Value(new Vector4(1,0,0,0));
		public ColorValue colorValue = new ColorValue(new Color(1,1,1,1));
		public QuaternionValue quatValue = new QuaternionValue();
	}

}