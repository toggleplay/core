﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Example of using FloatValue type and logging it to
 * a the screen.
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TogglePlay.Values;

namespace TogglePlay.Examples
{

	public class ScreenLoggerExample : MonoBehaviour
	{
		void Start()
		{
			List<FloatValue> vals = new List<FloatValue>();
			vals.Add(new FloatValue(FloatValue.EConstantPropertyType.randomBetweenTwoConstants, 4, 5) );
			vals.Add(new FloatValue(2) );

			vals[0].CompareTo( vals[1] );

			foreach( TogglePlay.Values.FloatValue v in vals )
				Debug.Log( v.ToString() );

			vals.Sort( TogglePlay.Values.FloatValue.sortByProperty );

			foreach( TogglePlay.Values.FloatValue v in vals )
				Debug.Log( v.ToString() );
		}

		void Update()
		{
			if( Input.GetKeyDown(KeyCode.A) )
				ScreenLogger.Log("The quick brown fox jumped over the lazy dog");

			if( Input.GetKeyDown(KeyCode.S) )
				ScreenLogger.Log("Harry the bright green turtle puts its head on the pillow");

			if( Input.GetKeyDown(KeyCode.D) )
				ScreenLogger.Log("How much wood could a wood chuck wood, if a wood chuck could chuck wood");
		}
	}

}