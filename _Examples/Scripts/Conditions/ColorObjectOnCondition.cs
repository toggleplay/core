﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Sets the colour of a renderer if a condition is met
 * on a ConditionComponent
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class ColorObjectOnCondition : MonoBehaviour
	{
		[SerializeField] private Conditional.ConditionComponent condition = null;
		[SerializeField] private Color color = Color.black;

		//----------------------------------------------------------------------------------

		private Renderer myRenderer = null;

		//----------------------------------------------------------------------------------

		void Start()
		{
			myRenderer = GetComponent<Renderer>();
		}

		void Update()
		{
			if( myRenderer != null && condition != null && condition.ConditionMet )
			{
				myRenderer.material.color = color;
			}
		}
	}

}