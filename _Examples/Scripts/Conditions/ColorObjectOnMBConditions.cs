﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Finds any components inheriting ICondition on a game object
 * and if any are met sets the colour of the renderer
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class ColorObjectOnMBConditions : MonoBehaviour
	{
		[SerializeField] private MonoBehaviour monoBWithICondition = null;
		public Conditional.ICondition cond = null;
		[SerializeField] private Color color = Color.black;

		//----------------------------------------------------------------------------------

		private Renderer myRenderer = null;
		private Conditional.ICondition condition = null;

		//----------------------------------------------------------------------------------

		void OnValidate()
		{
			if( monoBWithICondition != null )
			{
				Conditional.ICondition c = monoBWithICondition as Conditional.ICondition;
				if( c == null )
					monoBWithICondition = null;
			}
		}

		void Start()
		{
			myRenderer = GetComponent<Renderer>();
			if( monoBWithICondition != null )
			{
				Conditional.ICondition c = monoBWithICondition as Conditional.ICondition;
				if( c != null )
					condition = c;
			}
		}

		void Update()
		{
			if( myRenderer != null && condition.ConditionMet )
			{
				myRenderer.material.color = color;
			}
		}
	}

}