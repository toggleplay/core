﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Finds any components inheriting ICondition on a game object
 * and if any are met sets the colour of the renderer
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{
	/// <summary>
	/// Example showing getting an array of components that inherit the ICondition interface from a gameobject
	/// and set a renderer color to black if any of the conditions are met
	/// </summary>
	public class ColorObjectOnObjectConditions : MonoBehaviour
	{
		[SerializeField] private GameObject conditionsOnObject = null;
		[SerializeField] private Color color = Color.black;

		//----------------------------------------------------------------------------------

		private Renderer myRenderer = null;
		private Conditional.ICondition[] conditions = null;

		//----------------------------------------------------------------------------------

		void Start()
		{
			myRenderer = GetComponent<Renderer>();
			if( conditionsOnObject != null )
			{
#if UNITY_4_6
				conditions = conditionsOnObject.GetComponents( typeof(Conditional.ICondition) ) as Conditional.ICondition[];
#else
				conditions = conditionsOnObject.GetComponents<Conditional.ICondition>();
#endif
			}
		}

		void Update()
		{
			if( myRenderer != null && TogglePlay.Conditional.ConditionComponent.AnyConditionMet(conditions) )
			{
				myRenderer.material.color = color;
			}
		}
	}

}