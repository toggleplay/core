﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Example showing a class inheriting from the RefMonoBehaviour
 * which keeps references to all component instances of the class and can be searched
 * for by a referenceID
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class RefOne : TogglePlay.RefMonoBehaviour<RefOne>
	{
		public void Print()
		{
			Debug.Log("Ref one");
		}
	}

}