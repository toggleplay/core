﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Example of accessing global instances of a class that
 * inherits from RefMonoBehaviour
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using TogglePlay;

namespace TogglePlay.Examples
{

	public class Refs : MonoBehaviour
	{
		public TogglePlay.Values.StringValue callOn;

		void Start()
		{
			for( int i=0; i<callOn.strings.Length; ++i )
			{
				RefOne one = RefOne.GetGlobal( callOn[i] );
				RefTwo two = RefTwo.GetGlobal( callOn[i] );

				if( one != null )
					one.Print();
				if( two != null )
					two.Print();
			}
		}
	}

}