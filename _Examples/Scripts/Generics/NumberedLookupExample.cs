﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Example of using the NumberedLookup to store values and
 * acess with a standard lookup method and an nterpolated lookup
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Examples
{

	public class NumberedLookupExample : MonoBehaviour
	{
		void Start()
		{
			StringLookup();
			LerpLookup();
		}

		void StringLookup()
		{
			Generics.NumberedLookup<string> lookup = new Generics.NumberedLookup<string>();

			// cannot find anything, default value will be null
			string st = "";
			lookup.TryGetValue( out st, 0.5f );
			Debug.Log( st );
			
			lookup.Add( "potatoes", 5f );
			lookup.Add( "pumpkin", 20 );
			
			// closest to 10 will be "potatoes"
			lookup.TryGetValue( out st, 10f );
			Debug.Log( st );
			
			// closest to 15 will be "pumpkin"
			st = lookup[15f];
			Debug.Log( st );
		}

		void LerpLookup()
		{
			Generics.NumberedLookup<Vector3> lookup = new Generics.NumberedLookup<Vector3>();
			
			// cannot find anything, default value will be null
			Vector3 a = Vector3.zero;
			Vector3 b = Vector3.zero;
			float v = 0;

			lookup.TryGetValue( out a, out b, out v, 0.5f );
			Debug.Log( "a=" + a + "   b=" + b );
			
			lookup.Add( Vector3.zero, 25f );
			lookup.Add( Vector3.one, 100f );
			
			// floating lookup finding value between value Vector3.zero and Vector3.one, 25/75 = 0.3
			lookup.TryGetValue( out a, out b, out v, 50f );
			Debug.Log( "Interpolated value between a and b = " + Vector3.Lerp(a,b,v).ToString() );
		}
	}

}