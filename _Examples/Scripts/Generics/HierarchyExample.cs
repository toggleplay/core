﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: an example showing ways of accessing and using
 * a hierarchy generic container
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

using TogglePlay.Generics;

namespace TogglePlay.Examples
{

	public class HierarchyExample : MonoBehaviour
	{
		Hierarchy<string, float> hierarchy = new Hierarchy<string, float>();

		void Start()
		{
			FillHierarchy();
		}
		
		void FillHierarchy()
		{
			// new to root
			hierarchy.Add("enemy", 0);

			// sub node to enemy
			hierarchy["enemy"].Add( "health", 1000 );
			hierarchy["enemy"].Add( "shield", 1000 );

			// add sub nodes to shield, using a string array for reference
			hierarchy[new string[]{ "enemy", "shield"}].Add( "defence boost", 200 );
			hierarchy[new string[]{ "enemy", "shield"}].Add( "block damage", 20 );

			// new node to enemy
			hierarchy["enemy"].Add( "damage", 15 );

			Debug.Log( hierarchy.ToString() );
		}
	}

}