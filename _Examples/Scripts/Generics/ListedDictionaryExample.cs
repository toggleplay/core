﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: An example of use for the ListedDictionary the ListedDictionary is a 
 * collection that stores both a List and a Dictionary, iterates the list and
 * does gets elements using the dictionary or different uses
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using TogglePlay.Generics;

namespace TogglePlay.Examples
{
	public class ListedDictionaryExample : MonoBehaviour
	{
		public ListedDictionary<int, float> lDict = new ListedDictionary<int, float>();

		void Start()
		{
			lDict.Add( 0, 0.1f );
			lDict.Add( 1, 1.2f );
			lDict.Add( 2, 2.3f );

			Debug.Log( "---------------------------" );
			Debug.Log( "Standard foreach (ListedDictionary<int,float>.KeyStore)" );

			foreach( ListedDictionary<int,float>.KeyStore ks in lDict )
			{
				Debug.Log("Key: " + ks.key);
				Debug.Log("Value: " + ks.value);
			}

			Debug.Log( "---------------------------" );
			Debug.Log( "------- Enumerable keys" );
			
			foreach( int i in lDict.GetKeys() )
				Debug.Log("Key foreach : " + i.ToString());

			Debug.Log( "------- Enumerable values" );

			foreach( float f in lDict.GetValues() )
				Debug.Log("Value foreach : " + f.ToString());

			Debug.Log( "---------------------------" );
			Debug.Log( "Count = " + lDict.Count );
			Debug.Log( "Key at index 1 = " + lDict[1].key );
			Debug.Log( "---------------------------" );
		}
	}
}
