﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace Decorator
{

	public class BoxHeaderAttribute : PropertyAttribute
	{
		public readonly string text;
		public readonly string tooltip;
		
		public BoxHeaderAttribute( string t )
		{
			text = t;
			tooltip = "";
		}

		public BoxHeaderAttribute( string header, string tip )
		{
			text = header;
			tooltip = tip;
		}
	}

}