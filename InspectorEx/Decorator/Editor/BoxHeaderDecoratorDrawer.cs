﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Decorator
{
	[CustomPropertyDrawer( typeof(BoxHeaderAttribute) )]
	public class BoxHeaderDecoratorDrawer : DecoratorDrawer
	{
		BoxHeaderAttribute conditionHeaderAttribute
		{
			get{ return attribute as BoxHeaderAttribute; }
		}

		public override float GetHeight ()
		{
			return base.GetHeight() + 6;
		}

		public override void OnGUI (Rect position)
		{
			position.height -= 2;
			GUIContent con = new GUIContent( conditionHeaderAttribute.text, conditionHeaderAttribute.tooltip );
			GUI.Box( position, con );
		}
	}

}