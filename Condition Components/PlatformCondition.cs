/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/By Platform")]
	public class PlatformCondition : ConditionComponent
	{
		public RuntimePlatform[] platforms;
		public bool restrictIfIsPlatform = true;
		
		protected override bool IsConditionMet()
		{
			foreach( RuntimePlatform platform in platforms )
			{
				if( Application.platform == platform )
				{
					if( restrictIfIsPlatform )
						return true;
					else
						return false;
				}
				
				if( ! restrictIfIsPlatform )
					return true;
			}
			return false;
		}
	}
}
