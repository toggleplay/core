/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Renderer Visible")]
	public class RendererVisibleCondition : ConditionComponent
	{
		public enum ERenderVisibleRestrictorSetting { isVisible = 0, isNotVisible };
		public Renderer ofRenderer = null;
		public ERenderVisibleRestrictorSetting whenIs = ERenderVisibleRestrictorSetting.isVisible;

		void Reset()
		{
			if( ofRenderer == null )
				ofRenderer = this.GetComponent<Renderer>();
		}

		protected override bool IsConditionMet()
		{
			if( ofRenderer == null )
				return false;

			if( whenIs == ERenderVisibleRestrictorSetting.isVisible )
			{
				if( ofRenderer.isVisible )
					return true;
			}
			else
			{
				if( ! ofRenderer.isVisible )
					return true;
			}

			return false;
		}
	}
}
