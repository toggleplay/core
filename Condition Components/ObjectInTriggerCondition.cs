/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Object In Trigger")]
	public class ObjectInTriggerCondition : ConditionComponent
	{
		public enum ERestrictWhen { whenInside = 0, whenOutside };
		public GameObject targetObject = null;
		public ERestrictWhen restrictWhen = ERestrictWhen.whenOutside;

		bool isInside = false;

		void OnTriggerEnter(Collider other)
		{
			if( other.gameObject == targetObject )
				isInside = true;
		}

		void OnTriggerExit(Collider other)
		{
			if( other.gameObject == targetObject )
				isInside = false;
		}

		protected override bool IsConditionMet()
		{
			if( restrictWhen == ERestrictWhen.whenInside )
				return isInside;
			else
				return !isInside;
		}
	}
}