/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

// TODO custom editor, only display ofClip when specific selected
namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/AudioSource")]
	public class AudioSourceCondition : ConditionComponent
	{
		public enum ConditionSettings { isPlaying = 0, isNotPlaying, isPlayingClip, isNotPlayingClip };
		public AudioSource ofAudioSource = null;
		public ConditionSettings whenIs = ConditionSettings.isPlaying;
		public string ofClip = "";

		void Reset()
		{
			if( ofAudioSource == null )
				ofAudioSource = this.GetComponent<AudioSource>();
		}

		protected override bool IsConditionMet()
		{
			if( ofAudioSource == null )
				return false;

			switch( whenIs )
			{
			case ConditionSettings.isPlaying:
				if( ofAudioSource.isPlaying )
					return true;
				break;
			case ConditionSettings.isNotPlaying:
				if( ! ofAudioSource.isPlaying )
					return true;
				break;
			case ConditionSettings.isPlayingClip:
				if( ofAudioSource.isPlaying )
				{
					if( ofAudioSource.clip.name == ofClip )
						return true;
				}
				break;
			case ConditionSettings.isNotPlayingClip:
				if( ofAudioSource.isPlaying )
				{
					if( ofAudioSource.clip.name != ofClip )
						return true;
				}
				else
					return true;
				break;
			}

			return false;
		}
	}
}
