/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;


// TODO custom editor to hide, ofValue unless enum at values
namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Children")]
	public class TransformChildrenCondition : ConditionComponent
	{
		public enum EWithChildren { withChildren = 0, withoutChildren, childrenLessThan, childrenEqualTo, childrenGreaterThen };
		public Transform ofTransform = null;
		public EWithChildren whenIs = EWithChildren.withChildren;
		public float ofValue = 0;
		
		protected override bool IsConditionMet()
		{
			if( ofTransform == null )
				return false;

			switch( whenIs )
			{
			case EWithChildren.withChildren:
				if( ofTransform.childCount > 0 )
					return true;
				break;
			case EWithChildren.withoutChildren:
				if( ofTransform.childCount == 0 )
					return true;
				break;
			case EWithChildren.childrenLessThan:
				if( ofTransform.childCount < ofValue )
					return true;
				break;
			case EWithChildren.childrenEqualTo:
				if( ofTransform.childCount == ofValue )
					return true;
				break;
			case EWithChildren.childrenGreaterThen:
				if( ofTransform.childCount > ofValue )
					return true;
				break;
			}

			return false;
		}
	}
}
