/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Transform scale along axis")]
	public class TransformScaleAxisCondition : ConditionComponent
	{
		public Transform ofTransform = null;
		public TogglePlay.Serializables.EAxis onAxis = TogglePlay.Serializables.EAxis.x;
		public EEquality whenValueIs = EEquality.greaterThan;
		public float ofValue = 0;
		public bool inLocalSpace = true;

		private float tempVal = 0;

		void Reset()
		{
			if( ofTransform == null )
				ofTransform = this.transform;
		}

		protected override bool IsConditionMet()
		{
			if( ofTransform == null )
				return false;

			if( onAxis == EAxis.x )
				tempVal = inLocalSpace == true ? ofTransform.localScale.x : ofTransform.lossyScale.x;
			else if( onAxis == EAxis.y )
				tempVal = inLocalSpace == true ? ofTransform.localScale.y : ofTransform.lossyScale.y;
			else
				tempVal = inLocalSpace == true ? ofTransform.localScale.z : ofTransform.lossyScale.z;

			switch( whenValueIs )
			{
			case EEquality.equalTo:
				return tempVal == ofValue ? true : false;
			case EEquality.greaterThan:
				return tempVal > ofValue ? true : false;
			case EEquality.greaterThanEqualTo:
				return tempVal >= ofValue ? true : false;
			case EEquality.lessThan:
				return tempVal < ofValue ? true : false;
			case EEquality.lessThenEqualTo:
				return tempVal <= ofValue ? true : false;
			}

			return false;
		}
	}
}