﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Contains a base, abstract class for derrived Conditional Components
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Conditional
{
	/// <summary>
	/// ConditionComponent class is used as a inheritable component for use in the inspector
	/// </summary>
	public abstract class ConditionComponent : MonoBehaviourEx, ICondition
	{
		//----------------------------------------------------------------------------------
		// Static helper functions
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Checks if any of the conditions are met.
		/// </summary>
		/// <returns><c>true</c>, if any condition has been met, <c>false</c> otherwise.</returns>
		/// <param name="conditions">Conditions to check.</param>
		public static bool AnyConditionMet( Conditional.ICondition[] conditions )
		{
			for( int i=0; i<conditions.Length; ++i )
				if( conditions[i].ConditionMet ) return true;
			return false;
		}

		//----------------------------------------------------------------------------------
		// Inheritable variables
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Forces ConditionMet to return true no matter the result of the IsConditionMet() function
		/// </summary>
		protected bool forceRestriction = false;

		//----------------------------------------------------------------------------------
		// 
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Gets a value indicating whether this <see cref="TogglePlay.Conditional.ConditionComponent"/> condition is met.
		/// </summary>
		/// <value><c>true</c> if condition met; otherwise, <c>false</c>.</value>
		public virtual bool ConditionMet
		{
			get
			{
				if( forceRestriction )
					return true;
				return IsConditionMet();
			}
		}

		/// <summary>
		/// Function defined in the derrived class to determine conditionality
		/// </summary>
		/// <returns><c>true</c> if this instance is condition is met; otherwise, <c>false</c>.</returns>
		protected abstract bool IsConditionMet();

		//----------------------------------------------------------------------------------
		// Operators
		//----------------------------------------------------------------------------------

		public static bool operator true(ConditionComponent condition)
		{
			return condition.ConditionMet;
		}

		public static bool operator false(ConditionComponent condition)
		{
			return ! condition.ConditionMet;
		}
	}
}
