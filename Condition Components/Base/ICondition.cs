﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Conditional
{

	public interface ICondition
	{
		/// <summary>
		/// Gets a value indicating whether this <see cref="TogglePlay.Conditional.ICondition"/> condition met.
		/// </summary>
		/// <value><c>true</c> if condition met; otherwise, <c>false</c>.</value>
		bool ConditionMet
		{
			get;
		}
	}

}
