﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using TogglePlay;

namespace TogglePlay.Conditional.Inspector
{

	[CustomEditor(typeof(ConditionComponent),true), CanEditMultipleObjects]
	public class ConditionComponentInspector : Editor
	{
		ConditionComponent s = null;
		Texture conditionIcon = null;
		Texture metIcon = null;
		Texture failedIcon = null;

		public override void OnInspectorGUI()
		{
			if( s == null )
				s = serializedObject.targetObject as ConditionComponent;
			if( conditionIcon == null )
				conditionIcon = Resources.Load<Texture>( "conditionIcon" );

			Rect r = EditorGUILayout.GetControlRect();
			float third = r.height * 0.33f;
			r.y += third;
			r.height += third;

			if( s.ConditionMet )
			{
				Rect icon = r;
				icon.width = icon.height;
				GUI.DrawTexture( icon, conditionIcon );

				if( metIcon == null )
					metIcon = Resources.Load<Texture>( "greenTickIcon" );
				icon.x += icon.width + 3;
				GUI.DrawTexture( icon, metIcon );

				Rect textRect = r;
				textRect.x += (icon.width * 2) + 9;
				textRect.width -= (icon.width * 2) + 9;

				EditorGUI.LabelField( textRect, "Condition Met" );
			}
			else
			{
				Rect icon = r;
				icon.width = icon.height;
				GUI.DrawTexture( icon, conditionIcon );
				
				if( failedIcon == null )
					failedIcon = Resources.Load<Texture>( "redCrossIcon" );
				icon.x += icon.width + 3;
				GUI.DrawTexture( icon, failedIcon );
				
				Rect textRect = r;
				textRect.x += (icon.width * 2) + 9;
				textRect.width -= (icon.width * 2) + 9;
				
				EditorGUI.LabelField( textRect, "Condition Failed" );
			}

			r = EditorGUILayout.GetControlRect();

			base.OnInspectorGUI();
			serializedObject.Update();
		}
	}

}
	