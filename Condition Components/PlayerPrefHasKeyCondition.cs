/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/PlayerPref HasKey")]
	public class PlayerPrefHasKeyCondition : ConditionComponent
	{
		public string keyName = "";
		public bool whenHasKey = true;
		
		protected override bool IsConditionMet()
		{
			if( keyName == "" )
				return false;
			
			if( PlayerPrefs.HasKey(keyName) )
			{
				if( whenHasKey )
					return true;
				else
					return false;
			}
			else if( ! whenHasKey )
				return true;
			
			return false;
		}
	}
}
