/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/TimeScale")]
	public class TimeScaleCondition : ConditionComponent
	{
		public enum ETimeScaleSetting { lessThan = 0, greaterThen };
		public ETimeScaleSetting whenTimeScaleIs = ETimeScaleSetting.lessThan;
		public float ofValue = 0;

		protected override bool IsConditionMet()
		{
			if( whenTimeScaleIs == ETimeScaleSetting.lessThan )
			{
				if( Time.timeScale <= ofValue )
					return true;
			}
			else if( Time.timeScale >= ofValue )
				return true;
			
			return false;
		}
	}
}
