/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Parent")]
	public class TransformParentCondition : ConditionComponent
	{
		public enum EWithParent { withParent = 0, withoutParent };
		public Transform ofTransform = null;
		public EWithParent whenIs = EWithParent.withoutParent;
		
		protected override bool IsConditionMet()
		{
			if( ofTransform == null )
				return false;

			if( whenIs == EWithParent.withParent )
			{
				if( ofTransform.parent == null )
					return false;
				return true;
			}
			else if( whenIs == EWithParent.withoutParent )
			{
				if( ofTransform.parent == null )
					return true;
				return false;
			}

			return false;
		}
	}
}
