/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/


using UnityEngine;

// TODO custom editor, only display ofClip when specific(enum) selected
namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Animation")]
	public class AnimationCondition : ConditionComponent
	{
		public enum ConditionSettings { isPlaying = 0, isNotPlaying, isPlayingSpecific, isNotPlayingSpecific }; // TODO rename specific to Clip
		public Animation ofAnimation = null;
		public ConditionSettings whenIs = ConditionSettings.isPlaying;
		public string ofClip = ""; // TODO AnimationClip instead of string

		void Reset()
		{
			if( ofAnimation == null )
				ofAnimation = this.GetComponent<Animation>();
		}

		protected override bool IsConditionMet()
		{
			if( ofAnimation == null )
				return false;

			switch( whenIs )
			{
			case ConditionSettings.isPlaying:
				if( ofAnimation.isPlaying )
					return true;
				break;
			case ConditionSettings.isNotPlaying:
				if( ! ofAnimation.isPlaying )
					return true;
				break;
			case ConditionSettings.isPlayingSpecific:
				if( ofAnimation.IsPlaying(ofClip) )
					return true;
				break;
			case ConditionSettings.isNotPlayingSpecific:
				if( ! ofAnimation.IsPlaying(ofClip) )
					return true;
				break;
			}

			return false;
		}
	}
}
