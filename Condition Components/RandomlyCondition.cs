/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Restrict Randomly")]
	public class RandomlyCondition : ConditionComponent
	{
		[Range(0.0f, 100.0f)]
		public float chanceToRestrict = 50f;

		protected override bool IsConditionMet()
		{
			if( Random.value < chanceToRestrict/100f )
				return true;
			return false;
		}
	}
}
