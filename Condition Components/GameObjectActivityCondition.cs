/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/GameObject activity")]
	public class GameObjectActivityCondition : ConditionComponent
	{
		public enum EActivity { ifActive = 0, ifInactive };
		public GameObject target = null;
		public EActivity onActivity = EActivity.ifActive;
		
		protected override bool IsConditionMet()
		{
			if( target == null )
				return false;
			
#if !(UNITY_3_5 || UNITY_3_0 || UNITY_3_0_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4)
			if( target.activeInHierarchy && onActivity == EActivity.ifActive )
				return true;
			else if( target.activeInHierarchy == false && onActivity == EActivity.ifInactive )
				return true;
#else
			if( target.active && onActivity == EActivity.ifActive )
				return true;
			else if( target.active == false && onActivity == EActivity.ifInactive )
				return true;
#endif
			return false;
		}
	}
}
