/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/PlayerPref Float")]
	public class PlayerPrefFloatCondition : ConditionComponent
	{
		public string keyName = "";
		public EEquality whenValueIs = EEquality.greaterThan;
		public float ofValue = 0;

		protected override bool IsConditionMet()
		{
			if( keyName == "" )
				return false;

			switch( whenValueIs )
			{
			case EEquality.equalTo:
				return PlayerPrefs.GetFloat(keyName) == ofValue ? true : false;
			case EEquality.greaterThan:
				return PlayerPrefs.GetFloat(keyName) > ofValue ? true : false;
			case EEquality.greaterThanEqualTo:
				return PlayerPrefs.GetFloat(keyName) >= ofValue ? true : false;
			case EEquality.lessThan:
				return PlayerPrefs.GetFloat(keyName) < ofValue ? true : false;
			case EEquality.lessThenEqualTo:
				return PlayerPrefs.GetFloat(keyName) <= ofValue ? true : false;
			}

			return false;
		}
	}
}