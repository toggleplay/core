/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Conditional.Conditions
{

	[AddComponentMenu("Toggle Play/Conditions/Distance To Transform")]
	public class DistanceToCondition : ConditionComponent
	{
		public enum ETimeScaleSetting { lessThan = 0, greaterThen };
		public Transform ofTransform = null;
		public ETimeScaleSetting whenDistanceIs = ETimeScaleSetting.lessThan;
		public Transform toTransform = null;
		public float ofValue = 0;

		protected override bool IsConditionMet()
		{
			if( ofTransform == null || toTransform == null )
				return false;
			
			if( whenDistanceIs == ETimeScaleSetting.lessThan )
			{
				if( (ofTransform.position - toTransform.position).sqrMagnitude <= ofValue * ofValue )
					return true;
			}
			else if( (ofTransform.position - toTransform.position).sqrMagnitude >= ofValue * ofValue )
				return true;
			
			return false;
		}

		void OnDrawGizmos()
		{
		}
	}

}
