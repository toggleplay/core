/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/Particle System")]
	public class ParticleSystemCondition : ConditionComponent
	{
		public enum EParticleSystemRestrictorSettings { whenPlaying = 0, whenPaused, whenStopped };
		public ParticleSystem system = null;
		public EParticleSystemRestrictorSettings whenIs = EParticleSystemRestrictorSettings.whenPaused;

		void Reset()
		{
			if( system == null )
				system = this.GetComponent<ParticleSystem>();
		}

		protected override bool IsConditionMet()
		{
			if( system == null )
				return false;

			switch( whenIs )
			{
			case EParticleSystemRestrictorSettings.whenPaused:
				if( system.isPaused )
					return true;
				break;
			case EParticleSystemRestrictorSettings.whenPlaying:
				if( system.isPlaying )
					return true;
				break;
			case EParticleSystemRestrictorSettings.whenStopped:
				if( system.isStopped )
					return true;
				break;
			}

			return false;
		}
	}
}
