/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

namespace TogglePlay.Conditional.Conditions
{
	[AddComponentMenu("Toggle Play/Conditions/PlayerPref String")]
	public class PlayerPrefStringCondition : ConditionComponent
	{
		public string keyName = "";
		public string prefValue = "";
		public bool isPrefValue = true;
		
		protected override bool IsConditionMet()
		{
			if( keyName == "" )
				return false;
			
			if( isPrefValue )
				return prefValue == PlayerPrefs.GetString(keyName) ? true : false;
			else
				return prefValue == PlayerPrefs.GetString(keyName) ? false : true;
		}
	}
}
