﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Globals
{
	//-----------------------------------------------------------------------------------------
	// SharedDictionary for values with shared keys between the whole dictionary
	//-----------------------------------------------------------------------------------------

	/// <summary>
	/// Global dictionary of objects where all types share the same unique keys.
	/// </summary>
	public static class SharedDictionary
	{
		static Dictionary<string,object> objectDictionary = new Dictionary<string, object>();

		/// <summary>
		/// Gets the amount of entries that are stored in the dictionary.
		/// </summary>
		/// <value>The count.</value>
		public static int Count { get { return objectDictionary.Count; } }


		//------------------------------------------------------------
		// Static functions to work with the dictionary
		//------------------------------------------------------------

		/// <summary>
		/// Clear the SharedDictionary of all entries.
		/// </summary>
		public static void Clear()
		{
			objectDictionary.Clear();
		}

		/// <summary>
		/// Check to see if the dictionary contains the specified key.
		/// </summary>
		/// <returns><c>true</c>, if key was found, <c>false</c> if the key was not found.</returns>
		/// <param name="key">Key to look for.</param>
		public static bool ContainsKey( string key )
		{
			return objectDictionary.ContainsKey(key);
		}
		
		/// <summary>
		/// Check to see if the dictionary contains the specified value.
		/// </summary>
		/// <returns><c>true</c>, if value was found, <c>false</c> if the value was not found.</returns>
		/// <param name="value">Value to look for.</param>
		public static bool ContainsValue( object value )
		{
			return objectDictionary.ContainsValue( value );
		}


		/// <summary>
		/// Remove the specified entry with <param name="key">key of entry to remove.</param> from the dictionary.
		/// </summary>
		/// <param name="key">Key of the entry to remove.</param>
		public static bool Remove( string key )
		{
			return objectDictionary.Remove(key);
		}


		/// <summary>
		/// Gets the value saved for the key given, and attempts to cast to type.
		/// In case of a value that cannot be cast to type, InvalidCastException is thrown.
		/// In case of value not found, NullReferenceException is thrown.
		/// </summary>
		/// <param name="key">Key to the value wanted.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Get<T>( string key )
		{
			object outObj = null;
			if( ! objectDictionary.TryGetValue( key, out outObj ) )
			{
				// no object with key saved.
				throw new System.IndexOutOfRangeException();
			}
			
			// try casting object to type
			try
			{
				return (T)outObj;
			}
			catch
			{
				throw new System.InvalidCastException();
			}
		}

		/// <summary>
		/// Gets the value specified by the key.
		/// In case of value not found, NullReferenceException is thrown.
		/// </summary>
		/// <param name="key">Key to value wanted.</param>
		public static object Get( string key )
		{
			object outObj = null;
			if( ! objectDictionary.TryGetValue( key, out outObj ) )
			{
				// no object with key saved.
				throw new System.IndexOutOfRangeException();
			}
			
			return outObj;
		}

		/// <summary>
		/// Sets valueOut to the value for the given key if found successful.
		/// </summary>
		/// <returns><c>true</c>, if get value was found successfully, <c>false</c> if the value was not found, or unsuccessfully cast.</returns>
		/// <param name="key">Key to the value.</param>
		/// <param name="valueOut">Value out.</param>
		/// <typeparam name="T">The 1st type to cast to.</typeparam>
		public static bool TryGetValue<T>( string key, out T valueOut )
		{
			object outObj = null;
			if( ! objectDictionary.TryGetValue( key, out outObj ) )
			{
				throw new System.IndexOutOfRangeException();
			}

			try
			{
				valueOut = (T)outObj;
				return true;
			}
			catch
			{
				throw new System.InvalidCastException();
			}
		}

		/// <summary>
		/// Sets valueOut to the value for the given key if found successful.
		/// </summary>
		/// <returns><c>true</c>, if get value was found successfully, <c>false</c> if the value was not found.</returns>
		/// <param name="key">Key to the value.</param>
		/// <param name="valueOut">Value out.</param>
		public static bool TryGetValue( string key, out object valueOut )
		{
			if( ! objectDictionary.TryGetValue( key, out valueOut ) )
			{
				valueOut = null;
				return false;
			}
			
			return true;
		}

		/// <summary>
		/// Sets the value of the specified key.
		/// </summary>
		/// <param name="key">Key of the value to set.</param>
		/// <param name="value">Value to set to.</param>
		public static void Set( string key, object value )
		{
			objectDictionary[key] = value;
		}
	}


	//-----------------------------------------------------------------------------------------
	// TypeDictionary for values with keys unique between the same type only.
	//-----------------------------------------------------------------------------------------

	/// <summary>
	/// Global dictionary for saving values of specified types into
	/// a dictionary for lookup later. example usage: save player score from game to scoreing scene
	/// </summary>
	public static class TypeDictionary
	{
		/// <summary>
		/// The Dictionary of Dictionarys so that each key given for globals is unique
		/// per type and not the whole system.
		/// </summary>
		static Dictionary< System.Type, Dictionary<string,object> > typeDictionary = new Dictionary<System.Type, Dictionary<string, object>>();

		/// <summary>
		/// Gets count of types stored.
		/// </summary>
		/// <value>The count of the number of types saved.</value>
		public static int TypeCount
		{
			get
			{
				return typeDictionary.Count;
			}
		}

		/// <summary>
		/// Get the count of values saved for a given type.
		/// </summary>
		/// <returns>The count of values saved for a given type.</returns>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static int CountOf<T>()
		{
			Dictionary<string,object> dictOut = new Dictionary<string, object>();

			if( typeDictionary.TryGetValue( typeof(T), out dictOut ) )
				return dictOut.Count;
			else
				return 0;

		}

		/// <summary>
		/// Gets the count of values stored for all types.
		/// </summary>
		/// <value>The count entries for all types.</value>
		public static int Count
		{
			get
			{
				int total = 0;
				foreach( KeyValuePair< System.Type, Dictionary<string,object> > kvp in typeDictionary )
				{
					total += kvp.Value.Count;
				}
				return total;
			}
		}

		//------------------------------------------------------------
		// Static functions to work with the dictionary
		//------------------------------------------------------------

		/// <summary>
		/// Clears all values saved into the static dictionary.
		/// </summary>
		public static void Clear()
		{
			typeDictionary.Clear();
		}

		/// <summary>
		/// Clears all values of type from the static dictionary.
		/// </summary>
		/// <typeparam name="T">The type to clear.</typeparam>
		public static void Clear<T>()
		{
			typeDictionary.Remove( typeof(T) );
		}

		/// <summary>
		/// Check to see if any type contains the given key.
		/// </summary>
		/// <returns><c>true</c>, if key was found, <c>false</c> if it was not.</returns>
		/// <param name="key">Key to check for.</param>
		public static bool ContainsKey( string key )
		{
			foreach( KeyValuePair< System.Type, Dictionary<string,object> > kvp in typeDictionary )
			{
				if( kvp.Value.ContainsKey(key) )
					return true;
			}

			return false;
		}

		/// <summary>
		/// Check to see if a specific type contains a entry for the given key.
		/// </summary>
		/// <returns><c>true</c>, if key was found, <c>false</c> if it was not.</returns>
		/// <param name="key">Key to check for.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool ContainsKey<T>( string key )
		{
			Dictionary<string,object> objDictionary = null;
			if( typeDictionary.TryGetValue( typeof(T), out objDictionary ) )
			{
				return objDictionary.ContainsKey(key);
			}

			return false;
		}
		
		/// <summary>
		/// Check to see if a value is stored within any of the types.
		/// </summary>
		/// <returns><c>true</c>, if value was found, <c>false</c> if it was not.</returns>
		/// <param name="value">Value to look for.</param>
		public static bool ContainsValue( object value )
		{
			// check through each not just type, as objects could be placed into a type of the class parent type.
			foreach( KeyValuePair< System.Type, Dictionary<string,object> > kvp in typeDictionary )
			{
				if( kvp.Value.ContainsValue(value) )
					return true;
			}
			
			return false;
		}

		/// <summary>
		/// Check to see if a value is stored for the given type.
		/// </summary>
		/// <returns><c>true</c>, if value was found, <c>false</c> if it was not.</returns>
		/// <param name="value">Value to search for.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool ContainsValue<T>( string value )
		{
			Dictionary<string,object> objDictionary = null;
			if( typeDictionary.TryGetValue( typeof(T), out objDictionary ) )
			{
				return objDictionary.ContainsValue(value);
			}
			
			return false;
		}


		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="TogglePlay.Globals.TypeDictionary"/> values stored within.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="TogglePlay.Globals.TypeDictionary"/> values stored within.</returns>
		public static string ValuesAsString()
		{
			string str = "";
			foreach( KeyValuePair< System.Type, Dictionary<string,object> > kvp in typeDictionary )
			{
				str += "---- type : " + kvp.Key.Name + " ----\n{\n";
				foreach( KeyValuePair< string,object > element in kvp.Value )
				{
					str += element.Key + ": " + System.Convert.ChangeType(element.Value, kvp.Key).ToString() + ",\n";
				}
				str += "}\n";
			}

			return str;
		}



		/// <summary>
		/// Remove the specified entry with the key key.
		/// </summary>
		/// <param name="key">Key.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool Remove<T>( string key )
		{
			Dictionary<string,object> objDictionary = null;
			if( typeDictionary.TryGetValue( typeof(T), out objDictionary ) )
			{
				return objDictionary.Remove(key);
			}

			return false;
		}



		/// <summary>
		/// Gets value saved for type with the key <param name="T">Saved object type.</param> to recover.
		/// </summary>
		/// <param name="key">Key name of the saved value.</param>
		/// <typeparam name="T">Saved object type.</typeparam>
		public static T Get<T>( string key )
		{
			Dictionary<string,object> objDictionary = null;
			if( ! typeDictionary.TryGetValue( typeof(T), out objDictionary ) )
			{
				// no dictionary set for the type
				throw new System.IndexOutOfRangeException();
			}

			object outObj = null;
			if( ! objDictionary.TryGetValue( key, out outObj ) )
			{
				// no object with key saved.
				throw new System.IndexOutOfRangeException();
			}

			// try casting object to type
			try
			{
				return (T)outObj;
			}
			catch
			{
				throw new System.InvalidCastException();
			}
		}



		/// <summary>
		/// Sets valueOut to the value for the given key if found successful.
		/// </summary>
		/// <returns><c>true</c>, if get value was found successfully, <c>false</c> if the key was not found.</returns>
		/// <param name="key">Key to the value.</param>
		/// <param name="valueObj">Value out.</param>
		/// <typeparam name="T">The type of value to look for.</typeparam>
		public static bool TryGetValue<T>( string key, out T valueOut )
		{
			Dictionary<string,object> objDictionary = null;
			if( ! typeDictionary.TryGetValue( typeof(T), out objDictionary ) )
			{
				// no dictionary set for the type
				throw new System.IndexOutOfRangeException();
			}
			
			object outObj = null;
			if( ! objDictionary.TryGetValue( key, out outObj ) )
			{
				// no object with key saved.
				throw new System.IndexOutOfRangeException();
			}
			
			// try casting object to type
			try
			{
				valueOut = (T)outObj;
				return true;
			}
			catch
			{
				throw new System.InvalidCastException();
			}
		}


		/// <summary>
		/// Adds or modifys value for the specified type and key. saved to <param name="T">Save to type dictionary.</param>.
		/// Can save to dictionary for inheritance dictionarys. e.g. saving all value to parent type for different entrys.
		/// </summary>
		/// <param name="key">Key.</param>
		/// <param name="value">Value.</param>
		/// <typeparam name="T">The type parameter to save to.</typeparam>
		public static void Set<T>( string key, object value )
		{
	#if UNITY_EDITOR
			// check to make sure the cast is value
			if( ! typeof(T).IsAssignableFrom(value.GetType()) )
			{
				Debug.LogWarning("Incorrect assignment of object and type, object cannot be cast as type.");
				return;
			}
	#endif

			Dictionary<string,object> objDictionary = null;
			if( ! typeDictionary.TryGetValue( typeof(T), out objDictionary ) )
			{
				// no dictionary set for the specified type, saving new.
				objDictionary = new Dictionary<string, object>(1);
				objDictionary.Add(key,value);
				typeDictionary.Add( typeof(T), objDictionary );
				return;
			}

			objDictionary[key] = value;
		}



		/// <summary>
		/// Adds or modifys value for the specified key and value, saved to value type.
		/// </summary>
		/// <param name="key">Key to the value to set.</param>
		/// <param name="value">Value to set as.</param>
		public static void Set( string key, object value )
		{
			System.Type valueType = value.GetType();
			Dictionary<string,object> objDictionary = null;
			if( ! typeDictionary.TryGetValue( valueType, out objDictionary ) )
			{
				// no dictionary set for the specified type, saving new.
				objDictionary = new Dictionary<string, object>(1);
				objDictionary.Add(key,value);
				typeDictionary.Add( valueType, objDictionary );
				return;
			}
			
			objDictionary[key] = value;
		}
	}
}
