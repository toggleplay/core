﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Globals
{

	public class SpriteRendererToGlobalSprite : MonoBehaviour
	{
		public SpriteRenderer target = null;
		public string key = "";

		void Reset()
		{
			if( target == null )
				target = this.GetComponent<SpriteRenderer>();
		}

		void Start()
		{
			if( target != null )
				target.sprite = TypeDictionary.Get<Sprite>( key );
		}
	}

}