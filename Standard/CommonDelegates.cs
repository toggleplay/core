/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;


namespace TogglePlay.Delegates
{
	public delegate void BlankDelegate( );
	public delegate void VoidDelegate( );

	public delegate void ObjectDelegate( object o );

	public delegate void StringDelegate( string s );
	public delegate void StringPairDelegate( string s1, string s2 );
	public delegate void CharDelegate( char c );
	public delegate void FloatDelegate( float f );
	public delegate void IntDelegate( int i );
	public delegate void BoolDelegate( bool b );
	public delegate void Vector2Delegate( Vector2 v );
	public delegate void Vector3Delegate( Vector3 v );
	public delegate void Vector3PairDelegate( Vector3 v1, Vector3 v2 );
	public delegate void Vector4Delegate( Vector4 v );
	
	public delegate void TextureDelegate( Texture t );
	public delegate void AudioSourceDelegate( AudioSource a );
	public delegate void ColliderDelegate( UnityEngine.Collider c );
	public delegate void GameObjectDelegate( UnityEngine.GameObject g );
}
