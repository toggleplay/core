/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

/// <summary>
/// A collections of enums and basic classes that are commonly used
/// Stored in a namespace to minimize need to creating new versions elsewhere in code
/// and common types so can be easily transfered between different classes.
/// </summary>
namespace TogglePlay.Serializables
{
	//----------------------------------------------------------------------------------
	// 
	//----------------------------------------------------------------------------------

	public enum EGreaterOrLess { greaterThan = 0, lessThan };
	public enum EEquality { lessThan = 0, lessThenEqualTo, equalTo, greaterThanEqualTo, greaterThan };
	public enum EQuaternionInterpolateMethod { lerp = 0, slerp };
	public enum EAxis { x = 0, y, z };
	public enum ETransform { position = 0, rotation, scale };

	//----------------------------------------------------------------------------------
	// 
	//----------------------------------------------------------------------------------
	
	[System.Serializable]
	public class NamedTransform
	{
		public string name = "target";
		public Transform targetTransform = null;
	}

	[System.Serializable]
	public class NamedGameObject
	{
		public string name = "target";
		public GameObject targetGameObject = null;
	}

	// TODO make IntVector2
	[System.Serializable]
	public class IntPair
	{
		public int value1 = 0;
		public int value2 = 0;
		public IntPair()
		{
			value1 = 0;
			value2 = 0;
		}
		public IntPair( int val1, int val2 )
		{
			value1 = val1;
			value2 = val2;
		}
	}

	// TODO masked propertdrawer,  use enum flags
	[System.Serializable]
	public class TransformBoolean
	{
		public bool position = true;
		public bool rotation = true;
		public bool scale = true;
		
		public TransformBoolean()
		{
		}
		
		public TransformBoolean( bool _p, bool _r, bool _s )
		{
			position = _p;
			rotation = _r;
			scale = _s;
		}
	}

	// TODO property draw   ,  use enum
	[System.Serializable]
	public class VectorBoolean
	{
		public bool x = true;
		public bool y = true;
		public bool z = true;
		
		public VectorBoolean()
		{
		}
		
		public VectorBoolean( bool _x, bool _y, bool _z )
		{
			x = _x;
			y = _y;
			z = _z;
		}
	}
	
	[System.Serializable]
	public class Vector3Pair
	{
		public Vector3 a = Vector3.zero;
		public Vector3 b = Vector3.zero;
		
		public Vector3Pair()
		{
		}
		
		public Vector3Pair( Vector3 _a, Vector3 _b )
		{
			a = _a;
			b = _b;
		}
	}
}
