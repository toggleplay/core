﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Generics
{
	/// <summary>
	/// Listed dictionary.
	/// provides a interface where data can be easily accessed as a list
	/// or dictionary form for different purposes. But at the expense of storing the data twice
	/// </summary>
	public class ListedDictionary<T1,T2>
	{
		public struct KeyStore
		{
			public T1 key;
			public T2 value;
			
			public KeyStore( T1 k, T2 v )
			{
				key = k;
				value = v;
			}
		}

		protected Dictionary<T1,T2> dictionary = new Dictionary<T1, T2>();
		protected List<KeyStore> list = new List<KeyStore>();
		
		public int Count{ get{ return list.Count; } }
		public int Capacity { get{ return list.Capacity; } }

		//----------------------------------------------------------------------------------
		// Access
		//----------------------------------------------------------------------------------

		public KeyStore[] ToArray()
		{
			return list.ToArray();
		}

		public List<KeyStore>.Enumerator GetEnumerator()
		{
			return list.GetEnumerator();
		}

		public IEnumerable<T1> GetKeys()
		{
			for( int i=0; i<list.Count; ++i )
				yield return list[i].key;
		}

		public IEnumerable<T2> GetValues()
		{
			for( int i=0; i<list.Count; ++i )
				yield return list[i].value;
		}


		public KeyStore this[int index] { get{ return list[index]; } }

		public T2 ValueAtIndex( int i )
		{
			if( i < 0 || i >= list.Count )
				return default(T2);
			return list[i].value;
		}

		public T1 KeyAtIndex( int i )
		{
			if( i < 0 || i >= list.Count )
				return default(T1);
			return list[i].key;
		}
		
		public bool TryGetValue( T1 key, out T2 value )
		{
			return dictionary.TryGetValue(key, out value);
		}

		//----------------------------------------------------------------------------------
		// Inserting
		//----------------------------------------------------------------------------------

		public bool Add( T1 key, T2 value )
		{
			if( dictionary.ContainsKey(key) )
				return false;
			dictionary.Add(key,value);
			list.Add(new KeyStore(key,value));
			return true;
		}

		//----------------------------------------------------------------------------------
		// Searching
		//----------------------------------------------------------------------------------

		public bool ContainsKey( T1 key )
		{
			return dictionary.ContainsKey(key);
		}

		public bool ContainsValue( T2 value )
		{
			for( int i=0; i<list.Count; ++i )
			{
				if( list[i].value.Equals(value) )
					return true;
			}
			return false;
		}

		//----------------------------------------------------------------------------------
		// Removal
		//----------------------------------------------------------------------------------

		public bool Remove( T1 key )
		{
			for( int i=0; i<list.Count; ++i )
			{
				if( list[i].key.Equals(key) )
				{
					list.RemoveAt(i);
					return dictionary.Remove(key);
				}
			}
			return false;
		}

		public void RemoveAt( int index )
		{
			if( index < 0 || index >= list.Count )
				return;

			dictionary.Remove(list[index].key);
			list.RemoveAt(index);
		}

		public int RemoveAllByValue( System.Predicate<T2> match )
		{
			int removed = 0;
			
			// Remove and count how many match the predicate
			for( int i=list.Count-1; i>=0; --i )
			{
				if( match.Invoke(list[i].value) )
				{
					dictionary.Remove(list[i].key);
					list.RemoveAt(i);
					removed++;
				}
			}
			
			return removed;
		}

		public int RemoveAllByKey( System.Predicate<T1> match )
		{
			int removed = 0;
			
			// Remove and count how many match the predicate
			for( int i=list.Count-1; i>=0; --i )
			{
				if( match.Invoke(list[i].key) )
				{
					dictionary.Remove(list[i].key);
					list.RemoveAt(i);
					removed++;
				}
			}
			
			return removed;
		}

		public void Clear()
		{
			dictionary.Clear();
			list.Clear();
		}
	}
}

