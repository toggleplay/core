﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Generics
{
	public partial class Hierarchy<K,V>
	{
		//----------------------------------------------------------------------------------

		/// <summary>
		/// The root element of the hierarchy, This is private and contains no data other than children.
		/// </summary>
		HierarchyElement root = null;

		//----------------------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Initializes a new instance of the <see cref="TogglePlay.Generics.Hierarchy`2"/> class with no elements.
		/// </summary>
		public Hierarchy()
		{
			root = new HierarchyElement(default(K), default(V));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TogglePlay.Generics.Hierarchy`2"/> class.
		/// if newElement.Key is empty then all children will be added to the first hierarchy level,
		/// else the new element will be added to the first hierarchy level.
		/// </summary>
		/// <param name="root">Root.</param>
		public Hierarchy( HierarchyElement newElement )
		{
			root = new HierarchyElement(default(K), default(V));
			if( object.Equals( newElement.Key, null ) )
			{
				for( int i=0; i<newElement.Count; ++i )
					Add( newElement[i] );
			}
			else
				Add(newElement);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TogglePlay.Generics.Hierarchy`2"/> class with an array of elements.
		/// </summary>
		/// <param name="newElements">New elements.</param>
		public Hierarchy( HierarchyElement[] newElements )
		{
			root = new HierarchyElement(default(K), default(V));
			for( int i=0; i<newElements.Length; ++i )
				Add( newElements[i] );
		}

		//----------------------------------------------------------------------------------
		// 
		//----------------------------------------------------------------------------------

		public int Count
		{
			get{ return root.Count; }
		}

		//----------------------------------------------------------------------------------
		// Access
		//----------------------------------------------------------------------------------

		public HierarchyElement this[int index]
		{
			get
			{
				return root[index];
			}
		}

		/// <summary>
		/// Gets the element at the specified index.
		/// Does not check for within range.
		/// </summary>
		/// <returns>The element stored at child index.</returns>
		/// <param name="index">Index of the child element.</param>
		public HierarchyElement GetElement( int index )
		{
			return root[index];
		}

		/// <summary>
		/// Gets the value at specified index.
		/// Does not check for within range.
		/// </summary>
		/// <returns>The value stored at child index.</returns>
		/// <param name="index">Index of the child element.</param>
		public V GetValue( int index )
		{
			return root[index].Value;
		}

		public HierarchyElement this[K key]
		{
			get
			{
				return root[key];
			}
		}

		public HierarchyElement GetElement( K key )
		{
			return root[key];
		}

		/// <summary>
		/// Gets the value with the specified key.
		/// Does not check for within range.
		/// </summary>
		/// <returns>The value of the child with key.</returns>
		/// <param name="key">Key to search for.</param>
		public V GetValue( K key )
		{
			return root[key].Value;
		}

		public HierarchyElement this[K[] keyPath]
		{
			get { return root.GetElement(keyPath); }
		}

		public HierarchyElement GetElement( K[] keyPath )
		{
			HierarchyElement e = root;
			if( e == null || keyPath == null || keyPath.Length == 0 )
				return null;

			for( int i=0; i<keyPath.Length; ++i )
			{
				e = e[keyPath[i]];
				if( e == null )
					break;
			}

			return e;
		}

		//----------------------------------------------------------------------------------
		// Insertion
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Adds a new child element.
		/// </summary>
		/// <param name="key">Key.</param>
		/// <param name="value">Value.</param>
		public HierarchyElement Add( K key, V value )
		{
			return root.Add(key, value);
		}

		/// <summary>
		/// Adds a new child element.
		/// </summary>
		/// <param name="element">Element to add.</param>
		public HierarchyElement Add( HierarchyElement element )
		{
			return root.Add(element);
		}

		/// <summary>
		/// Inserts a new element at a specific index.
		/// </summary>
		/// <param name="index">Index to insert into.</param>
		/// <param name="key">Key.</param>
		/// <param name="value">Value.</param>
		public HierarchyElement Insert( int index, K key, V value )
		{
			return root.Insert(index, key, value);
		}

		//----------------------------------------------------------------------------------
		// Other
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Returns a string representing each of the child elements within the hierarchy.
		/// </summary>
		public override string ToString()
		{
			string str = "";
			for( int i=0; i<root.Count; ++i )
			{
				if( i != 0 )
					str += "\n";
				str += root[i].ToString();
			}

			return str;
		}

		/// <summary>
		/// Clears the hierarchy of all children.
		/// </summary>
		public void Clear()
		{
			root = null;
		}
	}
}
