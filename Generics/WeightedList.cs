﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// List of values with weights,
/// Used to obtain random values based on chance weights
/// </summary>
public class WeightedList<T>
{
	protected struct WeightStore
	{
		public float weight;
		public T value;

		public WeightStore( T v, float w )
		{
			value = v;
			weight = w;
		}

		public void SetWeight( float w )
		{
			weight = w;
		}
	}

	//----------------------------------------------------------------------------------

	protected List<WeightStore> collection = new List<WeightStore>();

	//----------------------------------------------------------------------------------
	// Inserting
	//----------------------------------------------------------------------------------

	public void Add( T value, float weight )
	{
		// See if it contains the value to be added, if it does just change its weight.
		for( int i=0; i<collection.Count; ++i )
		{
			if( value.Equals(collection[i].value) )
			{
				collection[i].SetWeight(weight);
				return;
			}
		}

		// is not in the collection yet, just add a new one
		collection.Add( new WeightStore( value, weight ) );
	}

	public void Insert( int index, T value, float weight )
	{
		// See if it contains the value to be added, if it does remove it, ready to be inserted elsewhere
		for( int i=collection.Count-1; i>=0; --i )
		{
			if( value.Equals(collection[i].value) )
			{
				collection.RemoveAt(i);
				break;
			}
		}
		collection.Insert(index, new WeightStore(value,weight));
	}

	//----------------------------------------------------------------------------------
	// Utility
	//----------------------------------------------------------------------------------

	public int Count { get{ return collection.Count; } }
	public int Capacity { get{ return collection.Capacity; } }

	public void Sort()
	{
		collection.Sort();
	}

	public void Reverse()
	{
		collection.Reverse();
	}
	public void Reverse( int index, int count )
	{
		collection.Reverse(index,count);
	}

	//----------------------------------------------------------------------------------
	// Searching
	//----------------------------------------------------------------------------------

	public int FindIndex( System.Predicate<T> match )
	{
		for( int i=0; i<collection.Count; ++i )
		{
			if( match.Invoke(collection[i].value) )
				return i;
		}

		return -1;
	}

	public int FindIndex( int startIndex, System.Predicate<T> match )
	{
		for( int i=startIndex; i<collection.Count; ++i )
		{
			if( match.Invoke(collection[i].value) )
				return i;
		}
		
		return -1;
	}

	public int FindLastIndex( System.Predicate<T> match )
	{
		for( int i=collection.Count-1; i>=0; --i )
		{
			if( match.Invoke(collection[i].value) )
				return i;
		}
		
		return -1;
	}
	
	public int FindLastIndex( int startIndex, System.Predicate<T> match )
	{
		for( int i=startIndex; i>=0; --i )
		{
			if( match.Invoke(collection[i].value) )
				return i;
		}
		
		return -1;
	}


	public bool Contains( T value )
	{
		for( int i=0; i<collection.Count; ++i )
		{
			if( collection[i].value.Equals(value) )
				return true;
		}

		return false;
	}

	public int IndexOf( T value )
	{
		for( int i=0; i<collection.Count; ++i )
		{
			if( collection[i].value.Equals(value) )
				return i;
		}
		
		return -1;
	}

	public bool Exists( System.Predicate<T> match )
	{
		for( int i=0; i<collection.Count; ++i )
		{
			if( match.Invoke(collection[i].value) )
				return true;
		}
		
		return false;
	}

	public T Find( System.Predicate<T> match )
	{
		for( int i=0; i<collection.Count; ++i )
		{
			if( match.Invoke(collection[i].value) )
				return collection[i].value;
		}
		
		return default(T);
	}

	public T FindLast( System.Predicate<T> match )
	{
		for( int i=collection.Count-1; i>=0; --i )
		{
			if( match.Invoke(collection[i].value) )
				return collection[i].value;
		}
		
		return default(T);
	}

	public List<T> FindAll( System.Predicate<T> match )
	{
		List<T> returnList = new List<T>();
		for( int i=0; i<collection.Count; ++i )
		{
			if( match.Invoke(collection[i].value) )
				returnList.Add(collection[i].value);
		}
		
		return returnList;
	}

	//----------------------------------------------------------------------------------
	// Access Functions
	//----------------------------------------------------------------------------------

	public T this[int index] { get{ return collection[index].value; } }

	public float GetWeightForIndex( int index )
	{
		return collection[index].weight;
	}

	public T GetValueForIndex( int index )
	{
		return collection[index].value;
	}

	public T[] ToArray()
	{
		T[] ary = new T[collection.Count];

		// fill the array with all values
		for( int i=0; i<collection.Count; ++i )
			ary[i] = collection[i].value;
		
		return ary;
	}

	public T WeightedRandom()
	{
		float w = 0;
		
		// find the total weight available
		for( int i=0; i<collection.Count; ++i )
			w += collection[i].weight;
		
		// choose a random value within the total weight
		float r = Random.Range(0,w);
		w = 0;
		for( int i=0; i<collection.Count; ++i )
		{
			w += collection[i].weight;
			if( w > r )
				return collection[i].value;
		}
		
		// No values were found, returning default
		return default(T);
	}

	public T WeightedRandom( System.Predicate<T> match )
	{
		float w = 0;

		// find the total weight available
		for( int i=0; i<collection.Count; ++i )
		{
			if( ! match.Invoke(collection[i].value) )
				continue;
			w += collection[i].weight;
		}

		// choose a random value within the total weight
		float r = Random.Range(0,w);
		w = 0;
		for( int i=0; i<collection.Count; ++i )
		{
			if( ! match.Invoke(collection[i].value) )
				continue;
			w += collection[i].weight;
			if( w > r )
				return collection[i].value;
		}

		// No values were found, returning default
		return default(T);
	}


	//----------------------------------------------------------------------------------
	// Removal
	//----------------------------------------------------------------------------------

	public bool Remove( T value )
	{
		for( int i=0; i<collection.Count; ++i )
		{
			if( collection[i].value.Equals(value) )
			{
				collection.RemoveAt(i);
				return true;
			}
		}
		return false;
	}

	public void RemoveAt( int index )
	{
		collection.RemoveAt(index);
	}

	public void RemoveRange( int index, int count )
	{
		collection.RemoveRange(index,count);
	}


	public int RemoveAll( System.Predicate<T> match )
	{
		int removed = 0;

		// Remove and count how many match the predicate
		for( int i=collection.Count-1; i>=0; --i )
		{
			if( match.Invoke(collection[i].value) )
			{
				collection.RemoveAt(i);
				removed++;
			}
		}

		return removed;
	}

	public void Clear()
	{
		collection.Clear();
	}
}
