﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Generics
{

	public partial class Hierarchy<K,V>
	{
		/// <summary>
		/// Copy of DynamicList as cannot use nested generics with some compilers
		/// </summary>
		public class HierarchyElementList : IEnumerable<HierarchyElement>
		{
			protected HierarchyElement[] array;
			
			private int length = 0;
			
			//----------------------------------------------------------------------------------
			// General
			//----------------------------------------------------------------------------------
			
			public int Count
			{
				get{ return length; }
			}
			
			//----------------------------------------------------------------------------------
			// Enumerators
			//----------------------------------------------------------------------------------
			
			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}
			
			// to allow foreach
			public IEnumerator<HierarchyElement> GetEnumerator()
			{
				if( array != null )
				{
					for( int i = 0; i < length; ++i )
					{
						yield return array[i];
					}
				}
				
				yield break;
			}
			
			//----------------------------------------------------------------------------------
			// Access
			//----------------------------------------------------------------------------------
			
			
			public HierarchyElement this[int i]
			{
				get{ return array[i]; }
				set{ array[i] = value; }
			}
			
			public HierarchyElement[] ToArray()
			{
				if( array == null || array.Length == 0 )
					return new HierarchyElement[0];
				
				HierarchyElement[] rtn = new HierarchyElement[length];
				System.Array.Copy( array, 0, rtn, 0, length );
				
				return rtn;
			}
			
			public bool Contains( HierarchyElement item )
			{
				if( array == null )
					return false;
				
				for( int i = 0; i < length; ++i )
				{
					if( array[i].Equals(item) )
						return true;
				}
				
				return false;
			}
			
			
			public int IndexOf( HierarchyElement item )
			{
				if( array == null )
					return -1;
				
				for( int i = 0; i < length; ++i )
				{
					if (array[i].Equals(item))
						return i;
				}
				
				return -1;
			}
			
			//----------------------------------------------------------------------------------
			// Size
			//----------------------------------------------------------------------------------
			
			public int Capacity
			{
				get{ return array.Length; }
				set
				{
					HierarchyElement[] newCap = new HierarchyElement[value];
					if( array != null )
						System.Array.Copy( array, 0, newCap, 0, System.Math.Min(value, length) );

					length = System.Math.Min(value, length);
					array = newCap;
				}
			}
			
			void ExpandCapacity()
			{
				Capacity = array == null ? 32 : System.Math.Max(array.Length << 1, 32);
			}
			
			//----------------------------------------------------------------------------------
			// Clearing
			//----------------------------------------------------------------------------------
			
			public void Clear( bool clearMemory = false )
			{
				length = 0;
				if( clearMemory )
					array = null;
			}
			
			//----------------------------------------------------------------------------------
			// Insertion
			//----------------------------------------------------------------------------------
			
			public void Add( HierarchyElement item )
			{
				if( array == null || length == array.Length )
					ExpandCapacity();
				
				array[length++] = item;
			}
			
			
			public void Insert( int index, HierarchyElement item )
			{
				// make sure there is room
				if (array == null || length == array.Length)
					ExpandCapacity();
				
				if( index < length )
				{
					// push everything up
					for( int i = length; i > index; --i )
						array[i] = array[i - 1];
					
					// replace the position element
					array[index] = item;
					++length;
				}
				else
					Add( item );
			}
			
			//----------------------------------------------------------------------------------
			// Removal
			//----------------------------------------------------------------------------------
			
			public bool Remove( HierarchyElement item )
			{
				if (array != null)
				{
					for( int i = 0; i < length; ++i )
					{
						if( array[i].Equals(item) )
						{
							RemoveAt(i);
							return true;
						}
					}
				}
				return false;
			}

			public int RemoveAll( System.Predicate<HierarchyElement> match )
			{
				int removed = 0;
				if( array != null )
				{
					for( int i = 0; i < length; ++i )
					{
						if( match.Invoke( array[i] ) )
						{
							removed++;
						}
						else if( removed > 0 )
						{
							// move down by removed count
							array[i-removed] = array[i];
						}
					}
				}
				
				// nullify the top 'removed'
				for( int i=length-1; i>length-1-removed; --i )
					array[i] = null;
				
				length -= removed;
				return removed;
			}
			
			public void RemoveAt( int index )
			{
				if( array == null || index >= length )
					return;
				
				--length;
				
				for( int i = index; i < length; ++i )
					array[i] = array[i + 1];
				
				array[length] = null;
			}
		}

		///<summary>
		///The hierarchy element defines a single element within the hierarchy
		///</summary>
		public class HierarchyElement
		{
			protected K myKey;
			protected V myValue;

			protected HierarchyElement parent = null;
			protected HierarchyElementList children = null;


			//----------------------------------------------------------------------------------
			// Constructors
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Initializes a new instance of the <see cref="TogglePlay.Generics.Hierarchy`2+HierarchyElement"/> class.
			/// </summary>
			/// <param name="key">Element Key.</param>
			/// <param name="value">Element Value.</param>
			public HierarchyElement( K key, V value )
			{
				myKey = key;
				myValue = value;
				children = new HierarchyElementList();
			}

			//----------------------------------------------------------------------------------
			// 
			//----------------------------------------------------------------------------------

			/// <summary>
			/// Parent HierarchyElement this Element is a child of
			/// </summary>
			public HierarchyElement Parent
			{
				get{ return parent; }
			}

			public int Count
			{
				get{ return children.Count; }
			}

			public K Key
			{
				get{ return myKey; }
			}

			public V Value
			{
				get{ return myValue; }
			}

			//----------------------------------------------------------------------------------
			// Element Access
			//----------------------------------------------------------------------------------

			public HierarchyElement[] ToArray()
			{
				return children.ToArray();
			}

			public bool ContainsChildKey(K key)
			{
				for( int i=0; i<children.Count; ++i )
				{
					if( children[i].myKey.Equals(key) )
						return true;
				}
				
				return false;
			}

			public HierarchyElement this[int index]
			{
				get
				{
					if( index < 0 || index >= children.Count )
						return null;
					return children[index];
				}
			}
			public HierarchyElement GetElement( int index )
			{
				return this[index];
			}

			public V GetValue( int index )
			{
				return this[index].Value;
			}

			public HierarchyElement this[K key]
			{
				get
				{
					for( int i=0; i<children.Count; ++i )
					{
						if( children[i].myKey.Equals(key) )
							return children[i];
					}
					return null;
				}
			}
			public HierarchyElement GetElement( K key )
			{
				return this[key];
			}

			public V GetValue( K key )
			{
				return this[key].Value;
			}

			public HierarchyElement this[K[] keyPath]
			{
				get{ return GetElement(keyPath); }
			}
			public HierarchyElement GetElement( K[] keyPath )
			{
				if( keyPath == null || keyPath.Length == 0 )
					return null;

				HierarchyElement e = this;
				for( int i=0; i<keyPath.Length; ++i )
				{
					e = e[keyPath[i]];
					if( e == null )
						break;
				}
				
				return e;
			}


			//----------------------------------------------------------------------------------
			// Add Child functions
			//----------------------------------------------------------------------------------

			public HierarchyElement Add( K key, V value )
			{
				if( ContainsChildKey(key) )
				{
					// Key already exists within heirarchy nodes children
					return null;
				}

				HierarchyElement childElement = new HierarchyElement(key, value);
				childElement.parent = this;

				children.Add(childElement);
				return childElement;
			}

			public HierarchyElement Add( HierarchyElement childElement )
			{
				if( childElement == null || ContainsChildKey(childElement.Key) )
				{
					Debug.LogError("Key already exists within heirarchy nodes children");
					return null;
				}

				if( childElement.parent != null && childElement.parent != this )
					childElement.parent.Remove(childElement.myKey);

				childElement.parent = this;
				children.Add(childElement);

				return childElement;
			}

			public HierarchyElement Insert( int index, K key, V value )
			{
				if( ContainsChildKey(key) )
					return null;

				HierarchyElement childElement = new HierarchyElement(key, value);
				childElement.parent = this;

				children.Insert(index, childElement);
				return childElement;
			}

			//----------------------------------------------------------------------------------
			// Remove Child Functions
			//----------------------------------------------------------------------------------

			public void Remove( K key )
			{
				int contained = -1;
				for( int i=0; i<children.Count; ++i )
				{
					if( children[i].myKey.Equals(key) )
					{
						contained = i;
						break;
					}
				}

				if( contained == -1 )
					return;

				children.RemoveAt(contained);
			}

			public int RemoveAll( System.Predicate<HierarchyElement> match )
			{
				return children.RemoveAll(match);
			}

			//----------------------------------------------------------------------------------
			// Other
			//----------------------------------------------------------------------------------

			public override string ToString()
			{
				string self = string.Format("[Element: Key={0}, Value={1}, ", Key, Value);
				for( int i=0; i<children.Count; ++i )
					self += "Child"+i.ToString() + "=" + children[i].ToString();
				return self + "]";
			}
		}

	}

}
