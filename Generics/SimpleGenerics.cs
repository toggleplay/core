﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Generics
{
	public struct TypeX2 <T>
	{
		public T value1;
		public T value2;

		public TypeX2( T value1, T value2 )
		{
			this.value1 = value1;
			this.value2 = value2;
		}
	}

	public struct TypeX3 <T>
	{
		public T value1;
		public T value2;
		public T value3;
		
		public TypeX3( T value1, T value2, T value3 )
		{
			this.value1 = value1;
			this.value2 = value2;
			this.value3 = value3;
		}
	}

	public struct TypeX4 <T>
	{
		public T value1;
		public T value2;
		public T value3;
		public T value4;

		public TypeX4( T value1, T value2, T value3, T value4 )
		{
			this.value1 = value1;
			this.value2 = value2;
			this.value3 = value3;
			this.value4 = value4;
		}
	}
}
