﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery: Class used to store values in a lookup by number.
 * number can be used to get value closest to, or find an interpolation between values
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Generics
{

	public class NumberedLookup<T>
	{
		//----------------------------------------------------------------------------------
		// working values
		//----------------------------------------------------------------------------------

		int index = 0;
		float key = 0;
		int indexLow = 0;
		int indexHigh = 0;

		float low = 0;
		float high = 0;

		T[] tableValues = null;
		float[] tableKeys = null;

		//----------------------------------------------------------------------------------
		// Parameters
		//----------------------------------------------------------------------------------

		public int Count
		{
			get{ return tableKeys == null ? 0 : tableKeys.Length; }
		}

		//----------------------------------------------------------------------------------
		// Acess functions
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Puts the closest value to lookup into the closestValue parameter
		/// </summary>
		/// <returns><c>true</c>, if get value was successful, <c>false</c> otherwise.</returns>
		/// <param name="closestValue">Closest value out.</param>
		/// <param name="lookup">Lookup.</param>
		public bool TryGetValue( out T closestValue, float lookup )
		{
			int length = Count;
			if( length == 0 )
			{
				closestValue = default(T);
				return false;
			}
			else if( length == 1 )
			{
				closestValue = tableValues[0];
				return true;
			}

			index = length/2;
			key = tableKeys[index];
			
			indexLow = 0;
			indexHigh = length-1;
			closestValue = default(T);

			while( true )
			{
				if( key < lookup )
				{
					// value is higher then current indexValue
					
					if( index == indexHigh-1 )
					{
						// distances from value
						float low = Mathf.Abs(tableKeys[index] - lookup);
						float high = Mathf.Abs(tableKeys[indexHigh] - lookup);

						// check which is closer to value
						if( low < high )
							// the value is closest to low
							closestValue = tableValues[index];
						else
							// the value is closest to high
							closestValue = tableValues[indexHigh];

						return true;
					}
					else if( index == indexHigh )
					{
						// this can happen when there are 3 values in the list
						closestValue = tableValues[index];
						return true;
					}

					// go up
					indexLow = index;
					index = index + ((indexHigh-index)/2);
					key = tableKeys[index];
				}
				else
				{
					// value is lower or equal to value
					
					if( index == indexLow+1 )
					{
						// distances from value
						float low = Mathf.Abs(tableKeys[indexLow] - lookup);
						float high = Mathf.Abs(tableKeys[index] - lookup);

						// check which is closer to value
						if( high < low )
						{
							closestValue = tableValues[index];
							return true;
						}
						else
						{
							closestValue = tableValues[indexLow];
							return true;
						}
					}
					else if( index == indexLow )
					{
						// this can happen when there are 3 values in the list
						closestValue = tableValues[index];
						return true;
					}
					
					// go down
					indexHigh = index;
					index = indexLow + ((index-indexLow)/2);
					key = tableKeys[index];
				}
			}
		}

		/// <summary>
		/// Puts the values closest to the lookup into lowValue and highValue
		/// </summary>
		/// <returns><c>true</c>, if get value was successful, <c>false</c> otherwise.</returns>
		/// <param name="lowValue">Low value.</param>
		/// <param name="highValue">High value.</param>
		/// <param name="lerpValue">Lerp value.</param>
		/// <param name="lookup">Lookup.</param>
		public bool TryGetValue( out T lowValue, out T highValue, out float lerpValue, float lookup )
		{
			int length = Count;
			if( length == 0 )
			{
				lowValue = default(T);
				highValue = default(T);
				lerpValue = 0.5f;
				return false;
			}
			else if( length == 1 )
			{
				lowValue = tableValues[0];
				highValue = tableValues[0];
				lerpValue = 0.5f;
				return true;
			}

			index = length/2;
			key = tableKeys[index];
			
			indexLow = 0;
			indexHigh = length-1;
			lerpValue = 0.5f;

			while( true )
			{
				if( key < lookup ) // value is higher then current indexValue
				{
					if( index == indexHigh-1 )
					{
						low = Mathf.Abs(tableKeys[index] - lookup);
						high = Mathf.Abs(tableKeys[indexHigh] - lookup);
						
						// check which is closer to value
						if( low < high )
						{
							lowValue = tableValues[index];
							highValue = tableValues[indexHigh];
							lerpValue = Mathf.InverseLerp(tableKeys[index], tableKeys[indexHigh], lookup);
							return true;
						}
						else
						{
							lowValue = tableValues[index];
							highValue = tableValues[indexHigh];
							lerpValue = Mathf.InverseLerp(tableKeys[index], tableKeys[indexHigh], lookup);
							return true;
						}
					}
					else if( index == indexHigh ) // this can happen when there are 3 values in the list
					{
						lowValue = tableValues[index];
						highValue = tableValues[indexHigh];
						lerpValue = 0.5f;
						return true;
					}
					
					// go up
					indexLow = index;
					index = index + ((indexHigh-index)/2);
					key = tableKeys[index];
				}
				else // value is lower or equal to value
				{
					if( index == indexLow+1 )
					{
						low = Mathf.Abs(tableKeys[indexLow] - lookup);
						high = Mathf.Abs(tableKeys[index] - lookup);
						
						// check which is closer to value
						if( high < low )
						{

							lowValue = tableValues[indexLow];
							highValue = tableValues[index];
							lerpValue = Mathf.InverseLerp(tableKeys[indexLow], tableKeys[index], lookup);
							return true;
						}
						else
						{
							lowValue = tableValues[indexLow];
							highValue = tableValues[index];
							lerpValue = Mathf.InverseLerp(tableKeys[indexLow], tableKeys[index], lookup);
							return true;
						}
					}
					else if( index == indexLow ) // this can happen when there are 3 values in the list
					{
						lowValue = tableValues[indexLow];
						highValue = tableValues[index];
						lerpValue = 0.5f;
						return true;
					}
					
					// go down
					indexHigh = index;
					index = indexLow + ((index-indexLow)/2);

					// getting keys in this way may be its prob
					key = tableKeys[index];
				}
			}
		}

		/// <summary>
		/// Gets the closest <see cref="TogglePlay.Generics.NumberedLookup`1"/> to the specified lookup.
		/// </summary>
		/// <param name="lookup">Lookup value.</param>
		public T this[ float lookup ]
		{
			get
			{
				int length = Count;
				if( length == 0 )
					return default(T);
				else if( length == 1 )
					return tableValues[0];
				
				index = length/2;
				key = tableKeys[index];
				
				indexLow = 0;
				indexHigh = length-1;
				
				while( true )
				{
					if( key < lookup )
					{
						// value is higher then current indexValue
						
						if( index == indexHigh-1 )
						{
							// check which is closer to value
							if( Mathf.Abs(tableKeys[index] - lookup) < Mathf.Abs(tableKeys[indexHigh] - lookup) )
								return tableValues[index];
							else
								return tableValues[indexHigh];
						}
						else if( index == indexHigh )
							return tableValues[index];
						
						// go up
						indexLow = index;
						index = index + ((indexHigh-index)/2);
					}
					else
					{
						// value is lower or equal to value
						
						if( index == indexLow+1 )
						{
							// check which is closer to value
							if( Mathf.Abs(tableKeys[index] - lookup) < Mathf.Abs(tableKeys[indexLow] - lookup) )
								return tableValues[index];
							else
								return tableValues[indexLow];
						}
						else if( index == indexLow )
							return tableValues[index];
						
						// go down
						indexHigh = index;
						index = indexLow + ((index-indexLow)/2);
					}
				}
			}
		}

		//----------------------------------------------------------------------------------
		// Adding values
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Add the specified value to the lookup
		/// </summary>
		/// <param name="value">Value to add.</param>
		/// <param name="lookup">Lookup value at.</param>
		public void Add( T value, float lookup )
		{
			if( tableKeys == null )
			{
				tableKeys = new float[]{ lookup };
				tableValues = new T[]{ value };
				return;
			}

			int length = Count;
			float[] newTableKeys = new float[length+1];
			T[] newTableValues = new T[length+1];

			int index = 0;
			for( ; index <= length; ++index )
			{
				if( index == length )
				{
					// reached the end, add the new key and value
					newTableKeys[index] = lookup;
					tableKeys = newTableKeys;
					
					newTableValues[index] = value;
					tableValues = newTableValues;
					return;
				}
				else if( lookup < tableKeys[index] )
					break;

				newTableKeys[index] = tableKeys[index];
				newTableValues[index] = tableValues[index];
			}

			// value is somewhere between
			for( int i=length-1; i>index; --i )
			{
				newTableKeys[i+1] = tableKeys[i];
				newTableValues[i+1] = tableValues[i];
			}

			newTableKeys[index] = lookup;
			newTableValues[index] = value;

			tableKeys = newTableKeys;
			tableValues = newTableValues;
		}

		//----------------------------------------------------------------------------------
		// Removing values
		//----------------------------------------------------------------------------------

		/// <summary>
		/// Clear the values stored.
		/// </summary>
		public void Clear()
		{
			tableKeys = null;
			tableValues = null;
		}
	}

}
