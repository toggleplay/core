﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace TogglePlay.Generics
{
	public class DynamicArray<TYPE> : IEnumerable<TYPE>
	{
		protected TYPE[] array;

		private int length = 0;

		//----------------------------------------------------------------------------------
		// General
		//----------------------------------------------------------------------------------

		public int Count
		{
			get{ return length; }
		}

		//----------------------------------------------------------------------------------
		// Enumerators
		//----------------------------------------------------------------------------------

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// to allow foreach
		public IEnumerator<TYPE> GetEnumerator()
		{
			if( array != null )
			{
				for( int i = 0; i < length; ++i )
				{
					yield return array[i];
				}
			}

			yield break;
		}

		//----------------------------------------------------------------------------------
		// Access
		//----------------------------------------------------------------------------------


		public TYPE this[int i]
		{
			get{ return array[i]; }
			set{ array[i] = value; }
		}

		public TYPE[] ToArray()
		{
			if( array == null || array.Length == 0 )
				return new TYPE[0];

			TYPE[] rtn = new TYPE[length];
			System.Array.Copy( array, 0, rtn, 0, length );

			return rtn;
		}

		public bool Contains( TYPE item )
		{
			if( array == null )
				return false;
			
			for( int i = 0; i < length; ++i )
			{
				if( array[i].Equals(item) )
					return true;
			}
			
			return false;
		}
		
		
		public int IndexOf( TYPE item )
		{
			if( array == null )
				return -1;
			
			for( int i = 0; i < length; ++i )
			{
				if (array[i].Equals(item))
					return i;
			}
			
			return -1;
		}

		//----------------------------------------------------------------------------------
		// Size
		//----------------------------------------------------------------------------------

		public int Capacity
		{
			get{ return array.Length; }
			set
			{
				if( array == null )
				{
					array = new TYPE[value];
					return;
				}

				TYPE[] newCap = new TYPE[value];
				System.Array.Copy( array, 0, newCap, 0, System.Math.Min(value, length) );
				length = System.Math.Min(value, length);
				array = newCap;
			}
		}
		
		void ExpandCapacity()
		{
			Capacity = array == null ? 32 : System.Math.Max(array.Length << 1, 32);
		}

		//----------------------------------------------------------------------------------
		// Clearing
		//----------------------------------------------------------------------------------

		public void Clear( bool clearMemory = false )
		{
			length = 0;
			if( clearMemory )
				array = null;
		}

		//----------------------------------------------------------------------------------
		// Insertion
		//----------------------------------------------------------------------------------

		public void Add( TYPE item )
		{
			if( array == null || length == array.Length )
				ExpandCapacity();

			array[length++] = item;
		}

		/// <summary>
		/// Checks to see if the item is already in the Array before adding
		/// </summary>
		/// <param name="item">Item.</param>
		/// <returns>true if the item is added, false if the array already contains it.</returns>
		public bool AddNonDuplicate( TYPE item )
		{
			if( Contains( item ) )
				return false;

			if( array == null || length == array.Length )
				ExpandCapacity();
			
			array[length++] = item;
			return true;
		}


		public void Insert( int index, TYPE item )
		{
			// make sure there is room
			if( array == null || length == array.Length )
				ExpandCapacity();
			
			if( index < length )
			{
				// push everything up
				for( int i = length; i > index; --i )
					array[i] = array[i - 1];

				// replace the position element
				array[index] = item;
				++length;
			}
			else
				Add( item );
		}

		//----------------------------------------------------------------------------------
		// Removal
		//----------------------------------------------------------------------------------

		public bool Remove( TYPE item )
		{
			if (array != null)
			{
				for( int i = 0; i < length; ++i )
				{
					if( array[i].Equals(item) )
					{
						RemoveAt(i);
						return true;
					}
				}
			}
			return false;
		}

		public int RemoveAll( System.Predicate<TYPE> match )
		{
			int removed = 0;
			if( array != null )
			{
				for( int i = 0; i < length; ++i )
				{
					if( match.Invoke( array[i] ) )
					{
						removed++;
					}
					else if( removed > 0 )
					{
						// move down by removed count
						array[i-removed] = array[i];
					}
				}
			}

			// defaultify the top 'removed'
			for( int i=length-1; i>length-1-removed; --i )
				array[i] = default(TYPE);

			length -= removed;
			return removed;
		}

		public void RemoveAt( int index )
		{
			if( array == null || index >= length )
				return;

			--length;

			for( int i = index; i < length; ++i )
				array[i] = array[i + 1];

			array[length] = default(TYPE);
		}
	}

}
