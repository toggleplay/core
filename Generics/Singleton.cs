﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Generics
{
	/// <summary>
	/// This singleton instance is for use with components that can be edited in the scene.
	/// This singleton does not create an instance if it is not found. For that instead <see cref="TogglePlay.Generics.InstantSingleton"/> 
	/// </summary>
	public class Singleton<T> : MonoBehaviourEx where T : Component
	{
		/// <summary>The singleton instance.</summary>
		protected static T singletonInstance;

		public static bool isInstanceAvailable
		{
			get{ return Instance != null; }
		}

		public static bool isInstanceSet
		{
			get{ return singletonInstance != null; }
		}

		/// <summary>
		/// Gets the instance. Null if there is no Singleton instance within the scene.
		/// </summary>
		/// <value>The singleton instance.</value>
		public static T Instance
		{
			get
			{
				if (singletonInstance == null)
				{
					singletonInstance = FindObjectOfType<T>();
#if UNITY_EDITOR
					if( singletonInstance == null )
						Debug.LogWarning("Trying to access singleton, but cannot be found in the scene [" + typeof(T).ToString() + "]");
#endif
				}
				return singletonInstance;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="TogglePlay.Generics.Singleton`1"/> has an instance within the scene.
		/// </summary>
		/// <value><c>true</c> if has an instance; otherwise, <c>false</c>.</value>
		public static bool hasSingleton
		{
			get
			{
				if (singletonInstance == null)
				{
					singletonInstance = FindObjectOfType<T>();
#if UNITY_EDITOR
					if( singletonInstance == null )
						Debug.LogWarning("Trying to access singleton, but cannot be found in the scene");
#endif
				}
				return singletonInstance == null ? false : true;
			}
		}

		/// <summary>
		/// If there is another singleton instance that has been found then remove self from the scene.
		/// </summary>
		protected override void OnEnable()
		{
			base.OnEnable();

			if( singletonInstance != null && singletonInstance != this )
			{
				// There is already a singleton found within the scene. This may be due to using multiple scenes that have a DontDestroyOnLoad
				// on singleton shared between scenes and booting up from that specific scene
#if UNITY_EDITOR
				Debug.LogWarning("Singleton instance already exists within the scene. Destroying self but leaving the gameObject intact : " + this.gameObject.name );
#endif
				Destroy(this);
			}
			else
				singletonInstance = this as T;
		}

		/// <summary>
		/// Null the singletonInstance to make sure it does not remain in memory as static.
		/// </summary>
		protected virtual void OnApplicationQuit()
		{
			singletonInstance = null;
		}
	}
		
}
