﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
 * Change log:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

namespace TogglePlay.Generics
{

	/// <summary>
	/// Class for use in inheritance. This base class will create a GameObject with the component connected
	/// if non was found in the scene.
	/// Should not be used with singletons which require saved values within the scene. instead <see cref="TogglePlay.Generics.Singleton"/> 
	/// </summary>
	public class InstantSingleton<T> : MonoBehaviour where T : Component
	{
		/// <summary>The singleton instance.</summary>
		protected static T singletonInstance;

		/// <summary>.</summary>
		private static bool quitting = false;
		public static bool isInstanceAvailable
		{
			get
			{
				return quitting ? false : Instance != null;
			}
		}

		public static bool isInstanceSet
		{
			get
			{
				return quitting ? false : singletonInstance != null;
			}
		}

		/// <summary>
		/// Gets or creates the instance.
		/// </summary>
		/// <value>The singleton instance.</value>
		public static T Instance
		{
			get
			{
				if( quitting )
				{
					// prevent the singleton from creating a new object into the scene during quit, (remains in the editor if at this point)
					Debug.LogWarning( "Trying to access singleton during application close" );
					return null;
				}

				if (singletonInstance == null)
				{
					// Check to make sure one is not already within the scene
					singletonInstance = FindObjectOfType<T>();
					if( singletonInstance == null )
					{
						// No singleton found in scene. Create a new object in the scene and attach this component
						singletonInstance = new GameObject(typeof(T).Name + "_Singleton").AddComponent<T>();
					}
				}
				return singletonInstance;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="TogglePlay.Generics.InstantSingleton`1"/> has created an instance.
		/// </summary>
		/// <value><c>true</c> if there has been an instance creasted; otherwise, <c>false</c>.</value>
		public static bool hasSingleton
		{
			get
			{
				return singletonInstance == null ? false : true;
			}
		}

		/// <summary>
		/// Creates an instance of the singleton.
		/// </summary>
		/// <returns><c>true</c>, if new instance was created, <c>false</c> if there is already an instance.</returns>
		public static bool CreateInstance()
		{
			if (singletonInstance == null)
			{
				singletonInstance = FindObjectOfType<T>();
				if( singletonInstance == null )
				{
					// No singleton found in scene. Create a new object in the scene and attach this component
					singletonInstance = new GameObject(typeof(T).Name + "_Singleton").AddComponent<T>();
				}
				return true;
			}
			return false;
		}

		/// <summary>
		/// Destroy and recreate this instance.
		/// </summary>
		public static T Regenerate()
		{
			if( singletonInstance != null )
				Destroy(singletonInstance.gameObject);

			singletonInstance = new GameObject(typeof(T).Name + "_Singleton").AddComponent<T>();
			return singletonInstance;
		}

		/// <summary>
		/// If there is another singleton instance that has been created. Then this instance must be saved within a scene.
		/// This should not happen with the InstantSingleton.
		/// </summary>
		protected virtual void OnEnable()
		{
			if( singletonInstance != null && singletonInstance != this )
			{
				Debug.LogWarning("Singleton instance already created, likely this is saved within a scene.\n" +
					"Destroying self but leaving the gameObject intact : " + this.gameObject.name );
				Destroy(this);
			}
		}

		/// <summary>
		/// Null the singletonInstance to make sure it does not remain in memory.
		/// </summary>
		protected virtual void OnApplicationQuit()
		{
			if( singletonInstance != null )
				DestroyImmediate( singletonInstance );

			quitting = true;
			singletonInstance = null;
		}
	}

}
